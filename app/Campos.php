<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Campos extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'campos';

    protected $fillable = [
        'categorias_Id', 'nombre','descripcion','familias_Id','activo','no_aplica', 'icono',
    ];
}
