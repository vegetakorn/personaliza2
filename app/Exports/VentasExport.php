<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
Use \Maatwebsite\Excel\Sheet;

class VentasExport implements  FromView, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }


    public function view(): View
    {
       /* $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('tiendas.numsuc', 'tiendas.nombre', 'tiendas.mail' );
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $sql->where('tiendas.activo', "=", 1);
        $data = $sql->get();
        */
        $anio = $this->inputs['anio'];
        return view('exports.ventas', [
            'anio' => $anio,
        ]);
    }


}
