<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
Use \Maatwebsite\Excel\Sheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


class TodosExport implements   FromView, WithEvents
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public $inputs;
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    public function registerEvents(): array
    {

        return [

            AfterSheet::class => function (AfterSheet $event) {

            },
        ];
    }


    public function view(): View
    {
        return view('exports.todos', [
            'info' => $this->inputs
        ]);
    }
}
