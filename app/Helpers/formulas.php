<?php
//app/Helpers/Envato/User.php
namespace App\Helpers;



class Formulas {
    /**
     * @param int $user_id User-id
     *
     * @return string
     */
    public static function testFormula($id) {


        return "El id recibido es ". $id;
    }

    public static function avancePresupuesto( $ventas, $presupuesto) {

        if($presupuesto != 0)
        {
            $avance = round($ventas / $presupuesto * 100, 2);
        }else
        {
            $avance = 0;
        }

        return $avance;
    }

    public static function avancePresupuestoTeorico( $diasPresTot, $totalSuc) {

        $dias = date('d') -1;
        if($dias == 0)
        {
            $dias = 1;
        }

        $diasPresProm = round($diasPresTot/$totalSuc, 2);

        $avance = round($dias / $diasPresProm * 100, 2);

        return $avance;
    }

    public static function tendencia( $ventas, $diaspres) {

        //obtenemos la meta diaria real
        $dias = date('d') -1;
        if($dias == 0)
        {
            $dias = 1;
        }
        $metaDiaReal = round($ventas / $dias, 4);
        $tendencia = round($metaDiaReal * $diaspres ,2);
        return $tendencia;
    }

    public static function tendenciaPorcentaje( $tendencia, $presupuesto) {

      if($presupuesto != 0)
      {
          $tendenciaPor = round($tendencia / $presupuesto * 100 ,2);
      }else
      {
          $tendenciaPor = 0;
      }

        return $tendenciaPor;
    }
    public static function transDiaPromedio( $tickets, $registros) {

       if($registros != 0)
       {
           $transProm = round($tickets / $registros,2);
       }else
       {
           $transProm = 0;
       }

        return $transProm;

    }

    public static function artXticketPromedio( $noartxtck, $registros) {

        if($registros != 0)
        {
            $notckProm = round($noartxtck / $registros,2);
        }else
        {
            $notckProm = 0;
        }
        return $notckProm;

    }
    public static function ticketPromedio( $ventas, $tickets) {

        if($tickets != 0)
        {
            $tckProm = round($ventas / $tickets ,2);
        }else
        {
            $tckProm = 0;
        }

        return $tckProm;

    }

    public static function metaDiaria( $presupuesto, $diaspres) {

        if($diaspres != 0)
        {
            $meta = round($presupuesto / $diaspres ,2);
        }else
        {
            $meta = 0;
        }

        return $meta;

    }

    public static function diferenciaDias($fecha1, $fecha2)
    {
        $fecha1 = explode("-", $fecha1);
        $fecha2 = explode("-", $fecha2);
        $fechaMk1=mktime(0,0,0,$fecha1[1],$fecha1[2],$fecha1[0]);
        $fechaMk2=mktime(0,0,0,$fecha2[1],$fecha2[2],$fecha2[0]);

        $segundos_diferencia=$fechaMk1-$fechaMk2;

        //convierto segundos en días
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

        //obtengo el valor absoulto de los días (quito el posible signo negativo)
        $dias_diferencia = abs($dias_diferencia);

        //quito los decimales a los días de diferencia
        $dias_diferencia = floor($dias_diferencia);

        return $dias_diferencia;
    }

    public static function diferenciaSegundos($fecha1, $fecha2)
    {
        $fecha1Mes  = date('m', strtotime($fecha1));
        $fecha1Anio = date('Y', strtotime($fecha1));
        $fecha1Dia  = date('d', strtotime($fecha1));
        $fecha1Hora = date('H', strtotime($fecha1));
        $fecha1Min  = date('i', strtotime($fecha1));
        $fecha1Seg  = date('s', strtotime($fecha1));

        $fecha2Mes  = date('m', strtotime($fecha2));
        $fecha2Anio = date('Y', strtotime($fecha2));
        $fecha2Dia  = date('d', strtotime($fecha2));
        $fecha2Hora = date('H', strtotime($fecha2));
        $fecha2Min  = date('i', strtotime($fecha2));
        $fecha2Seg  = date('s', strtotime($fecha2));


        $fechaMk1=mktime($fecha1Hora,$fecha1Min,$fecha1Seg,$fecha1Mes,$fecha1Dia,$fecha1Anio);
        $fechaMk2=mktime($fecha2Hora,$fecha2Min,$fecha2Seg,$fecha2Mes,$fecha2Dia,$fecha2Anio);

        $segundos_diferencia=$fechaMk1-$fechaMk2;

        return $segundos_diferencia * -1;
    }

    public static function fechaBD($fecha1)
    {
        $fecha = date('Y-m-d', strtotime($fecha1));

        return $fecha;

    }
    /**Devuelve los dias que tiene un mes**/
    public static function diasMes($mes, $anio)
    {

        $numero = cal_days_in_month(CAL_GREGORIAN, $mes, $anio); // 31

        return $numero;

    }


    public static function coloresSignos($actual, $anterior)
    {
       if($actual > $anterior)
       {
           $color_texto = 'text-green';
       }else if($actual ==  $anterior)
       {
           $color_texto = 'text-yellow';
       }else if($actual <  $anterior)
       {
           $color_texto = 'text-red';
       }

        return $color_texto ;
    }

    public static function flechasSignos($actual, $anterior)
    {
        if($actual > $anterior)
        {
            $flecha = 'fa-caret-up';
        }else if($actual ==  $anterior)
        {
            $flecha = 'fa-caret-left';
        }else if($actual <  $anterior)
        {
            $flecha = 'fa-caret-down';
        }

        return $flecha ;
    }

}