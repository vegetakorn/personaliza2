<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\MailDirVentas;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use DB;
use App\Helpers\Formulas;

class correoDirVentas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'correo:dirventas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Correo que manda las ventas a los directores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $formulas = new Formulas();
        $anio = date('Y');
        $anioAnt = date('Y');
        $mes = date('m');

        if($mes == 1)
        {
            $mesAnt = 12;
            $anioAnt = $anio -1;
        }else
        {
            $mesAnt = $mes -1;
        }

        //listamos todas las empresas registradas
        $data['test'] = '';
        $data_emp =   DB::table('empresas')
            ->whereIn('empresas.Id',array(6,7))->get();
        $empresas = collect($data_emp)->toArray();
        $arrEmp = array();
        for($i = 0;$i<count($empresas);$i++) {

            //buscamos todos los supervisores
            $sql  = DB::table('empleados')
                ->leftjoin('puestos', function ($join) {
                    $join->on('puestos.Id', '=', 'empleados.puesto_Id');
                })
                ->leftjoin('users', function ($join) {
                    $join->on('empleados.Id', '=', 'users.empleados_Id');
                })
                ->leftjoin('emp_puestos', function ($join) {
                    $join->on('puestos.Id', '=', 'emp_puestos.puestos_Id');
                })
                ->leftjoin('empresas', function ($join) {
                    $join->on('empresas.Id', '=', 'empleados.empresas_Id');
                })
                ->select('empleados.*', 'puestos.puesto', 'empresas.Nombre as Empresa', 'users.id as user_Id' );

            $sql->where('emp_puestos.tipo_puesto_Id', "=", 2); //los tipo dos son directores
            $sql->where('empleados.empresas_Id', "=", $empresas[$i]->Id);
            $sql->where('empleados.activo', "=", 1);

            $data_sup = $sql->get();
            $supervisores = collect($data_sup)->toArray();
            //buscamos las tiendas asignadas de cada director
            $arrSup = array();
            for($j = 0;$j<count($supervisores);$j++)
            {
                $data_suc=   DB::table('users_tiendas')
                    ->leftjoin('tiendas', function ($join) {
                        $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
                    })
                    ->leftjoin('razon_social', function ($join) {
                        $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
                    })
                    ->leftjoin('plazas', function ($join) {
                        $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
                    })
                    ->select('tiendas.*', 'razon_social.nombre as Razon', 'plazas.plaza' )
                    ->where('users_tiendas.users_Id','=',$supervisores[$j]->user_Id)
                    ->where('tiendas.activo','=',1)
                    ->get();
                $sucursales = collect($data_suc)->toArray();
                //buscamos la información financiera de cada tienda
                $arrSuc = array();
                $arrVis = array();
                $arrTod = array();
                for($k = 0;$k<count($sucursales);$k++)
                {

                    $mes_reg = DB::table('ventas_mes')
                        ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                        ->where('ventas_mes.mes', '=', $mes )
                        ->where('ventas_mes.anio', '=', date('Y') )
                        ->count();
                    if($mes_reg != 0)
                    {
                        //buscamos la información mensual actual de la tienda
                        $data_mes_act = DB::table('ventas_mes')
                            ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                            ->where('ventas_mes.mes', '=', $mes )
                            ->where('ventas_mes.anio', '=', $anio )
                            ->first();
                        $mes_act = collect($data_mes_act)->toArray();

                        //buscamos la información mensual anterior de la tienda
                        $data_mes_ant = DB::table('ventas_mes')
                            ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                            ->where('ventas_mes.mes', '=', $mesAnt )
                            ->where('ventas_mes.anio', '=', $anioAnt )
                            ->first();
                        $mes_ant = collect($data_mes_ant)->toArray();

                        //buscamos la ultima venta cargada
                        $data_dia =   DB::table('ventas_dia')
                            ->where('ventas_dia.tiendas_Id','=',$sucursales[$k]->Id)
                            ->whereMonth('ventas_dia.fhventa','=',$mes)
                            ->whereYear('ventas_dia.fhventa' , "=", date('Y'))
                            ->orderByRaw('ventas_dia.Id DESC')
                            ->first();
                        $ventas_dias = collect($data_dia)->toArray();

                        //buscamos los dias capturados actual
                        $data_dia_count =   DB::table('ventas_dia')
                            ->where('ventas_dia.tiendas_Id','=',$sucursales[$k]->Id)
                            ->where('ventas_dia.monto','<>',0)
                            ->whereMonth('ventas_dia.fhventa','=',$mes)
                            ->whereYear('ventas_dia.fhventa' , "=", date('Y'))
                            ->count();

                        $data_dia_count_ant =   DB::table('ventas_dia')
                            ->where('ventas_dia.tiendas_Id','=',$sucursales[$k]->Id)
                            ->where('ventas_dia.monto','<>',0)
                            ->whereMonth('ventas_dia.fhventa','=',$mesAnt)
                            ->whereYear('ventas_dia.fhventa' , "=", $anioAnt)
                            ->count();



                        $avance_pres = $formulas->avancePresupuesto( $mes_act['ventas'], $mes_act['presupuesto']);

                        $tendencia = $formulas->tendencia($mes_act['ventas'], $mes_act['dias']);

                        $tendenciaPor = $formulas->tendenciaPorcentaje($tendencia, $mes_act['presupuesto']);

                        $transDiaAct = $formulas->transDiaPromedio($mes_act['tickets'], $data_dia_count);

                        $transDiaAnt = $formulas->transDiaPromedio($mes_ant['tickets'], $data_dia_count_ant);


                        //transacciones diarias anterior
                        $arrSuc[] = array('Razon' => $sucursales[$k]->Razon,
                            'Plaza' => $sucursales[$k]->plaza,
                            'NumSuc' => $sucursales[$k]->numsuc,
                            'Nombre' => $sucursales[$k]->nombre,
                            'Sucursal' => $sucursales[$k]->nombre,
                            'Presupuesto' => '$ '.number_format($mes_act['presupuesto'], 2, '.', ','),
                            'Ventas' => '$ '.number_format($mes_act['ventas'], 2, '.', ','),
                            'LastVenta' =>$ventas_dias['fhventa'],
                            'Avance'    => $avance_pres.'%',
                            'Tendencia' => '$ '.number_format($tendencia, 2, '.', ',')." (".$tendenciaPor."%)",
                            'TckPromAct' => '$ '.number_format($mes_act['tckprom'], 2, '.', ','),
                            'TicketsAct' => $transDiaAct,
                            'NoTckAct' => $mes_act['artxtck'],
                            'TckPromAnt' => '$ '.number_format($mes_ant['tckprom'], 2, '.', ','),
                            'TicketsAnt' => $transDiaAnt,
                            'NoTckAnt' => $mes_ant['artxtck']
                        );
                    }


                }
                //enviamos correo
                $data['Supervisor'] = $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat;
                $data['TablaVentas'] = $arrSuc;

                /**Configuración de servidor de correo**/

                if($empresas[$i]->Id == 1)
                {
                    // Setup a new SmtpTransport instance for Gmail
                    $transport = new SmtpTransport();
                    $transport->setHost('smtp.mailgun.org');
                    $transport->setPort(587);
                    $transport->setEncryption('tls');
                    $transport->setUsername('postmaster@envios.ventumsupervision.com');
                    $transport->setPassword('b4139af002e42bdcdebf82604feeb1c0-c8e745ec-3c370a1d');


                    // Assign a new SmtpTransport to SwiftMailer
                    $driver = new Swift_Mailer($transport);

                    // Assign it to the Laravel Mailer
                    Mail::setSwiftMailer($driver);
                }else
                {
                    // Setup a new SmtpTransport instance for Gmail
                    $transport = new SmtpTransport();
                    $transport->setHost('smtp-relay.sendinblue.com');
                    $transport->setPort(587);
                    $transport->setEncryption('tls');
                    $transport->setUsername('ventum@ventummx.com');
                    $transport->setPassword('LTr9qvOFVBC7PX6H');


                    // Assign a new SmtpTransport to SwiftMailer
                    $driver = new Swift_Mailer($transport);

                    // Assign it to the Laravel Mailer
                    Mail::setSwiftMailer($driver);
                }

                  Mail::to($supervisores[$j]->mail)->send(new MailDirVentas($data));
                //Mail::to('hdam23@gmail.com')->send(new MailDirVentas($data));
            }
        }
        // Send your message
        // Mail::to('hdam23@gmail.com')->send(new MailSupervisores($data));
    }
}
