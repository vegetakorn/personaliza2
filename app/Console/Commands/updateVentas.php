<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Helpers\Formulas;


class updateVentas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ventas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $formulas = new Formulas();
        //listamos todas las empresas registradas
        $data_emp =   DB::table('empresas')
            ->where('empresas.Activo','=',1)->get();
        $empresas = collect($data_emp)->toArray();
        for($i = 0;$i<count($empresas);$i++)
        {
            $data_suc=   DB::table('tiendas')
                ->where('tiendas.empresas_Id','=',$empresas[$i]->Id)
                ->where('tiendas.activo','=',1)
                ->get();
            $sucursales = collect($data_suc)->toArray();

            //listamos los meses del año en curso para ser actualizados
            for($j = 1; $j <= date('m'); $j++)
            {

                for($k = 0;$k<count($sucursales);$k++)
                {
                    //listamos las tiendas de la empresa

                    //listamos las ventas diarias del mes de cada tienda
                    $data_dia =   DB::table('ventas_dia')
                        ->where('ventas_dia.tiendas_Id','=',$sucursales[$k]->Id)
                        ->whereMonth('ventas_dia.fhventa','=',$j)
                        ->whereYear('ventas_dia.fhventa' , "=", date('Y'))
                        ->get();
                    $ventas_dias = collect($data_dia)->toArray();

                    //hacemos la suma de todos los elementos de los dias encontrados
                    $ventas = 0;
                    $tickets = 0;
                    $artxticket = 0;
                    for($l = 0;$l<count($ventas_dias);$l++)
                    {
                        $ventas = $ventas + $ventas_dias[$l]->monto;
                        $tickets = $tickets + $ventas_dias[$l]->tickets;
                        $artxticket = $artxticket + $ventas_dias[$l]->artxticket;
                    }

                    $tckPromMes = $formulas->ticketPromedio($ventas, $tickets);

                    $promArtxtck = $formulas->artXticketPromedio($artxticket, count($ventas_dias));

                    //vemos si tiene presupuesto capturado, en caso contrario no actualizamos
                    $mes_reg = DB::table('ventas_mes')
                        ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                        ->where('ventas_mes.mes', '=', $j )
                        ->where('ventas_mes.anio', '=', date('Y') )
                        ->count();
                    if($mes_reg != 0)
                    {
                        //seleccionamos los valores de la venta del mes para actualizar
                        $data_mes = DB::table('ventas_mes')
                            ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                            ->where('ventas_mes.mes', '=', $j )
                            ->where('ventas_mes.anio', '=', date('Y') )
                            ->get();
                        $ventas_mes = collect($data_mes)->toArray();
                        $meta_dia = $formulas->metaDiaria($ventas_mes[0]->presupuesto , $ventas_mes[0]->dias);


                        DB::table('ventas_mes')
                            ->where('ventas_mes.tiendas_Id', '=', $sucursales[$k]->Id )
                            ->where('ventas_mes.mes', '=', $j )
                            ->where('ventas_mes.anio', '=', date('Y') )
                            ->update([
                                'ventas' =>  $ventas,
                                'tickets' =>  $tickets,
                                'artxtck' => $promArtxtck,
                                'tckprom' => $tckPromMes,
                                'metadia' => $meta_dia
                            ]);
                    }
                }
            }
        }
    }
}
