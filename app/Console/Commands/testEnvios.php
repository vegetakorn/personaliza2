<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\MailTest;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;

class testEnvios extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:envios';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prueba de envio de correos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data['Test'] = "";
        //vamos a guardar el arreglo de las ventas
        //listamos todas las empresas


        // Send your message
        Mail::to('hdam23@gmail.com')->send(new MailTest($data));
    }
}
