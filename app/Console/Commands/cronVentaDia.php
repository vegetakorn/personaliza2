<?php

namespace App\Console\Commands;

use function GuzzleHttp\Psr7\str;
use Illuminate\Console\Command;
use DB;
use App\User;
use http\Exception;
use App\Helpers\Formulas;


class cronVentaDia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sube:venta';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron de subida de ventas diarias para ROMA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $formulas = new Formulas();
        $Error = "";
        $errCode = "";
        $validFile = "";
        $data =   DB::table('empresas')->get();
        $empresas = collect($data)->toArray();
        for($i = 0;$i<count($empresas);$i++)
        {
            $empresas_Id = $empresas[$i]->Id;
            $carpeta = $empresas[$i]->RFC;
            $rutaDirectorio = public_path('Clientes/'.$carpeta.'/Ventas');
            $rutaDirectorioHist = public_path('Clientes/'.$carpeta.'/Historial');
            $ErrorFin = "";

            $directorio = opendir($rutaDirectorio);

            while ($archivo = readdir($directorio))
            {
                if ($archivo == "." || $archivo == "..")
                    continue;


                $ext = explode(".", $archivo);

                switch($ext[1])
                {
                    case "txt":
                        $Contenido = file($rutaDirectorio."/".$archivo);
                        if(count($Contenido)>0){
                            for($x = 0;$x<count($Contenido);$x++){

                                $linetmp = explode(";",$Contenido[$x]); //separamos los elementos de la fila con el simbolo ;
                                if(count($linetmp) == 5)
                                {
                                    $clave = strip_tags($linetmp[0]) ;
                                    $str = $clave;
                                    $intClave = (int) filter_var($str, FILTER_SANITIZE_NUMBER_INT);

                                    $dataSuc =   DB::table('tiendas')
                                        ->where('tiendas.numsuc','=', $intClave )
                                        ->where('tiendas.empresas_Id','=', $empresas_Id )
                                        ->first();
                                    $tiendas = collect($dataSuc)->toArray();

                                    $fecha = trim($linetmp[1]);
                                    $monto = trim($linetmp[2]);
                                    $ticket = trim($linetmp[3]);
                                    $noart   = trim($linetmp[4]);
                                    $fechadiv = explode("/", $fecha);
                                    $tckProm = $formulas->ticketPromedio($monto, $ticket);
                                    //verificamos si el dia ya esta capturado para saber si actualizar o insertar
                                    $fecha = date('Y-m-d', strtotime($fecha));
                                    $dataDia =   DB::table('ventas_dia')
                                        ->where('ventas_dia.fhventa','=', $fecha )
                                        ->where('ventas_dia.tiendas_Id','=', $tiendas['Id'] )
                                        ->count();
                                    if($dataDia == 0)
                                    {
                                        //capturamos la venta
                                        DB::table('ventas_dia')->insert([
                                            [
                                                'tiendas_Id' =>  $tiendas['Id'],
                                                'fhventa' => date('Y-m-d', strtotime($fecha)),
                                                'fhupload' => date('Y-m-d '),
                                                'monto' => $monto,
                                                'tickets' => $ticket,
                                                'artxticket' => $noart,
                                                'tckprom' => $tckProm
                                            ]
                                        ]);

                                    }else
                                    {
                                        DB::table('ventas_dia')
                                            ->where('ventas_dia.fhventa','=', $fecha )
                                            ->where('ventas_dia.tiendas_Id','=', $tiendas['Id'] )
                                            ->update([
                                                'fhupload' => date('Y-m-d '),
                                                'monto' => $monto,
                                                'tickets' => $ticket,
                                                'artxticket' => $noart,
                                                'tckprom' => $tckProm
                                            ]);
                                    }

                                    //$this->info($fechadiv[1] );
                                    copy($rutaDirectorio."/".$archivo, $rutaDirectorioHist."/"."mov_".$archivo);
                                    //borramos el archivo
                                    //unlink($rutaDirectorio."/".$archivo);
                                }

                            }
                        }
                        // $this->info(count($Contenido));
                        break;
                    default:
                        break;
                }


            }
        }


    }
}
