<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class TiendasController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {

        $sql  = DB::table('tiendas');
        $tiendas = $sql->get();


        return $tiendas;
    }


    public function getTiendas(Request $request)
    {
        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id' );
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $tiendas = $sql->get();
        $tiendas_count = $sql->count();



        return response()->json(['tiendas' => $tiendas, 'no' =>$tiendas_count ]);
    }

    public function getUbicacion(Request $request)
    {
        $sql  = DB::table('ubicaciones');
        $sql->where('ubicaciones.CP', "=", $request['cp']);
        $ubicacion = $sql->first();


        return response()->json(['ubicacion' => $ubicacion ]);
    }

    public function fotoTienda(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Tienda";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Tiendas/', $nombre);
        $ruta = $this->path.'Tiendas/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function cargaInfoTiendaEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('tiendas')
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('zonas.Id', '=', 'tiendas.zona');
            })
            ->select('tiendas.*', 'plazas.plaza',  'razon_social.nombre as razon', 'zonas.zona as nombreZona' );
        $sql->where('tiendas.Id', "=", $request['id']);
        $tienda = $sql->first();


        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();


        $sql = DB::table('zonas');

        $sql->where('zonas.empresas_Id','=',auth()->user()->empresas_Id);
        $zonas = $sql->get();



        return response()->json(['razones' => $razones, 'tienda' => $tienda , 'zonas' => $zonas ]);
    }

    public function updateTienda(Request $request)
    {

        DB::table('tiendas')
            ->where('Id', $request['id'])
            ->update([
                'numsuc' =>  $request['numsuc'],
                'nombre' =>  $request['nombre'],
                'fhapertura' =>  $request['fhapertura'],
                'direccion' =>  $request['direccion'],
                'telefono' =>  $request['telefono'],
                'imagen' =>  $request['foto'],
                'mail' =>  $request['mail'],
                'm2' =>  $request['m2'],
                'emp_ideal' =>  $request['emp_ideal'],
                'plaza_Id' =>  $request['plaza_Id'],
                'razon_Id' =>  $request['razon_Id'],
                'password'  =>  $request['password'],
                'zona'  =>  $request['zona'],
                'CP'  =>  $request['cp'],
                'lat'  =>  $request['lat'],
                'lon'  =>  $request['lng']
            ]);


        return response()->json(['message' => $request->all() ] );
    }
}
