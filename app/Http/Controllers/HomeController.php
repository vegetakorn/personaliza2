<?php

namespace App\Http\Controllers;

use App\Exports\DesempenoExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use DB;
use App\Helpers\Formulas;
use App\Helpers\Listados;
use Barryvdh\DomPDF\Facade as PDF;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['test'] = "Inicio";
        return view('/home')->with( $data);


       
    }


    public function filtroSignosVitales(Request $request)
    {
        $listado = new Listados();
        $formulas = new Formulas();
        $anio = $request['anio'];
        $anioAnt = $request['anio'];

        if($request['mes'] != 0)
        {
            $mes = $request['mes'];
        }else
        {
            $mes = date('m');
        }


        if($mes == 1)
        {
            $mesAnt = 12;
            $anioAnt = $anio - 1;
        }else
        {
            $mesAnt = $mes - 1;
        }


       $data_suc = $listado->listaTiendasFiltro(auth()->user()->id, $request['razon'], $request['plaza'], $request['suc']);
         $tiendas_signos = collect($data_suc)->toArray();
         $arrSignos = array();
         for($i = 0;$i<count($tiendas_signos);$i++)
         {
             //buscamos los dias capturados
             //buscamos los dias capturados actual
             $data_dia_count =   DB::table('ventas_dia')
                 ->where('ventas_dia.tiendas_Id','=',$tiendas_signos[$i]->Id)
                 ->where('ventas_dia.monto','<>',0)
                 ->whereMonth('ventas_dia.fhventa','=',$mes)
                 ->whereYear('ventas_dia.fhventa' , "=", date('Y'))
                 ->count();

             $data_dia_count_ant =   DB::table('ventas_dia')
                 ->where('ventas_dia.tiendas_Id','=',$tiendas_signos[$i]->Id)
                 ->where('ventas_dia.monto','<>',0)
                 ->whereMonth('ventas_dia.fhventa','=',$mesAnt)
                 ->whereYear('ventas_dia.fhventa' , "=", $anioAnt)
                 ->count();

             //buscamos el ticket promedio
             $data_mes_act = DB::table('ventas_mes')
                 ->where('ventas_mes.tiendas_Id', '=', $tiendas_signos[$i]->Id )
                 ->where('ventas_mes.mes', '=', $mes )
                 ->where('ventas_mes.anio', '=', $anio )
                 ->first();
             $mes_act = collect($data_mes_act)->toArray();

             //buscamos la información mensual anterior de la tienda
             $data_mes_ant = DB::table('ventas_mes')
                 ->where('ventas_mes.tiendas_Id', '=', $tiendas_signos[$i]->Id  )
                 ->where('ventas_mes.mes', '=', $mesAnt )
                 ->where('ventas_mes.anio', '=', $anioAnt )
                 ->first();
             $mes_ant = collect($data_mes_ant)->toArray();

             if(count($mes_act) != 0 )
             {
                 if(count($mes_ant) != 0 )
                 {
                     $tckprom = '$ '.number_format($mes_act['tckprom'], 2, '.', ',');
                     $tckpromnum = $mes_act['tckprom'];


                     $tckpromnumant = $mes_ant['tckprom'];

                     $transDiaAct = $formulas->transDiaPromedio($mes_act['tickets'], $data_dia_count);
                     $transDiaAnt = $formulas->transDiaPromedio($mes_ant['tickets'], $data_dia_count_ant);

                     $noartxtck = $mes_act['artxtck'];
                     $noartxtckant = $mes_ant['artxtck'];
                 }else
                 {
                     $tckprom = '$ '.number_format($mes_act['tckprom'], 2, '.', ',');
                     $tckpromnum = $mes_act['tckprom'];


                     $tckpromnumant = 0;

                     $transDiaAct = $formulas->transDiaPromedio($mes_act['tickets'], $data_dia_count);
                     $transDiaAnt = 0;

                     $noartxtck = $mes_act['artxtck'];
                     $noartxtckant = 0;
                 }



             }else
             {
                 $tckprom = 0;
                 $tckpromnum = 0;
                 $tckpromnumant = 0;

                 $noartxtck = 0;
                 $noartxtckant = 0;

                 $transDiaAct = 0;
                 $transDiaAnt = 0;


             }

             //calculo de colores


             $arrSignos[] = array("razon" =>$tiendas_signos[$i]->razon,
                 "plaza" => $tiendas_signos[$i]->plaza,
                 "numsuc" => $tiendas_signos[$i]->numsuc,
                 "nombre" => $tiendas_signos[$i]->nombre,
                 "ticket" => $tckprom,
                 "color_tck" => $formulas->coloresSignos($tckpromnum, $tckpromnumant),
                 "flecha_tck" => $formulas->flechasSignos($tckpromnum, $tckpromnumant),
                 "trans" => $transDiaAct,
                 "color_trans" => $formulas->coloresSignos($transDiaAct, $transDiaAnt),
                 "flecha_trans" => $formulas->flechasSignos($transDiaAct, $transDiaAnt),
                 "artxtck" => $noartxtck,
                 "color_art" => $formulas->coloresSignos($noartxtck, $noartxtckant),
                 "flecha_art" => $formulas->flechasSignos($noartxtck, $noartxtckant));
         }
         $signos = $arrSignos;
        $mes_text = $listado->getMes(date('m'));

        return response()->json(['signos' => $signos, 'mes' => $mes_text ] );


    }


    public function getDesempeno(Request $request)
    {
        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);
        /**Cargamos los supervisores asignados**/
        $listado = new Listados();
        $data_sup = $listado->listaSupervisores(auth()->user()->id);
        $supervisores = collect($data_sup)->toArray();
        // $campos = array(1,2,3,4,5);
        $arrSup = array();
        for($i = 0;$i<count($supervisores);$i++)
        {
            $razon = "Sin razón";
            $plaza = "Sin plaza";


            $raz_data = DB::table('razon_social')->select('razon_social.nombre')->where('razon_social.Id','=',$supervisores[$i]->razon_Id);
            $razones = $raz_data->first();
            $count = $raz_data->count();
            if($count != 0)
            {
                $razon = $razones->nombre;
            }

            $pla_data = DB::table('plazas')->select('plazas.plaza')->where('plazas.Id','=',$supervisores[$i]->plaza_Id);
            $plazas = $pla_data->first();
            $countP = $pla_data->count();
            if($countP != 0)
            {
                $plaza = $plazas->plaza;
            }

            //obtenemos el numero de sucursales de cada supervisor
            $emp_data = DB::table('users')->where('users.id','=',$supervisores[$i]->supervisor_Id);
            $usuario = $emp_data->first();
            $countU = $emp_data->count();

            if($countU != 0)
            {
                //si tiene usuario
                $tiendas = DB::table('users_tiendas')->select('users_tiendas.*')->where('users_tiendas.users_Id','=',$usuario->id)->count();

                //buscamos cuantas visitas de checklist tiene
                $checklist = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',1)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $seguimientos = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',2)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                $anteriores = DB::table('visitas_todo')
                            ->leftjoin('visitas', function ($join) {
                                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                            })
                            ->where('visitas.users_Id','=',$usuario->id)
                            ->where('visitas_todo.fecha_inicio','<',trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]))
                            ->count();

                $creados = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $nuevos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',151)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $curso = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',152)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $pendientes = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',153)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $concluidos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',154)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                //actividades
                $act  = DB::table('actividades')
                    ->leftjoin('actividad_empleado', function ($join) {
                        $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                    })
                    ->leftjoin('empleados', function ($join) {
                        $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                    });
                $act->where('actividad_empleado.empleados_Id', "=", $supervisores[$i]->Id);
                $act ->whereIn('actividades.status',array(151,152,153));
                $act ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
                $act->groupBy('actividades.Id');
                $actividades = $act->count();



            }else
            {
                $tiendas = 'S/U';
                $checklist = 0;
                $seguimientos = 0;
                $anteriores = 0;
                $creados = 0;
                $nuevos = 0;
                $curso = 0;
                $pendientes = 0;
                $concluidos = 0;
                $actividades = 0;


            }
            //calculo de efectividad
            if($creados != 0)
            {
                $efectividad = round($concluidos / $creados* 100, 2);
            }else
            {
                $efectividad = 0;
            }



            $arrSup[] = array("clave"   => $supervisores[$i]->clave,
                "nombre"  =>  $supervisores[$i]->nombre." ".$supervisores[$i]->apepat,
                "puesto"   => $supervisores[$i]->puesto,
                "razon"   => $razon,
                "plaza"   => $plaza,
                "tiendas" => $tiendas,
                "checklist" => $checklist,
                "seguimientos" => $seguimientos,
                "anteriores" => $anteriores,
                "creados" => $creados,
                "nuevos" => $nuevos,
                "curso" => $curso,
                "pendientes" => $pendientes,
                "concluidos" => $concluidos,
                "actividades" => $actividades,
                "efectividad" => $efectividad);
        }



        return response()->json(['supervisores' => $arrSup ] );
    }


    public function genGraficaVta(Request $request)
    {
       //buscamos si tiene activo el widget de ventas
        $sql  = DB::table('users_widgets');
        $sql->where('users_widgets.users_Id', "=", auth()->user()->id);
        $sql->where('users_widgets.widgets_Id', "=", 1);
       $widget = $sql->count();

       if($widget != 0)
       {
           $formulas = new Formulas();

           if($request['mes']  != 0)
           {
               $mes = $request['mes'] ;
           }else
           {
               $mes = date('m') ;
           }


           $anio = $request['anio'] ;
           //obtenemos las tiendas asignadas
           $sql  = DB::table('users_tiendas')
               ->leftjoin('tiendas', function ($join) {
                   $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
               })
               ->select('tiendas.*', 'users_tiendas.Id as usti_id' );
           $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
           if($request['razon']  != 0)
           {
               $sql->where('tiendas.razon_Id', "=", $request['razon'] );
           }
           if($request['plaza']  != 0)
           {
               $sql->where('tiendas.plaza_Id', "=", $request['plaza'] );
           }
           if($request['sucursal']  != 0)
           {
               $sql->where('tiendas.Id', "=", $request['sucursal'] );
           }

           $sql->where('tiendas.activo', "=", 1);
           $sql->orderByRaw('tiendas.nombre ASC');
           $data_tiendas = $sql->get();
           $tiendas = collect($data_tiendas)->toArray();
           $arrInfo = array();
           $width = 1000;
           $sinpres = 0;
           $totsuc = 0;
           $enpres = 0;
           $porEnpres = 0;
           $porSinpres = 0;
           $sumVentas = 0;
           $sumPresupuesto = 0;
           $sumTickets = 0;
           $sumNoartxtck = 0;
           $diasCapturadosTot = 0;
           $diasPresTot = 0;
           $tendenciaMayor = 0;
           for($i = 0;$i<count($tiendas);$i++)
           {
               $totsuc = count($tiendas);
               if($i > 5)
               {
                   $width =  $width +150;
               }

               if($width > 25000)
               {
                   $width = 30000;
               }

               //verificamos que tenfa presupuesto capturado
               $sql  = DB::table('ventas_mes');
               $sql->where('ventas_mes.tiendas_Id', "=", $tiendas[$i]->Id);
               $sql->where('ventas_mes.mes', "=", $mes);
               $sql->where('ventas_mes.anio', "=", $anio);
               $count_pres = $sql->count();
               $tienda = $tiendas[$i]->nombre;
               if($count_pres != 0)
               {
                   //buscamos las ventas, presupuestos del mes actual de la tienda
                   $sql  = DB::table('ventas_mes');
                   $sql->where('ventas_mes.tiendas_Id', "=", $tiendas[$i]->Id);
                   $sql->where('ventas_mes.mes', "=", $mes);
                   $sql->where('ventas_mes.anio', "=", $anio);
                   $venta_mes = $sql->first();
                   $informes = collect($venta_mes)->toArray();

                   //obtenemos las ventas diarias capturadas
                   $sql  = DB::table('ventas_dia');
                   $sql->where('ventas_dia.tiendas_Id', "=", $tiendas[$i]->Id);
                   $sql->whereMonth('ventas_dia.fhventa', "=", $mes);
                   $sql->whereYear('ventas_dia.fhventa', "=", $anio);
                   $sql->where('ventas_dia.monto', "<>", 0);
                   $venta_dia = $sql->count();



                   //guardamos las variables antes de enviar el arreglo

                   $ventas = $informes['ventas'];
                   $presupuesto = $informes['presupuesto'];
                   $dias = $informes['dias'];
                   $tickets = $informes['tickets'];
                   $noartxticket = $informes['artxtck'];

                   $sumNoartxtck = $sumNoartxtck + $noartxticket;

                   $diasCapturadosTot = $diasCapturadosTot + $venta_dia;
                   $diasPresTot = $diasPresTot + $dias;

                   if($presupuesto == 0)
                   {
                       $sinpres++;
                   }

                   if($ventas > $presupuesto)
                   {
                       if($presupuesto != 0)
                       {
                           $enpres++;
                       }

                   }

                   $sumVentas = $sumVentas + $ventas;
                   $sumPresupuesto = $sumPresupuesto + $presupuesto;
                   $sumTickets = $sumTickets + $tickets;


                   $tendencia = $formulas->tendencia($ventas, $dias);

                   if($tendencia > $presupuesto)
                   {
                       $tendenciaMayor++;
                   }


                   //guardamos los resultados
                   $arrInfo[] = array('tienda' => $tienda, 'ventas' =>  $ventas, 'presupuesto' => $presupuesto, 'tendencia' => $tendencia, 'dias' => $dias, 'sum_diaria' => $venta_dia);
               }else
               {
                   $arrInfo[] = array('tienda' => $tienda, 'ventas' => 0, 'presupuesto' => 0, 'tendencia' => 0, 'dias' => 0 , 'sum_diaria' => 0);
               }



           }

           $transDia = round($sumTickets / $totsuc,2);

           $tckProm = $formulas->ticketPromedio($sumVentas, $sumTickets);
           $avancePresTeorico = $formulas->avancePresupuestoTeorico($diasPresTot, $totsuc);

           if($enpres != 0)
           {
               $porEnpres = floor($enpres / $totsuc * 100);
           }

           $totEnpres = $totsuc - $sinpres;

           if($sinpres != 0)
           {
               $porSinpres = floor($totEnpres / $totsuc * 100);
           }
           $porTendenciaMayor = 0;
           if($tendenciaMayor != 0)
           {
               $porTendenciaMayor = floor($tendenciaMayor / $totsuc * 100);
           }

           $vtavspres = $formulas->avancePresupuesto($sumVentas, $sumPresupuesto);
           $noArtxtckProm = $formulas->artXticketPromedio($sumNoartxtck , $totsuc );


           return response()->json(['grafica' => $arrInfo ,
               'ancho' => $width,
               'info' =>$request->all(),
               'totsuc' => $totsuc,
               'nopres' => $totEnpres,
               'enpres' => $enpres,
               'porEnpres' => $porEnpres,
               'porSinpres' => $porSinpres,
               'vtavspres' => $vtavspres,
               'transDia' => $transDia,
               'avanceTeorico' => $avancePresTeorico,
               'sumTickets' => $sumTickets,
               'noArtxtckprom' => $noArtxtckProm,
               'tendenciaMayor' => $tendenciaMayor,
               'porTendenciaMayor' => $porTendenciaMayor,
               'tckProm' => number_format($tckProm, 2, '.', ',') ,
               'sumVta' =>number_format($sumVentas, 2, '.', ',') ,
               'sumPres' =>number_format($sumPresupuesto, 2, '.', ',')  ]);
       }else
       {
           return response()->json(['grafica' => "" ,
               'ancho' => "",
               'info' => "",
               'totsuc' => "",
               'nopres' => "",
               'enpres' => "",
               'porEnpres' => "",
               'porSinpres' => "",
               'vtavspres' => "",
               'transDia' => "",
               'avanceTeorico' => "",
               'sumTickets' => "",
               'noArtxtckprom' => "",
               'tendenciaMayor' => "",
               'porTendenciaMayor' => "",
               'tckProm' => number_format("0", 2, '.', ',') ,
               'sumVta' =>number_format("0", 2, '.', ',') ,
               'sumPres' =>number_format("0", 2, '.', ',')  ]);
       }



    }

    public function evalDesempPdf($fechaini, $fechafin)
    {
        $listado = new Listados();
        $formulas = new Formulas();


        $data['fecha_ini'] = trim($fechaini);
        $data['fecha_fin'] = trim($fechafin);

        $fechaini = explode('-', trim($fechaini));
        $fechafin = explode('-', trim($fechafin));

        $data['logo'] = auth()->user()->fotoEmp;

        /**Cargamos los supervisores asignados**/
        $listado = new Listados();
        $data_sup = $listado->listaSupervisores(auth()->user()->id);
        $supervisores = collect($data_sup)->toArray();
        // $campos = array(1,2,3,4,5);
        $arrSup = array();
        for($i = 0;$i<count($supervisores);$i++)
        {
            $razon = "Sin razón";
            $plaza = "Sin plaza";


            $raz_data = DB::table('razon_social')->select('razon_social.nombre')->where('razon_social.Id','=',$supervisores[$i]->razon_Id);
            $razones = $raz_data->first();
            $count = $raz_data->count();
            if($count != 0)
            {
                $razon = $razones->nombre;
            }

            $pla_data = DB::table('plazas')->select('plazas.plaza')->where('plazas.Id','=',$supervisores[$i]->plaza_Id);
            $plazas = $pla_data->first();
            $countP = $pla_data->count();
            if($countP != 0)
            {
                $plaza = $plazas->plaza;
            }

            //obtenemos el numero de sucursales de cada supervisor
            $emp_data = DB::table('users')->where('users.id','=',$supervisores[$i]->supervisor_Id);
            $usuario = $emp_data->first();
            $countU = $emp_data->count();

            if($countU != 0)
            {
                //si tiene usuario
                $tiendas = DB::table('users_tiendas')->select('users_tiendas.*')->where('users_tiendas.users_Id','=',$usuario->id)->count();

                //buscamos cuantas visitas de checklist tiene
                $checklist = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',1)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $seguimientos = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',2)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                $anteriores = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas_todo.fecha_inicio','<',trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]))
                    ->count();

                $creados = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $nuevos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',151)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $curso = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',152)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $pendientes = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',153)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $concluidos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',154)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                //actividades
                $act  = DB::table('actividades')
                    ->leftjoin('actividad_empleado', function ($join) {
                        $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                    })
                    ->leftjoin('empleados', function ($join) {
                        $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                    });
                $act->where('actividad_empleado.empleados_Id', "=", $supervisores[$i]->Id);
                $act ->whereIn('actividades.status',array(151,152,153));
                $act ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
                $act->groupBy('actividades.Id');
                $actividades = $act->count();



            }else
            {
                $tiendas = 'S/U';
                $checklist = 0;
                $seguimientos = 0;
                $anteriores = 0;
                $creados = 0;
                $nuevos = 0;
                $curso = 0;
                $pendientes = 0;
                $concluidos = 0;
                $actividades = 0;


            }
            //calculo de efectividad
            if($creados != 0)
            {
                $efectividad = round($concluidos / $creados* 100, 2);
            }else
            {
                $efectividad = 0;
            }



            $arrSup[] = array("clave"   => $supervisores[$i]->clave,
                "nombre"  =>  $supervisores[$i]->nombre." ".$supervisores[$i]->apepat,
                "puesto"   => $supervisores[$i]->puesto,
                "razon"   => $razon,
                "plaza"   => $plaza,
                "tiendas" => $tiendas,
                "checklist" => $checklist,
                "seguimientos" => $seguimientos,
                "anteriores" => $anteriores,
                "creados" => $creados,
                "nuevos" => $nuevos,
                "curso" => $curso,
                "pendientes" => $pendientes,
                "concluidos" => $concluidos,
                "actividades" => $actividades,
                "efectividad" => $efectividad);
        }


        $data['supervisores'] = $arrSup;

        $path = 'evaluacionDes.pdf';
        return $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->setPaper('A2', 'landscape')->loadView('/pdf/evaluacion', compact('data'))->download($path);
    }

    public function evalDesempXls($fechaini, $fechafin)
    {

        $listado = new Listados();
        $formulas = new Formulas();


        $data['fecha_ini'] = trim($fechaini);
        $data['fecha_fin'] = trim($fechafin);

        $fechaini = explode('-', trim($fechaini));
        $fechafin = explode('-', trim($fechafin));
        /**Cargamos los supervisores asignados**/
        $listado = new Listados();
        $data_sup = $listado->listaSupervisores(auth()->user()->id);
        $supervisores = collect($data_sup)->toArray();
        // $campos = array(1,2,3,4,5);
        $arrSup = array();
        for($i = 0;$i<count($supervisores);$i++)
        {
            $razon = "Sin razón";
            $plaza = "Sin plaza";


            $raz_data = DB::table('razon_social')->select('razon_social.nombre')->where('razon_social.Id','=',$supervisores[$i]->razon_Id);
            $razones = $raz_data->first();
            $count = $raz_data->count();
            if($count != 0)
            {
                $razon = $razones->nombre;
            }

            $pla_data = DB::table('plazas')->select('plazas.plaza')->where('plazas.Id','=',$supervisores[$i]->plaza_Id);
            $plazas = $pla_data->first();
            $countP = $pla_data->count();
            if($countP != 0)
            {
                $plaza = $plazas->plaza;
            }

            //obtenemos el numero de sucursales de cada supervisor
            $emp_data = DB::table('users')->where('users.id','=',$supervisores[$i]->supervisor_Id);
            $usuario = $emp_data->first();
            $countU = $emp_data->count();

            if($countU != 0)
            {
                //si tiene usuario
                $tiendas = DB::table('users_tiendas')->select('users_tiendas.*')->where('users_tiendas.users_Id','=',$usuario->id)->count();

                //buscamos cuantas visitas de checklist tiene
                $checklist = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',1)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $seguimientos = DB::table('visitas')
                    ->select('visitas.*')
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas.tipo_visita','=',2)
                    ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                $anteriores = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->where('visitas_todo.fecha_inicio','<',trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]))
                    ->count();

                $creados = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $nuevos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',151)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $curso = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',152)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $pendientes = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',153)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();
                $concluidos = DB::table('visitas_todo')
                    ->leftjoin('visitas', function ($join) {
                        $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
                    })
                    ->where('visitas_todo.Status','=',154)
                    ->where('visitas.users_Id','=',$usuario->id)
                    ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])))
                    ->count();

                //actividades
                $act  = DB::table('actividades')
                    ->leftjoin('actividad_empleado', function ($join) {
                        $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                    })
                    ->leftjoin('empleados', function ($join) {
                        $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                    });
                $act->where('actividad_empleado.empleados_Id', "=", $supervisores[$i]->Id);
                $act ->whereIn('actividades.status',array(151,152,153));
                $act ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
                $act->groupBy('actividades.Id');
                $actividades = $act->count();



            }else
            {
                $tiendas = 'S/U';
                $checklist = 0;
                $seguimientos = 0;
                $anteriores = 0;
                $creados = 0;
                $nuevos = 0;
                $curso = 0;
                $pendientes = 0;
                $concluidos = 0;
                $actividades = 0;


            }
            //calculo de efectividad
            if($creados != 0)
            {
                $efectividad = round($concluidos / $creados* 100, 2);
            }else
            {
                $efectividad = 0;
            }



            $arrSup[] = array("clave"   => $supervisores[$i]->clave,
                "nombre"  =>  $supervisores[$i]->nombre." ".$supervisores[$i]->apepat,
                "puesto"   => $supervisores[$i]->puesto,
                "razon"   => $razon,
                "plaza"   => $plaza,
                "tiendas" => $tiendas,
                "checklist" => $checklist,
                "seguimientos" => $seguimientos,
                "anteriores" => $anteriores,
                "creados" => $creados,
                "nuevos" => $nuevos,
                "curso" => $curso,
                "pendientes" => $pendientes,
                "concluidos" => $concluidos,
                "actividades" => $actividades,
                "efectividad" => $efectividad);
        }


        $data['supervisores'] = $arrSup;


        return Excel::download(new DesempenoExport($data), 'evaluacion_des.xlsx');

    }

}
