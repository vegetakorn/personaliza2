<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    //

    public function importUsers(Request $request)
    {
        \Excel::load($request->excel, function($reader) {

            $excel = $reader->get();

            // iteracción
            $reader->each(function($row) {

                $user = new User;
                $user->name = $row->name;
                $user->email = $row->email;
                $user->password = bcrypt($row->password);
                $user->save();

            });

        });

        return $request->all();
    }
}
