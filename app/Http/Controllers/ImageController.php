<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
class ImageController extends Controller
{
    //protected $path = '/home/hdammx/public_html/VentumAdmin/uploads/'; //path para host en linea
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function index()
    {
        return view('image.index');
    }

    public function uploadImgEmpresa(Request $request)
    {
        //$file = Input::file('image');
        $file =  $request->file('file');
        //$nombre = $file->getClientOriginalName();
        $nombre = "_Logo_Empresa";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."-".$fecha."-".$nombre.".".$extension;
       // $path = public_path('uploads/Empresas/'.$nombre);
        $image = Image::make($file->getRealPath());

        $image->save($this->path.'Empresas/'.$nombre);
        $image->save($this->pathUploadCli.'Empresas/'.$nombre);
        return $nombre;
    }

    public function uploadImgVisita(Request $request)
    {
        //$file = Input::file('image');
        $files =  $request->file('file');
        foreach($files as $file){
            //$nombre = $file->getClientOriginalName();
            $nombre = "_Foto_Visita";
            $extension = $file->getClientOriginalExtension();
            $random = str_random(10);
            $fecha = date('Ymdhis');
            $nombre = $random."-".$fecha."-".$nombre.".".$extension;
            $file->move($this->path.'FotoVisita/', $nombre);
            $nombres[] = $nombre;

        }

        return $nombres;
    }

    public function uploadImgSeguimiento(Request $request)
    {
        //$file = Input::file('image');
        $files =  $request->file('file');
        foreach($files as $file){
            //$nombre = $file->getClientOriginalName();
            $nombre = "_Foto_Seguimiento";
            $extension = $file->getClientOriginalExtension();
            $random = str_random(10);
            $fecha = date('Ymdhis');
            $nombre = $random."-".$fecha."-".$nombre.".".$extension;
            $file->move($this->path.'FotoVisita/', $nombre);
            $nombres[] = $nombre;
//
        }

        return $nombres;
    }

    public function uploadImgCampo(Request $request)
    {
        //$file = Input::file('image');


        return "OK";
    }

    public function uploadXlsTiendas(Request $request)
    {
        //$file = Input::file('image');
      $file =  $request->file('file');
        //$nombre = $file->getClientOriginalName();
        $nombre = "_Import_Tiendas";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."-".$fecha."-".$nombre.".".$extension;
      //  $image = Image::make($file->getRealPath());
      //  $image->save($this->path.'Tiendas/Import/'.$nombre);
        $file->move($this->path.'Tiendas/Import/', $nombre);
        return $nombre;
    }
}
