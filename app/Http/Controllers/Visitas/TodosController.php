<?php

namespace App\Http\Controllers\Visitas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Helpers\Listados;
use App\Exports\TodosExport;
use Maatwebsite\Excel\Facades\Excel;

class TodosController extends Controller
{
    //protected $path = '/home/hdammx/public_html/VentumAdmin/uploads/'; //path para host en linea
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = new Listados();
        $data['tiendas'] = $this->getListadoTiendas();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        $sql = DB::table('familias');
        $sql ->where('familias.empresas_Id','=',auth()->user()->empresas_Id);
        $data['familias'] = $sql->get();
        //buscamos los checklist asignados a puesto
        $data['checklist']  = $listado->listaChecklist(auth()->user()->empleados_Id , auth()->user()->empresas_Id);
        //buscamos los supervisores que tienen asignados
        $data['supervisores'] = $listado->listaSupervisores(auth()->user()->id);
        return view('/todos/lista')->with( $data);
    }

    public function indextienda()
    {
        $listado = new Listados();

        $sql = DB::table('familias');
        $sql ->where('familias.empresas_Id','=',auth()->user()->empresas_Id);
        $data['familias'] = $sql->get();
        //buscamos los checklist asignados a puesto
        $data['checklist']  = $listado->listaChecklist(0 , auth()->user()->empresas_Id);
        //buscamos los supervisores que tienen asignados
        $data['supervisores'] = $listado->listaSupervisores(77); /**TODO: cuando se implemente en otros clientes se debera quitar el usuario 77 y ver la forma de listar supervisores.**/
        return view('/usuariotienda/todos/lista')->with( $data);
    }

    public function getTodos(Request $request)
    {

        //obtenemos las visitas buscadas por el filtro
        $sql = DB::table('visitas');

        $sql->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            });

        $sql->leftjoin('users_tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
        })

        ->leftjoin('checklist', function ($join) {
            $join->on('checklist.Id', '=', 'visitas.checklist_Id');
        })
        ->select('visitas.*', 'tiendas.nombre', 'checklist.nombre as Checklist');
        $sql ->where('visitas.Status','=',131);
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);


        if($request['tienda'] != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$request['tienda']);
        }

        if($request['sup'] == 1 && $request['seg'] == 0)
        {
            $sql ->where('visitas.tipo_visita','=',1);
        }
        if($request['sup'] == 0 && $request['seg'] == 1)
        {
            $sql ->where('visitas.tipo_visita','=',2);
        }



        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);

        $sql ->whereBetween('visitas.HoraInicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $visitas = $sql->get();
        $consulta = $sql->toSql();

        return response()->json(['visitas' => $visitas, 'consulta' => $consulta] );
    }

    public function getToDoFiltro(Request $request)
    {

        //obtenemos los to-do's existentes de la tienda
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })->leftjoin('users_tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('visitas.users_Id','visitas_todo.*','categorias.nombre as categoria', 'checklist.nombre as checklist', 'campos.nombre', 'campos.smart','campos.familias_Id', 'familias.Nombre as familia', 'visitas.tiendas_Id', 'tiendas.nombre as tienda');

            $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);

            if($request['tienda'] != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$request['tienda']);
        }
        if($request['razon'] != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$request['razon']);
        }
        if($request['plaza'] != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$request['plaza']);
        }

        if($request['sup_id'] != 0)
        {
            $sql ->where('visitas.users_Id','=',$request['sup_id']);
        }

        if($request['chk_Id'] != 0)
        {
            $sql ->where('checklist.Id','=',$request['chk_Id']);
        }

        if($request['cat_Id'] != 0)
        {
            $sql ->where('categorias.Id','=',$request['cat_Id']);
        }

        if($request['chk'] == 1 && $request['exp'] == 0)
        {
            $sql ->where('visitas_todo.tipo','=',1);
        }
        if($request['chk'] == 0 && $request['exp'] == 1)
        {
            $sql ->where('visitas_todo.tipo','=',2);
        }
        if($request['fam'] != 0)
        {
            $sql ->where('campos.familias_Id','=',$request['fam']);
        }
        if($request['nuevo'] == 1 && $request['cur'] == 1 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152, 153));
        }else
        if($request['nuevo'] == 1 && $request['cur'] == 1 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152));
        }else
        if($request['nuevo'] == 1 && $request['cur'] == 0 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,153));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 1 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(152,153));
        }else
        if($request['nuevo'] == 1 && $request['cur'] == 0 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 1 && $request['pend'] == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(152));
        }else
        if($request['nuevo'] == 0 && $request['cur'] == 0 && $request['pend'] == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(153));
        }

        $fechas = explode("-",$request['fecha'] );

        $fechaini = explode('/', $fechas[0]);
        $fechafin = explode('/', $fechas[1]);

        $sql ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $todos = $sql->get();

        return response()->json(['message' => $todos , 'usuario' => $request['sup_id']] );
    }

    public function pdf($tipoChk, $tipoExp, $fam, $nuevo, $curso, $pendiente, $fechaini, $fechafin, $tiendas_Id, $chk_Id, $cat_Id, $razon, $plaza, $supervisor)
    {
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('users_tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('visitas_todo.*','categorias.nombre as categoria', 'checklist.nombre as checklist', 'campos.nombre', 'campos.familias_Id', 'familias.Nombre as familia', 'visitas.tiendas_Id', 'tiendas.nombre as tienda');
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);
        if($tiendas_Id != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$tiendas_Id);
        }

        if($razon != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$razon);
        }
        if($plaza != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$plaza);
        }

        if($chk_Id != 0)
        {
            $sql ->where('checklist.Id','=',$chk_Id);
        }

        if($cat_Id != 0)
        {
            $sql ->where('categorias.Id','=',$cat_Id);
        }

        if($supervisor != 0)
        {
            $sql ->where('visitas.users_Id','=',$supervisor);
        }

        if($tipoChk == 1 && $tipoExp == 0)
        {
            $sql ->where('visitas_todo.tipo','=',1);
        }
        if($tipoChk == 0 && $tipoExp == 1)
        {
            $sql ->where('visitas_todo.tipo','=',2);
        }
        if($fam != 0)
        {
            $sql ->where('campos.familias_Id','=',$fam);
        }
        if($nuevo == 1 && $curso == 1 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152, 153));
        }else
        if($nuevo == 1 && $curso == 1 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152));
        }else
        if($nuevo == 1 && $curso == 0 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,153));
        }else
        if($nuevo == 0 && $curso == 1 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(152,153));
        }else
        if($nuevo == 1 && $curso == 0 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151));
        }else
        if($nuevo == 0 && $curso == 1 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(152));
        }else
        if($nuevo == 0 && $curso == 0 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(153));
        }

        //$fechas = explode("-",$pendiente );

        $fechaini = explode('-', $fechaini);
        $fechafin = explode('-', $fechafin);

        $sql ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $data['todos'] = $sql->get();

        $data['logo'] = auth()->user()->fotoEmp;

        $path = 'reporte_Todo.pdf';
        return $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('/todos/listapdf', compact('data'))->download($path);


    }

    public function excel($tipoChk, $tipoExp, $fam, $nuevo, $curso, $pendiente, $fechaini, $fechafin, $tiendas_Id, $chk_Id, $cat_Id, $razon, $plaza, $supervisor)
    {
        $sql = DB::table('visitas_todo');
        $sql->leftjoin('campos', function ($join) {
            $join->on('campos.Id', '=', 'visitas_todo.campos_Id');
        })
            ->leftjoin('visitas', function ($join) {
                $join->on('visitas.Id', '=', 'visitas_todo.visitas_Id');
            })
            ->leftjoin('categorias', function ($join) {
                $join->on('categorias.Id', '=', 'campos.categorias_Id');
            })
            ->leftjoin('checklist', function ($join) {
                $join->on('checklist.Id', '=', 'categorias.checklist_Id');
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'visitas.tiendas_Id');
            })
            ->leftjoin('users_tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('visitas_todo.*','categorias.nombre as categoria', 'checklist.nombre as checklist', 'campos.nombre', 'campos.familias_Id', 'familias.Nombre as familia', 'visitas.tiendas_Id', 'tiendas.nombre as tienda');
        $sql ->where('users_tiendas.users_Id','=',auth()->user()->id);
        if($tiendas_Id != 0)
        {
            $sql ->where('visitas.tiendas_Id','=',$tiendas_Id);
        }

        if($razon != 0)
        {
            $sql ->where('tiendas.razon_Id','=',$razon);
        }
        if($plaza != 0)
        {
            $sql ->where('tiendas.plaza_Id','=',$plaza);
        }

        if($chk_Id != 0)
        {
            $sql ->where('checklist.Id','=',$chk_Id);
        }

        if($cat_Id != 0)
        {
            $sql ->where('categorias.Id','=',$cat_Id);
        }

        if($supervisor != 0)
        {
            $sql ->where('visitas.users_Id','=',$supervisor);
        }

        if($tipoChk == 1 && $tipoExp == 0)
        {
            $sql ->where('visitas_todo.tipo','=',1);
        }
        if($tipoChk == 0 && $tipoExp == 1)
        {
            $sql ->where('visitas_todo.tipo','=',2);
        }
        if($fam != 0)
        {
            $sql ->where('campos.familias_Id','=',$fam);
        }
        if($nuevo == 1 && $curso == 1 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152, 153));
        }else
        if($nuevo == 1 && $curso == 1 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,152));
        }else
        if($nuevo == 1 && $curso == 0 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(151,153));
        }else
        if($nuevo == 0 && $curso == 1 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(152,153));
        }else
        if($nuevo == 1 && $curso == 0 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(151));
        }else
        if($nuevo == 0 && $curso == 1 && $pendiente == 0)
        {
            $sql ->whereIn('visitas_todo.Status',array(152));
        }else
        if($nuevo == 0 && $curso == 0 && $pendiente == 1)
        {
            $sql ->whereIn('visitas_todo.Status',array(153));
        }

        //$fechas = explode("-",$pendiente );

        $data['FechaIni'] = date('d/m/Y', strtotime($fechaini));
        $data['FechaFin'] = date('d/m/Y', strtotime($fechafin));

        $fechaini = explode('-', $fechaini);
        $fechafin = explode('-', $fechafin);

        $sql ->whereBetween('visitas_todo.fecha_inicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

        $data['todos'] = $sql->get();

        return  Excel::download(new TodosExport($data), 'reporte_todos.xlsx');
    }



}
