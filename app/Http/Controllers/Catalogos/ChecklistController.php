<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Checklist;
use App\Categorias;
use App\Campos;
use App\CamposVal;
use App\Helpers\Listados;

class ChecklistController extends Controller
{
    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes

    /**Creamos la instancia de listados**/
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     *

     */
    public function index()
    {
        $sql = DB::table('users');
        $sql->leftjoin('empresas', function ($join) {
            $join->on('empresas.Id', '=', 'users.empresas_id');
        })->select('users.id', 'users.empresas_Id', 'empresas.Logo');
        $sql->where('users.id','=',auth()->user()->id);
        $datos = $sql->get();

        $sesion  = json_decode($datos, true);
        $data['checklist']  =   DB::table('checklist')
                                 ->where('checklist.empresas_Id','=',$sesion[0]['empresas_Id'])
            ->where('checklist.activo','=',1)
            ->get();
        $sql2 = DB::table('tiendas');

        $data['tiendas'] = $this->getListadoTiendas();


        $data['familias'] =   DB::table('familias')

            ->where('familias.empresas_Id','=',auth()->user()->empresas_Id)->get();
        return view('/checklist/checklist')->with( $data);
    }

    public function categorias(Request $request)
    {
        $data =   DB::table('categorias')

                    ->where('categorias.checklist_Id','=',$request['body'])
            ->where('categorias.activo','=',1)

            ->get();

        return response()->json(['message' => $data] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function campos(Request $request)
    {
        $data =   DB::table('campos')
            ->leftjoin('campos_valores', function ($join) {
                $join->on('campos.Id', '=', 'campos_valores.campos_Id')
                    ->where('campos_valores.valor', '<>', 0);
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })

            ->select('campos.Id', 'campos.nombre', 'campos.descripcion',
                     'familias.nombre as familia', 'campos_valores.valor')
            ->where('campos.categorias_Id','=',$request['body'])
            ->where('campos.activo','=',1)
            ->get();

        return response()->json(['message' => $data ]);

    }

    public function addChecklist(Request $request)
    {

        DB::table('checklist')->insert([
            [
                'empresas_Id' =>  auth()->user()->empresas_Id,
                'nombre' =>  $request['name'],
                'activo' =>  1,
                'descripcion' => ""
            ]
        ]);


        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function updChecklist(Request $request)
    {

        DB::table('checklist')
            ->where('Id', $request['id'])
            ->update([
                'nombre' =>  $request['name']
            ]);


        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function delChecklist(Request $request)
    {

        DB::table('checklist')
            ->where('Id', $request['id'])
            ->update([
                'activo' =>  0
            ]);


        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }



    public function addCampo(Request $request)
    {

        DB::table('campos')->insert([
            [
                'categorias_Id' =>  $request['categorias_Id'],
                'nombre' =>  $request['nombre'],
                'descripcion' =>  $request['campo_desc'],
                'familias_Id' =>  $request['familia_Id'],
                'no_aplica' =>  $request['no_aplica'],
                'smart' =>  $request['smart'],
                'foto' =>  $request['foto'],
                'activo' =>  1
            ]
        ]);

        $id_campo = DB::getPdo()->lastInsertId();


        DB::table('campos_valores')->insert([
            [
                'campos_Id' =>  $id_campo,
                'nombre' =>  'Si',
                'valor' =>  $request['valor'],
                'gentodo' =>  0,
                'activo' =>  1
            ]
        ]);

        DB::table('campos_valores')->insert([
            [
                'campos_Id' =>  $id_campo,
                'nombre' =>  'No',
                'valor' =>  0,
                'gentodo' =>  1,
                'activo' =>  1
            ]
        ]);


        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function editCampo(Request $request)
    {

        $data =   DB::table('campos')
            ->leftjoin('campos_valores', function ($join) {
                $join->on('campos.Id', '=', 'campos_valores.campos_Id')
                    ->where('campos_valores.valor', '<>', 0);
            })
            ->leftjoin('familias', function ($join) {
                $join->on('familias.Id', '=', 'campos.familias_Id');
            })

            ->select('campos.*',
                'familias.nombre as familia', 'campos_valores.valor')

            ->where('campos.Id','=',$request['id'])
            ->first();

        $familias =   DB::table('familias')->get();

        return response()->json(['message' => $data , 'familias' => $familias]);

    }

    public function updateCampo(Request $request)
    {


        DB::table('campos')
            ->where('Id', $request['id'])

            ->update([
                'categorias_Id' =>  $request['categorias_Id'],
                'nombre' =>  $request['nombre'],
                'descripcion' =>  $request['campo_desc'],
                'familias_Id' =>  $request['familia_Id'],
                'no_aplica' =>  $request['no_aplica'],
                'smart' =>  $request['smart'],
                'foto' =>  $request['foto'],
                'activo' =>  1
            ]);



        DB::table('campos_valores')
            ->where('campos_Id', $request['id'])
            ->where('gentodo', 0)
            ->update([
                'valor' =>  $request['valor']
            ]);


        return response()->json(['message' => $request->all() ] );

    }

    public function delCampo(Request $request)
    {

        DB::table('campos')
            ->where('Id', $request['id'])
            ->update([
                'activo' =>  0
            ]);


        //return view('/checklist/checklist')->with( $data);
    }


    public function addCategoria(Request $request)
    {

        DB::table('categorias')->insert([
            [
                'checklist_Id' =>  $request['chk_Id'],
                'nombre' =>  $request['name'],
                'activo' =>  1,
                'descripcion' => $request['desc'],
                'orden' => 0,
                'evaluacion_cat' => 0
            ]
        ]);


        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function updCategoria(Request $request)
    {
        DB::table('categorias')
            ->where('Id', $request['id'])
            ->update([
                'nombre' =>  $request['name'],
                'descripcion' =>  ""
            ]);


        return response()->json(['message' => $request->all() ] );

    }

    public function delCategoria(Request $request)
    {

        DB::table('categorias')
            ->where('Id', $request['id'])
            ->update([
                'activo' => 0,

            ]);

        return response()->json(['message' => $request->all() ] );

        //return view('/checklist/checklist')->with( $data);
    }

    public function fotoCampo(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Campo";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Campos/', $nombre);
        $ruta = $this->path.'Campos/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

}
