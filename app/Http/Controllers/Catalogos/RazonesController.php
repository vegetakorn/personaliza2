<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
class RazonesController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();

        //dd($data['puestos'] );

        return view('/razones/razones')->with( $data);
    }

    public function fotoRazon(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Razon";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Razones/', $nombre);
        $ruta = $this->path.'Razones/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function addRazon(Request $request)
    {

        DB::table('razon_social')->insert([
            [
                'empresas_Id' =>  auth()->user()->empresas_Id,
                'clave' =>  $request['clave'],
                'nombre' =>  $request['nombre'],
                'foto' =>  $request['foto']
            ]
        ]);


        return response()->json(['message' => $request->all() ] );
    }

    public function cargaInfoRazonEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('razon_social');
        $sql->where('razon_social.Id', "=", $request['id']);
        $razon = $sql->first();

        return response()->json(['razon' => $razon ]);
    }

    public function updateRazon(Request $request)
    {

        DB::table('razon_social')
            ->where('Id', $request['id'])
            ->update([
                'clave' =>  $request['clave'],
                'nombre' =>  $request['nombre'],
                'foto' =>  $request['foto']
            ]);

        return response()->json(['message' => $request->all() ] );
    }

    public function bajaRazon(Request $request)
    {

        DB::table('tiendas')
            ->where('razon_Id', $request['id'])
            ->update([
                'razon_Id' =>  0,
                 'plaza_Id' =>  0
            ]);

        DB::table('empleados')
            ->where('razon_Id', $request['id'])
            ->update([
                'razon_Id' =>  0,
                'plaza_Id' =>  0
            ]);


        DB::table('razon_social')
            ->where('Id', '=', $request['id'])->delete();



        return response()->json(['message' => $request->all() ] );
    }





}
