<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Listados;
use App\Helpers\Formulas;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use App\Mail\MailPassUser;

class UsuariosController extends Controller
{
    /**Creamos la instancia de listados**/
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = DB::table('users');

        $sql->where('users.empresas_Id','=',auth()->user()->empresas_Id);
        $sql->where('users.empleados_Id','<>',0);
        $sql->where('users.Activo','=',1);
        $sql->where('users.tipo_user','=',1);
        $data['usuarios'] = $sql->get();
        //dd($data['puestos'] );
        //obtenemos los empleados de la sucursal
        $sql = DB::table('empleados');
        $sql->where('empleados.empresas_Id','=',auth()->user()->empresas_Id);

        $data['empleados'] = $sql->get();

        $data['tiendas'] = $this->getListadoTiendas();

        return view('/usuarios/lista')->with( $data);
    }

    public function perfil()
    {

        $data['tiendas'] = $this->getListadoTiendas();

        $sql = DB::table('users')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users.empleados_Id');
            })
            ->select('users.*', 'empleados.Id as emp_Id', 'empleados.nombre', 'empleados.apepat', 'empleados.apemat','empleados.telefono', 'empleados.mail', 'empleados.mail', 'empleados.domicilio', 'empleados.curp', 'empleados.ssocial', 'empleados.foto' );
        $sql->where('users.Id', "=", auth()->user()->id);
        $data['empleado'] = $sql->first();

        return view('/usuarios/perfil')->with( $data);
    }

    public function actividades()
    {

        $data['tiendas'] = $this->getListadoTiendas();

        $sql = DB::table('users')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users.empleados_Id');
            })
            ->select('users.*', 'empleados.Id as emp_Id', 'empleados.nombre', 'empleados.apepat', 'empleados.apemat','empleados.telefono', 'empleados.mail', 'empleados.mail', 'empleados.domicilio', 'empleados.curp', 'empleados.ssocial', 'empleados.foto' );
        $sql->where('users.Id', "=", auth()->user()->id);
        $data['empleado'] = $sql->first();

        return view('/usuarios/actividades')->with( $data);
    }

    public function updateUser(Request $request)
    {

        DB::table('empleados')
            ->where('Id', $request['id'])
            ->update([
                'nombre' =>  $request['nombre'],
                'apepat' =>  $request['apepat'],
                'apemat' =>  $request['apemat'],
                'domicilio' =>  $request['domicilio'],
                'telefono' =>  $request['telefono'],
                'foto' =>  $request['foto'],
                'mail' =>  $request['mail'],
                'curp' =>  $request['curp'],
                'ssocial' =>  $request['nss']
            ]);

        DB::table('users')
            ->where('empleados_Id', $request['id'])
            ->update([
                'name' =>  $request['nombre']." ".$request['apepat']." ".$request['apemat'],
                'email' =>  $request['mail'],
                'fotoUser' =>  $request['foto']
            ]);



        return response()->json(['message' => $request->all() ] );
    }

    public function cambiaPass(Request $request)
    {
        $message = "";

        //si es el pass anterior, se puede cambiar
        DB::table('users')
            ->where('id', auth()->user()->id)
            ->update([
                'password' =>  bcrypt($request['passNew'])
            ]);

        $message = 1;

        return response()->json(['message' => 1]);
    }





    public function todosPerfil(Request $request)
    {
        //buscamos los to-do de cada tienda
        $formulas = new Formulas();
        $listado = new Listados();
        $data_suc = $this->getListadoTiendas();
        $sucursales = collect($data_suc)->toArray();
        $arrSuc = array();
        for($i = 0;$i<count($sucursales);$i++)
        {
            //buscamos los todo de cada sucursal
            $data_todo = $listado->listaTodoAntiguedad($sucursales[$i]->Id);
                $todos = collect($data_todo)->toArray();
                for($j = 0;$j<count($todos);$j++)
                {
                    $fecha1 = date('Y-m-d', strtotime($todos[$j]->fecha_inicio));
                    $fecha2 = date('Y-m-d');
                    //vemos que tipo de to-do es
                    $tipo = $listado->getTipoTodo($todos[$j]->tipo);
                    $status = $listado->getStatus($todos[$j]->Status);


                    $arrSuc[] = array(
                        'Id' => $todos[$j]->Id,
                        'campos_Id' => $todos[$j]->campos_Id,
                        'visitas_Id' => $todos[$j]->visitas_Id,
                        'Imagen' => $todos[$j]->ImagenIni,
                        'Tienda' => $todos[$j]->tienda,
                        'Checklist' => $todos[$j]->checklist,
                        'Categoria' => $todos[$j]->categoria,
                        'Campo' => $todos[$j]->campo,
                        'Descripcion' => $todos[$j]->descripcion,
                        'Tipo' => $tipo,
                        'Status' => $status,
                        'Fecha' => date('d/m/Y', strtotime($fecha1)),
                        'Dias' => $formulas->diferenciaDias($fecha1, $fecha2)
                    );
                }

        }
        $fecha2 = date('Y-m-d');


        //buscamos las actividades del supervisor
        $data_act = DB::table('actividad_empleado')
            ->leftjoin('actividades', function ($join) {
                $join->on('actividades.Id', '=', 'actividad_empleado.actividad_Id');
            })
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'actividades.user_Id');
            })
            ->select("actividades.*", "users.name as Creador")
            ->where('actividad_empleado.empleados_Id', "=", auth()->user()->empleados_Id)
            ->where('actividades.status', "<>", 154)
            ->get();

        $actividades = collect($data_act)->toArray();
        $arrAct = array();
        for($j = 0;$j<count($actividades);$j++)
        {
            $avance = 0;
           $status = $listado->getStatus($actividades[$j]->status);
           //sumamos el total de su avance
            $sql  = DB::table('actividad_comentario')
                ->leftjoin('users', function ($join) {
                    $join->on('users.id', '=', 'actividad_comentario.user_Id');
                })
                ->select('actividad_comentario.*', 'users.name', 'users.fotoUser');
            $sql->where('actividad_comentario.actividad_Id', "=", $actividades[$j]->Id);
            $sql->where('actividad_comentario.user_Id', "=", auth()->user()->id);
            $data = $sql->get();
            $avance = 0;
            $actividades_com = collect($data)->toArray();
            //sumamos las actividades
            for($i = 0;$i<count($actividades_com);$i++)
            {
                $avance = $avance + $actividades_com[$i]->avance;
            }

                $arrAct[] = array(
                    'Id' => $actividades[$j]->Id,
                    'actividad' => $actividades[$j]->actividad,
                    'descripcion' => $actividades[$j]->descripcion,
                    'fhinicio' => date('d/m/Y', strtotime($actividades[$j]->fhinicio)),
                    'foto' => $actividades[$j]->foto,
                    'creador' => $actividades[$j]->Creador,
                    'Status' => $status,
                    'Avance' => $avance,
                    'Dias' => $formulas->diferenciaDias($actividades[$j]->fhinicio, $fecha2)
                );

        }
        return response()->json(['todos' =>  $arrSuc, "actividades" => $arrAct]);
    }

    public function express()
    {
        $listado = new Listados();
        $data['tiendas'] = $this->getListadoTiendas();
        $data['supervisores'] = $listado->listaSupervisores(auth()->user()->id);

        return view('/usuarios/express')->with( $data);
    }

    public function tickets()
    {
        $data['tiendas'] = $this->getListadoTiendas();

        return view('/usuarios/tickets')->with( $data);
    }

    public function setTodoExpress(Request $request)
    {
        $tipo = 2;
        if($request['supervisor'] != 0)
        {
            $tipo = 4;
        }

        DB::table('visitas_todo')->insert(
            [
                'visitas_Id' => 0,
                'campos_Id' => 0,
                'valor' => 0,
                'fecha_inicio' => date('Y-m-d'),
                'descripcion' => $request['descripcion'],
                'fecha_inicio' => date('Y-m-d'),
                'ImagenIni' => $request['imagen'],
                'Status' => 152,
                'tipo'  => $tipo,
                'empleados_Id' => $request['supervisor'],
                'tienda_exp_Id' => $request['tienda']

            ]
        );

        return response()->json(['message' =>  $request->all()]);
    }

    public function getMailEmp(Request $request)
    {

        $sql = DB::table('empleados');

        $sql->where('empleados.Id','=', $request['id']);
        $mail = $sql->first();

        return response()->json(['mail' =>  $mail]);
    }


    public function bajaUser(Request $request)
    {
        $id = $request['user_id'];

       //borramos sus modulos asignadas
        DB::table('users_modulos')
            ->where('users_Id', '=', $id)
            ->delete();

        DB::table('users_colores')
            ->where('users_Id', '=', $id)
            ->delete();
        DB::table('users_widgets')
            ->where('users_Id', '=', $id)
            ->delete();
        DB::table('users_empleados')
            ->where('users_Id', '=', $id)
            ->delete();
        DB::table('users_tiendas')
            ->where('users_Id', '=', $id)
            ->delete();
       //actualizamos el usuario, unicamente para no perder sus asociaciones en viejas visitas
        DB::table('users')
            ->where('id', $id)
            ->update([
                'Activo' =>  0
            ]);


        return response()->json(['mail' =>  "OK"]);
    }

    public function saveUsuario(Request $request)
    {

        $sql = DB::table('users');

        $sql->where('users.email','=', $request['mail']);
        $mail = $sql->count();
        $empleado = DB::table('empleados')->where('empleados.Id', '=', $request['emp_id'] )->first();
        if($mail == 0)
        {
            //buscamos la foto del empleado


            DB::table('users')->insert([
                [
                    'empresas_Id' =>  auth()->user()->empresas_Id,
                    'empleados_Id' =>  $request['emp_id'],
                    'name' =>  $request['name'],
                    'email' =>  $request['mail'],
                    'password' =>  bcrypt($request['pass']),
                    'Activo' =>  1,
                    'fotoEmp' => auth()->user()->fotoEmp,
                    'fotoUser' => $empleado->foto
                ]
            ]);
        }

        return response()->json(['mail' =>  $mail]);
    }

    public function saveEditEmp(Request $request)
    {
        DB::table('users')
            ->where('id', $request['id_emp'])
            ->update([
                'password' =>  bcrypt($request['pass'])
            ]);


        return response()->json(['mail' =>  "OK"]);
    }

    public function addModEmpleado(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('users_modulos')->insert([
                [
                    'users_Id' => $request['user_id'],
                    'modulos_Id' => $request['modulos_id']
                ]
            ]);
        }else
        {
            DB::table('users_modulos')
                ->where('users_Id', '=', $request['user_id'])
                ->where('modulos_Id', '=', $request['modulos_id'])
                ->delete();
        }


        return response()->json(['message' => $request['type']] );
    }

    public function addWidEmpleado(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('users_widgets')->insert([
                [
                    'users_Id' => $request['user_id'],
                    'widgets_Id' => $request['widgets_id']
                ]
            ]);
        }else
        {
            DB::table('users_widgets')
                ->where('users_Id', '=', $request['user_id'])
                ->where('widgets_Id', '=', $request['widgets_id'])
                ->delete();
        }


        return response()->json(['message' => $request['type']] );
    }

    public function addSucEmpleado(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('users_tiendas')->insert([
                [
                    'users_Id' => $request['user_id'],
                    'tiendas_Id' => $request['tiendas_id']
                ]
            ]);
        }else
        {
            DB::table('users_tiendas')
                ->where('users_Id', '=', $request['user_id'])
                ->where('tiendas_Id', '=', $request['tiendas_id'])
                ->delete();
        }


        return response()->json(['message' => $request['type']] );
    }

    public function addEmpEmpleado(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('users_empleados')->insert([
                [
                    'users_Id' => $request['user_id'],
                    'empleados_Id' => $request['empleados_id']
                ]
            ]);
        }else
        {
            DB::table('users_empleados')
                ->where('users_Id', '=', $request['user_id'])
                ->where('empleados_Id', '=', $request['empleados_id'])
                ->delete();
        }



        return response()->json(['message' => $request->all()] );
    }

    public function showEmps(Request $request)
    {

        $sql = DB::table('empleados');

        if($request['puesto'] != '0')
        {
            $sql->where('empleados.puesto_Id','=',$request['puesto']);
        }
        if($request['razon'] != '0')
        {
            $sql->where('empleados.razon_Id','=',$request['razon'] );
        }
        if($request['plaza'] != '0')
        {
            $sql->where('empleados.plaza_Id','=',$request['plaza']);
        }

        $sql->where('empleados.empresas_Id','=',auth()->user()->empresas_Id);
        $sql->where('empleados.activo','=',1);
        $empleados = $sql->get();

        return response()->json(['modEmp' => $empleados] );

    }

    public function showSuc(Request $request)
    {

        $sql = DB::table('tiendas');


        if($request['razon'] != '0')
        {
            $sql->where('tiendas.razon_Id','=',$request['razon'] );
        }
        if($request['plaza'] != '0')
        {
            $sql->where('tiendas.plaza_Id','=',$request['plaza']);
        }

        $sql->where('tiendas.empresas_Id','=',auth()->user()->empresas_Id);
        $sql->where('tiendas.activo','=',1);
        $tiendas = $sql->get();

        return response()->json(['modEmp' => $tiendas] );

    }

    public function addColEmpleado(Request $request)
    {

        DB::table('users_colores')
            ->where('users_Id', '=', $request['user_id'])

            ->delete();

            DB::table('users_colores')->insert([
                [
                    'users_Id' => $request['user_id'],
                    'colores_Id' => $request['colores_id']
                ]
            ]);

        return response()->json(['message' => $request->all()] );
    }


    public function getInfoUsuario(Request $request)
    {

        //obtenemos la información general del usuario
        $sql = DB::table('users')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users.empleados_Id');
            })

            ->select('users.*', 'empleados.nombre', 'empleados.apepat', 'empleados.apemat', 'empleados.mail' );
        $sql->where('users.Id', "=", $request['user_id']);
        $user = $sql->first();


        $sql = DB::table('modulos');
        $modulos = $sql->get();

        $sql = DB::table('widgets');
        $widgets = $sql->get();

        $sql = DB::table('colores');
        $colores = $sql->get();


        $sql = DB::table('tiendas');

        $sql->where('tiendas.empresas_Id','=',auth()->user()->empresas_Id);
        $tiendas = $sql->get();

        $sql = DB::table('empleados');
        $sql->where('empleados.empresas_Id','=',auth()->user()->empresas_Id);
        $empleados = $sql->get();

        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $puestos = $sql->get();

        $mods_selected = DB::table('users_modulos')
            ->where('users_modulos.users_Id','=',$request['user_id'])
            ->get();

        $wids_selected = DB::table('users_widgets')
            ->where('users_widgets.users_Id','=',$request['user_id'])
            ->get();

        $sucs_selected = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })->select('tiendas.*', 'users_tiendas.tiendas_Id')
            ->where('users_tiendas.users_Id','=',$request['user_id'])
            ->get();

        $emps_selected = DB::table('users_empleados')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
            })->select('empleados.*', 'users_empleados.empleados_Id')
            ->where('users_empleados.users_Id','=',$request['user_id'])
            ->get();

        $col_selected = DB::table('users_colores')
            ->where('users_colores.users_Id','=',$request['user_id'])
            ->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();


        return response()->json(['user' =>  $user,
                                'modulos' => $modulos,
                                'widgets' => $widgets,
                                'tiendas' => $tiendas,
                                'empleados' => $empleados,
                                'puestos' => $puestos,
                                'razones' => $razones,
                                'modSel' => $mods_selected,
                                'modWid' => $wids_selected,
                                'modSuc' => $sucs_selected,
                                'modEmp' => $emps_selected,
                                'colores' => $colores,
                                'modCol' => $col_selected]);
    }


    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

}
