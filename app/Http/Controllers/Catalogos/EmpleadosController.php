<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Exports\EmpleadosExport;
use Maatwebsite\Excel\Facades\Excel;

class EmpleadosController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$sql = DB::table('empleados');

        $sql->where('empleados.empresas_Id','=',auth()->user()->empresas_Id);
        $data['empleados'] = $sql->get();*/
        //dd($data['puestos'] );

        $sql  = DB::table('users_empleados')
            ->leftjoin('empleados', function ($join) {
                $join->on('empleados.Id', '=', 'users_empleados.empleados_Id');
            })
            ->select('empleados.*', 'users_empleados.Id as usti_id' );
        $sql->where('users_empleados.users_Id', "=", auth()->user()->id);
        $data['empleados'] = $sql->get();

        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id' );
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $sql->where('tiendas.activo', "=", 1);
        $data['tiendas'] = $sql->get();

        $data['cuestionario'] =   DB::table('cuestionario')
            ->where('cuestionario.empresas_Id','=',auth()->user()->empresas_Id)
            ->where('cuestionario.tipos_cuestionario_Id','=',3)
            ->first();


        return view('/empleados/empleados')->with( $data);
    }

    public function setCuestBaja(Request $request)
    {

        //buscamos si ya existe la prgunta respondida

        $preg =   DB::table('bajas_cuestionario')
            ->where('bajas_cuestionario.pregunta_Id','=',$request['pregunta'])
            ->where('bajas_cuestionario.empleados_Id','=',$request['empleados_Id'])
            ->count();
        //vemos que tipo vamos a capturar
        if($preg == 0)
        {
            //nueva pregunta

            if($request['tipo'] == 0)
            {
                DB::table('bajas_cuestionario')->insert([
                    [
                        'empleados_Id' => $request['empleados_Id'],
                        'pregunta_Id' => $request['pregunta'],
                        'Abierta' => $request['respuesta']
                    ]
                ]);

            }else
            {
                DB::table('bajas_cuestionario')->insert([
                    [
                        'empleados_Id' => $request['empleados_Id'],
                        'pregunta_Id' => $request['pregunta'],
                        'respuesta_Id' => $request['respuesta']
                    ]
                ]);
            }


        }else
        {
            //actualizar preguntas

            if($request['tipo'] == 0)
            {
                DB::table('bajas_cuestionario')
                    ->where('visitas_cuestionario.pregunta_Id','=',$request['pregunta'])
                    ->where('visitas_cuestionario.empleados_Id','=',$request['empleados_Id'])
                    ->update(['Abierta' => $request['respuesta']]);

            }else
            {
                DB::table('bajas_cuestionario')
                    ->where('bajas_cuestionario.pregunta_Id','=',$request['pregunta'])
                    ->where('bajas_cuestionario.empleados_Id','=',$request['empleados_Id'])
                    ->update([ 'respuesta_Id' => $request['respuesta']]);
            }

        }


        return response()->json(['message' => $request['tipo']]);
    }



    public function fotoEmpleado(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Empleado";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Empleados/', $nombre);
        $ruta = $this->path.'Empleados/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function cargaInfoEmpleado(Request $request)
    {
        //obtenemos los puestos, tiendas, razones
        $sql = DB::table('puestos');

        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $puestos = $sql->get();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();

        return response()->json(['puestos' => $puestos, 'razones' => $razones ] );
    }

    public function cargaInfoEmpleadoEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('empleados')
            ->leftjoin('puestos', function ($join) {
                $join->on('puestos.Id', '=', 'empleados.puesto_Id');
            })
        ->leftjoin('plazas', function ($join) {
            $join->on('plazas.Id', '=', 'empleados.plaza_Id');
        })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'empleados.razon_Id');
            })
            ->leftjoin('tiendas_empleados', function ($join) {
                $join->on('tiendas_empleados.empleados_Id', '=', 'empleados.Id')
                    ->where('tiendas_empleados.status', '<>', 0);
            })
            ->leftjoin('tiendas', function ($join) {
                $join->on('tiendas.Id', '=', 'tiendas_empleados.tiendas_Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('empleados.zona_Id', '=', 'zonas.Id');
            })
        ->select('empleados.*', 'plazas.plaza', 'puestos.puesto', 'razon_social.nombre as razon', 'tiendas_empleados.tiendas_Id', 'tiendas.nombre as sucursal', 'zonas.zona', 'zonas.Id as zId' );
        $sql->where('empleados.Id', "=", $request['id']);
        $empleado = $sql->first();



        //obtenemos los puestos, tiendas, razones
        $sql = DB::table('puestos');
        $sql->where('puestos.empresas_Id','=',auth()->user()->empresas_Id);
        $puestos = $sql->get();


        $sql = DB::table('zonas');
        $sql->where('zonas.empresas_Id','=',auth()->user()->empresas_Id);
        $zonas = $sql->get();


        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();

        return response()->json(['puestos' => $puestos, 'razones' => $razones, 'empleado' => $empleado , 'nombre' => $empleado, 'zonas' => $zonas ]);
    }

    public function cargaPlazas(Request $request)
    {

        $sql = DB::table('plazas');

        $sql->where('plazas.razon_social_Id','=',$request['id']);
        $plazas = $sql->get();

        return response()->json(['plazas' => $plazas ] );
    }


    public function cargaTiendas(Request $request)
    {

        $sql = DB::table('tiendas');

        $sql->where('tiendas.plaza_Id','=',$request['id']);
        $tiendas = $sql->get();

        return response()->json(['tiendas' => $tiendas ] );
    }

    public function validaMail(Request $request)
    {

            //contamos las veces que existe el correo electronico
            $countemp = DB::table('empleados')
                ->where('empleados.mail','=',$request['mail'])
                ->where('empleados.empresas_Id','=',auth()->user()->empresas_Id)
                ->count();

            $countuser = DB::table('users')
                ->where('users.email','=',$request['mail'])
                ->where('users.empresas_Id','=',auth()->user()->empresas_Id)
                ->count();

            $tot = $countemp + $countuser;




        return response()->json(['message' => $tot  ] );
    }

    public function addEmpleado(Request $request)
    {

      DB::table('empleados')->insert([
            [
                'empresas_Id' =>  auth()->user()->empresas_Id,
                'clave' =>  $request['clave'],
                'nombre' =>  $request['nombre'],
                'apepat' =>  $request['apepat'],
                'apemat' =>  $request['apemat'],
                'fhingreso' =>  $request['fhingreso'],
                'domicilio' =>  $request['domicilio'],
                'telefono' =>  $request['telefono'],
                'foto' =>  $request['foto'],
                'mail' =>  $request['mail'],
                'curp' =>  $request['curp'],
                'ssocial' =>  $request['nss'],
                'puesto_Id' =>  $request['puesto_Id'],
                'plaza_Id' =>  $request['plaza_Id'],
                'razon_Id' =>  $request['razon_Id'],
                'zona_Id' =>  $request['zona'],
                'activo'   => 1
            ]
        ]);

        $id_empleado = DB::getPdo()->lastInsertId();

        DB::table('users_empleados')->insert([
            [
                'users_Id' =>  auth()->user()->id,
                'empleados_Id' =>  $id_empleado
            ]
        ]);

        if($request['suc'] != 0)
        {
            DB::table('tiendas_empleados')->insert([
                [
                    'tiendas_Id' =>  $request['suc'],
                    'empleados_Id' =>  $id_empleado,
                    'fhasignacion' =>  date('Y-m-d'),
                    'status' => 1
                ]
            ]);


        }




        return response()->json(['message' => $request->all() ] );
    }

    public function updateEmpleado(Request $request)
    {

        DB::table('empleados')
            ->where('Id', $request['id'])
            ->update([
                'empresas_Id' =>  $request['empresas_Id'],
                'clave' =>  $request['clave'],
                'nombre' =>  $request['nombre'],
                'apepat' =>  $request['apepat'],
                'apemat' =>  $request['apemat'],
                'fhingreso' =>  $request['fhingreso'],
                'domicilio' =>  $request['domicilio'],
                'telefono' =>  $request['telefono'],
                'foto' =>  $request['foto'],
                'mail' =>  $request['mail'],
                'curp' =>  $request['curp'],
                'ssocial' =>  $request['nss'],
                'puesto_Id' =>  $request['puesto_Id'],
                'plaza_Id' =>  $request['plaza_Id'],
                'zona_Id' =>  $request['zona'],
                'razon_Id' =>  $request['razon_Id'],
                'activo'   => 1
            ]);

        if($request['suc'] != 0)
        {

            //buscamos si ya existe en la tienda que se va a asignar

            $count = DB::table('tiendas_empleados')
                ->where('tiendas_empleados.empleados_Id','=',$request['id'])
                ->where('tiendas_empleados.tiendas_Id','=',$request['suc'])
                ->count();

            if($count != 1)
            {
                //no existe la asignacion a tienda, primero actualizamos la asignacion en caso de existir
                if($request['suc_ant'] != 0)
                {
                    DB::table('tiendas_empleados')
                        ->where('empleados_Id', $request['id'])
                        ->where('tiendas_Id', $request['suc_ant'])
                        ->update([
                            'status'   => 1,
                            'fhasalida' =>  date('Y-m-d')
                        ]);
                }


                //insertamos la nueva tienda
                DB::table('tiendas_empleados')->insert([
                    [
                        'tiendas_Id' =>  $request['suc'],
                        'empleados_Id' =>  $request['id'],
                        'fhasignacion' =>  date('Y-m-d'),
                        'status' => 1
                    ]
                ]);

            }

            /**/
        }


        return response()->json(['message' => $request->all() ] );
    }

    public function bajaEmpleado(Request $request)
    {
        DB::table('empleados')
            ->where('Id', $request['id'])
            ->update(['activo' => 0
            ]);

        DB::table('tiendas_empleados')
            ->where('empleados_Id', $request['id'])
            ->update(['status' => 0
            ]);
        DB::table('users')
            ->where('empleados_Id', $request['id'])
            ->update(['Activo' => 0
            ]);


        return response()->json(['mesagge' => "OK" ] );
    }

    public function export()
    {
        $data['id'] =  19;
        return Excel::download(new EmpleadosExport($data), 'empleados.xlsx');
    }
}
