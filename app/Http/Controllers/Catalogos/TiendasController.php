<?php

namespace App\Http\Controllers\Catalogos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Exports\TiendasExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Listados;


class TiendasController extends Controller
{

    protected $path = 'uploads/'; //path para pruebas locales
    protected $pathUploadCli = '/home/hdammx/public_html/VentumClientes/uploads/'; //path para copia en clientes
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['tiendas'] = $this->getListadoTiendas();
        $data['existe'] = "NA";
        return view('/tiendas/tiendas')->with( $data);
    }

    public function usertienda()
    {
        $data['existe'] = "NA";
        return view('/usuariotienda/tienda/tiendas')->with( $data);
    }



    public function getTiendas(Request $request)
    {
        $sql  = DB::table('users_tiendas')
            ->leftjoin('tiendas', function ($join) {
            $join->on('tiendas.Id', '=', 'users_tiendas.tiendas_Id');
            })
            ->select('tiendas.*', 'users_tiendas.Id as usti_id' );
        $sql->where('users_tiendas.users_Id', "=", auth()->user()->id);
        $tiendas = $sql->get();
        $tiendas_count = $sql->count();

        return response()->json(['tiendas' => $tiendas, 'no' =>$tiendas_count ]);
    }

    public function getUbicacion(Request $request)
    {
        $sql  = DB::table('ubicaciones');
        $sql->where('ubicaciones.CP', "=", $request['cp']);
        $ubicacion = $sql->first();


        return response()->json(['ubicacion' => $ubicacion ]);
    }

    public function fotoTienda(Request $request)
    {

        $file = $request->file('photo');

        $nombre = "_Foto_Tienda";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Tiendas/', $nombre);
        $ruta = $this->path.'Tiendas/'.$nombre;


        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function cargaInfoTiendaEditar(Request $request)
    {
        //obtenemos la información general del empleado
        $sql = DB::table('tiendas')
            ->leftjoin('plazas', function ($join) {
                $join->on('plazas.Id', '=', 'tiendas.plaza_Id');
            })
            ->leftjoin('razon_social', function ($join) {
                $join->on('razon_social.Id', '=', 'tiendas.razon_Id');
            })
            ->leftjoin('zonas', function ($join) {
                $join->on('zonas.Id', '=', 'tiendas.zona');
            })
            ->select('tiendas.*', 'plazas.plaza',  'razon_social.nombre as razon', 'zonas.zona as nombreZona' );
        $sql->where('tiendas.Id', "=", $request['id']);
        $tienda = $sql->first();


        $sql = DB::table('razon_social');

        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $razones = $sql->get();


        $sql = DB::table('zonas');

        $sql->where('zonas.empresas_Id','=',auth()->user()->empresas_Id);
        $zonas = $sql->get();

        $sql  = DB::table('tiendas_empleados')
            ->leftjoin('empleados', function ($join) {
                $join->on('tiendas_empleados.empleados_Id', '=', 'empleados.Id');
            })
            ->select('empleados.*', 'tiendas_empleados.fhasignacion' );
        $sql->where('tiendas_empleados.tiendas_Id', "=", $request['id']);
        $sql->where('tiendas_empleados.status', "=", 1);
        $empleados = $sql->get();

        $sql = DB::table('widgets_tienda');
        $widgets = $sql->get();

        $wids_selected = DB::table('users_widgets_tiendas')
            ->where('users_widgets_tiendas.tiendas_Id','=',$request['id'])
            ->get();


        return response()->json(['razones' => $razones,
                                'tienda' => $tienda ,
                                'zonas' => $zonas ,
                                'empleados' => $empleados,
                                 'widgets' => $widgets,
                                'modWid' => $wids_selected]);
    }

    public function addWidTienda(Request $request)
    {
        //generamos la visita
        if($request['type'] == "true")
        {
            DB::table('users_widgets_tiendas')->insert([
                [
                    'tiendas_Id' => $request['tiendas_Id'],
                    'widgets_tiendas_Id' => $request['widgets_id']
                ]
            ]);
        }else
        {
            DB::table('users_widgets_tiendas')
                ->where('tiendas_Id', '=', $request['tiendas_Id'])
                ->where('widgets_tiendas_Id', '=', $request['widgets_id'])
                ->delete();
        }


        return response()->json(['message' => $request['type']] );
    }

    public function updateTienda(Request $request)
    {

        DB::table('tiendas')
            ->where('Id', $request['id'])
            ->update([
                'numsuc' =>  $request['numsuc'],
                'nombre' =>  $request['nombre'],
                'fhapertura' =>  $request['fhapertura'],
                'direccion' =>  $request['direccion'],
                'telefono' =>  $request['telefono'],
                'imagen' =>  $request['foto'],
                'mail' =>  $request['mail'],
                'm2' =>  $request['m2'],
                'emp_ideal' =>  $request['emp_ideal'],
                'plaza_Id' =>  $request['plaza_Id'],
                'razon_Id' =>  $request['razon_Id'],
                'password'  =>  $request['password'],
                'zona'  =>  $request['zona'],
                'CP'  =>  $request['cp'],
                'lat'  =>  $request['lat'],
                'lon'  =>  $request['lng']
            ]);

        DB::table('users')
            ->where('empleados_Id', $request['id'])
            ->update([

                'fotoUser' =>  $request['foto']

            ]);

        //si hay cambio de password validamos en users
        if($request['password'] != "")
        {
            //actualizamos el usuario tipo tienda
            DB::table('users')
                ->where('empleados_Id', $request['id'])
                ->update([

                    'password'  =>  bcrypt($request['password']),
                    'fotoEmp'   => $request['foto'],
                    'name'      => $request['nombre'],
                    'fotoEmp'   => auth()->user()->fotoEmp

                ]);
        }


        return response()->json(['message' => $request->all() ] );
    }

    public function updateFotoTienda(Request $request)
    {

        DB::table('tiendas')
            ->where('Id', $request['tiendas_Id'])
            ->update([

                'imagen' =>  $request['foto'],

            ]);

        DB::table('users')
            ->where('empleados_Id', $request['tiendas_Id'])
            ->update([

                'fotoUser' =>  $request['foto']

            ]);




        return response()->json(['message' => $request->all() ] );
    }

    public function cargaEmp(Request $request)
    {
        $sql  = DB::table('users_empleados')
            ->leftjoin('empleados', function ($join) {
                $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
            })
            ->select('empleados.*');
        $sql->where('users_empleados.users_Id', "=", auth()->user()->id);
        $empleados = $sql->get();

        return response()->json(['empleados' => $empleados ] );
    }

    public function asignaEmp(Request $request)
    {
        //buscamos si el empleado ya está asignado a la tienda
        $sql  = DB::table('tiendas_empleados');
        $sql->where('tiendas_empleados.empleados_Id', "=", $request['id']);
        $sql->where('tiendas_empleados.status', "=", 1);
        $empleado = $sql->count();

        if($empleado == 0)
        {
            //no existe el empleado en tiendas
            DB::table('tiendas_empleados')->insert([
                [
                    'status' => 1,
                    'fhasignacion' => date('Y-m-d H:i:s'),
                    'empleados_Id' => $request['id'],
                    'tiendas_Id' => $request['tiendas_Id']
                ]
            ]);


        }


        return response()->json(['message' => $empleado ] );
    }

    public function bajaSucEmp(Request $request)
    {

        DB::table('tiendas_empleados')
            ->where('empleados_Id', $request['id'])
            ->where('tiendas_Id', $request['tiendas_Id'])
            ->update([
                'status' => 0,
                'fhsalida' => date('Y-m-d H:i:s'),

            ]);

        return response()->json(['message' => "OK" ] );

    }

    public function getFoda(Request $request)
    {

        $foda = DB::table('tiendas_foda')

            ->where('tiendas_Id', $request['id'])
            ->get();

        return response()->json(['foda' => $foda ] );

    }

    public function updateFoda(Request $request)
    {

        DB::table('tiendas_foda')
            ->where('tiendas_Id', $request['tiendas_Id'])
            ->update([
                'fortaleza' => $request['fortaleza'],
                'oportunidad' => $request['oportunidad'],
                'debilidad' => $request['debilidad'],
                'amenaza' => $request['amenaza'],

            ]);

        return response()->json(['message' => "OK" ] );

    }


    public function export()
    {
        $data['id'] =  19;
        return Excel::download(new TiendasExport($data), 'sucursales.xlsx');
    }
}
