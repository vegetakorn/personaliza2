<?php

namespace App\Http\Controllers\Supervisores;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Helpers\Listados;
use App\Helpers\Formulas;
use App\Mail\MailActividad;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use \Swift_SmtpTransport as SmtpTransport;
use App\Exports\ActividadesExport;
use Maatwebsite\Excel\Facades\Excel;


class ActividadesController extends Controller
{
    protected $path = 'uploads/'; //path para pruebas locales
    public function getListadoTiendas()
    {
        $listado = new Listados();

        return $listado->listaTiendas(auth()->user()->id);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listado = new Listados();
        $data['tiendas'] = $this->getListadoTiendas();

        $sql = DB::table('razon_social');
        $sql->where('razon_social.empresas_Id','=',auth()->user()->empresas_Id);
        $data['razones'] = $sql->get();

        //cargamos los supervisores

        $data['puestos'] = $listado->listaSupervisores(auth()->user()->id);

        return view('/actividades/lista')->with( $data);

    }

    public function foto(Request $request)
    {

        $file = $request->file('photo');
        $nombre = "_Foto_Actividad";
        $extension = $file->getClientOriginalExtension();
        $random = str_random(10);
        $fecha = date('Ymdhis');
        $nombre = $random."_".$fecha."-".$nombre.".".$extension;
        $file->move($this->path.'Actividades/', $nombre);
        $ruta = $this->path.'Actividades/'.$nombre;

        return response()->json(['message' =>  $nombre, 'path' =>  $ruta]);
    }

    public function agregaActividad(Request $request)
    {
        //generamos la visita
      DB::table('actividades')->insert([
            [
                'actividad' => $request['actividad'],
                'user_Id' => auth()->user()->id,
                'descripcion' => $request['descripcion'],
                'fhinicio' => date('Y-m-d'),
                'status' => 154,
                'foto'  => $request['foto']
            ]
        ]);

      $id_actividad = DB::getPdo()->lastInsertId();
        return response()->json(['message' => $request->all() , 'idactividad' => $id_actividad] );
    }

    public function asignaSupervisor(Request $request)
    {
        if($request['type'] == "true")
        {
            DB::table('actividad_empleado')->insert([
                [
                    'empleados_Id' => $request['empleados_Id'],
                    'actividad_Id' => $request['actividad_Id']
                ]
            ]);
        }else
        {
            DB::table('actividad_empleado')
                ->where('empleados_Id', '=', $request['empleados_Id'])
                ->where('actividad_Id', '=', $request['actividad_Id'])
                ->delete();
        }

        $conteo =  DB::table('actividad_empleado')
            ->where('actividad_Id', '=', $request['actividad_Id'])
            ->count();

        if($conteo != 0)
        {
            DB::table('actividades')
                ->where('actividades.Id','=',$request['actividad_Id'])
                ->update([ 'status' => 152]);
        }else
        {
            DB::table('actividades')
                ->where('actividades.Id','=',$request['actividad_Id'])
                ->update([ 'status' => 154]);
        }

        //obtenemos los datos de la actividad
        $sql  = DB::table('actividades')
                ->leftjoin('users', function ($join) {
                    $join->on('actividades.user_Id', '=', 'users.id');
                })
                ->select('actividades.*', 'users.name');
        $sql->where('actividades.Id', "=", $request['actividad_Id']);
        $data = $sql->get();
        $actividades = collect($data)->toArray();
        for($i = 0;$i<count($actividades);$i++)
        {
            $sups = array();
            $actId = $actividades[$i]->Id;
            $datasup =   DB::table('actividad_empleado')
                ->leftjoin('empleados', function ($join) {
                    $join->on('empleados.Id', '=', 'actividad_empleado.empleados_Id');
                })
                ->leftjoin('users', function ($join) {
                    $join->on('empleados.Id', '=', 'users.empleados_Id');
                })
                ->select('empleados.*', 'actividad_empleado.Id as acsupId', 'users.id as user_Id')
                ->where('actividad_empleado.empleados_Id', '=', $request['empleados_Id'])
                ->where('actividad_empleado.actividad_Id','=',$actId)->get();
            $supervisores = collect($datasup)->toArray();
            $avance_tot = 0;
            $dias = 0;
            $no_sups = 0;
            for($j = 0;$j<count($supervisores);$j++)
            {
                //guardamos los datos para el envio
                $data['Creador'] = $actividades[$i]->name;
                $data['Supervisor'] = $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat;
                $data['Actividad'] = $actividades[$i]->actividad;
                $data['Descripcion'] = $actividades[$i]->descripcion;
                // Send your message
                Mail::to($supervisores[$j]->mail)->send(new MailActividad($data));

            }
        }
        return response()->json(['message' => $request['type'], 'numsup' => $conteo] );
    }

    public function getActividadFiltro(Request $request)
    {
        $formulas = new Formulas();

        //si elige todos los supervisores carga todas las actividades creadas por el usuario
        /**EN DESUSO**/
       if($request['supervisor'] == 0)
        {
            $sql  = DB::table('actividades')
                ->leftjoin('actividad_empleado', function ($join) {
            $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
            })
            ->leftjoin('empleados', function ($join) {
                $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
            })
            ->leftjoin('users_empleados', function ($join) {
                $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
            })
            ->select('actividades.*');
            $sql->where('users_empleados.users_Id', "=", auth()->user()->id);
            $sql ->whereIn('actividades.status',array(151,152,153));



            $fechas = explode("-",$request['fecha'] );

            $fechaini = explode('/', $fechas[0]);
            $fechafin = explode('/', $fechas[1]);
            $sql ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));

            $sql->groupBy('actividades.Id');
            $data = $sql->get();

            $actividades = collect($data)->toArray();



        }else
        {
            $sql  = DB::table('actividades')
                ->leftjoin('actividad_empleado', function ($join) {
                    $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                })
                ->leftjoin('empleados', function ($join) {
                    $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                })
                ->leftjoin('users_empleados', function ($join) {
                    $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
                })
                ->select('actividades.*');
            $sql->where('actividad_empleado.empleados_Id', "=", $request['supervisor']);
            $sql->where('users_empleados.users_Id', "=", auth()->user()->id);

            if($request['abierto'] == 1 && $request['cerrado'] == 1)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));

            }else if($request['abierto'] == 1 && $request['cerrado'] == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153));

            }else if($request['abierto'] == 0 && $request['cerrado'] == 1)
            {
                $sql ->whereIn('actividades.status',array(154));

            }else if($request['abierto'] == 0 && $request['cerrado'] == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));
            }



            $fechas = explode("-",$request['fecha'] );

            $fechaini = explode('/', $fechas[0]);
            $fechafin = explode('/', $fechas[1]);
            $sql ->whereBetween('actividades.fhinicio',array(trim($fechaini[2])."-".trim($fechaini[1])."-".trim($fechaini[0]),trim($fechafin[2])."-".trim($fechafin[1])."-".trim($fechafin[0])));
            $sql->groupBy('actividades.Id');
            $data = $sql->get();

            $actividades = collect($data)->toArray();
        }

        if(count($actividades) != 0)
        {
            for($i = 0;$i<count($actividades);$i++)
            {
                $sups = array();
                $actId = $actividades[$i]->Id;
                $datasup =   DB::table('actividad_empleado')
                    ->leftjoin('empleados', function ($join) {
                        $join->on('empleados.Id', '=', 'actividad_empleado.empleados_Id');
                    })
                    ->leftjoin('users', function ($join) {
                        $join->on('empleados.Id', '=', 'users.empleados_Id');
                    })
                    ->select('empleados.*', 'actividad_empleado.Id as acsupId', 'users.id as user_Id')
                    ->where('actividad_empleado.actividad_Id','=',$actId)->get();
                $supervisores = collect($datasup)->toArray();
                $avance_tot = 0;
                $dias = 0;
                $no_sups = 0;
                for($j = 0;$j<count($supervisores);$j++)
                {
                    $avance = 0;
                    //aqui se va a calcular el avance por usuario
                    //sumamos el total de su avance
                    $sql  = DB::table('actividad_comentario');
                    $sql->where('actividad_comentario.actividad_Id', "=", $actividades[$i]->Id);
                    $sql->where('actividad_comentario.user_Id', "=", $supervisores[$j]->user_Id);
                    $data = $sql->get();
                    $actividades_com = collect($data)->toArray();
                    //sumamos las actividades
                    for($k = 0;$k<count($actividades_com);$k++)
                    {
                        $avance = $avance + $actividades_com[$k]->avance;
                    }
                    $no_sups++;
                    $avance_tot = $avance_tot + $avance;

                    $sups[] = array("Id" => $supervisores[$j]->Id,
                                    "Supervisor" => $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat,
                                    "Avance" => $avance);

                }
                $fecha2 = date('Y-m-d');
                $dias =  $formulas->diferenciaDias($actividades[$i]->fhinicio, $fecha2);
                $prom_avance = round($avance_tot / $no_sups, 2);

                $acts[] = array("Id" => $actividades[$i]->Id,
                    "actividad" => $actividades[$i]->actividad,
                    "descripcion" => $actividades[$i]->descripcion,
                    "foto" => $actividades[$i]->foto,
                    "status" => $actividades[$i]->status,
                    "avance" => $prom_avance,
                    "Dias"   => $dias,
                    "fhinicio"   => date('d/m/Y', strtotime($actividades[$i]->fhinicio)),
                    "supervisores" => $sups);



            }
        }else
        {
            $acts = 0;
        }


        return response()->json(['message' => $acts] );
    }

    public function editarActividad(Request $request)
    {
        $sql  = DB::table('actividades')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'actividades.user_Id');
            })
            ->select('actividades.*', 'users.name', 'users.fotoUser');
        $sql->where('actividades.Id', "=", $request['id']);
        $actividad = $sql->first();

        //determinamos el avance segun el usuario que esta revisando la actividad
        $sql  = DB::table('actividad_comentario')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'actividad_comentario.user_Id');
            })
            ->select('actividad_comentario.*', 'users.name', 'users.fotoUser');
        $sql->where('actividad_comentario.actividad_Id', "=", $request['id']);
        $sql->where('actividad_comentario.user_Id', "=", auth()->user()->id);
        $data = $sql->get();

        $sum_avance_actividades = 0;
        $actividades = collect($data)->toArray();
        //sumamos las actividades
        for($j = 0;$j<count($actividades);$j++)
        {
            $sum_avance_actividades = $sum_avance_actividades + $actividades[$j]->avance;
        }

        return response()->json(['message' => $actividad, 'avance' => $sum_avance_actividades] );
    }

    public function setComentario(Request $request)
    {
        //calculamos el porcentaje a guardar en el avance
        $sum_avance = 0;
        $sum_avance_actividades = 0;
        $sql  = DB::table('actividad_comentario')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'actividad_comentario.user_Id');
            })
            ->select('actividad_comentario.*', 'users.name', 'users.fotoUser');
        $sql->where('actividad_comentario.actividad_Id', "=", $request['id']);
        $sql->where('actividad_comentario.user_Id', "=", auth()->user()->id);
        $avance = $sql->count();
        $data = $sql->get();

        if($avance == 0)
        {
            $sum_avance = $request['avance'];
        }else
        {
            $actividades = collect($data)->toArray();
            //sumamos las actividades
            for($j = 0;$j<count($actividades);$j++)
            {
                $sum_avance_actividades = $sum_avance_actividades + $actividades[$j]->avance;
            }

            $sum_avance = $request['avance'] - $sum_avance_actividades;

        }

        DB::table('actividad_comentario')->insert([
            [
                'comentario' => $request['comentario'],
                'user_Id' => auth()->user()->id,
                'actividad_Id' => $request['id'],
                'avance' => $sum_avance,
                'fhcomentario' => date('Y-m-d')
            ]
        ]);

        return response()->json(['message' => "OK"] );
    }

    public function getComentario(Request $request)
    {
        $sql  = DB::table('actividad_comentario')
            ->leftjoin('users', function ($join) {
                $join->on('users.id', '=', 'actividad_comentario.user_Id');
            })
            ->select('actividad_comentario.*', 'users.name', 'users.fotoUser');
        $sql->where('actividad_comentario.actividad_Id', "=", $request['id']);
        $comentarios = $sql->get();

        return response()->json(['message' => $comentarios] );
    }

    public function updComentario(Request $request)
    {
        DB::table('actividades')
            ->where('actividades.Id','=',$request['id'])

            ->update([ 'status' => $request['status']]);

        return response()->json(['message' => "OK"] );
    }

    public function valDirector(Request $request)
    {
       $director =  DB::table('empleados')
            ->leftjoin('puestos', function ($join) {
                $join->on('puestos.Id', '=', 'empleados.puesto_Id');
            })
           ->leftjoin('emp_puestos', function ($join) {
               $join->on('emp_puestos.puestos_Id', '=', 'puestos.Id');
           })
           ->leftjoin('tipo_puesto', function ($join) {
               $join->on('tipo_puesto.Id', '=', 'emp_puestos.tipo_puesto_Id');
           })
            ->select('puestos.puesto', 'puestos.Id', 'tipo_puesto.tipo')
            ->where('empleados.Id','=',auth()->user()->empleados_Id)
           ->where('tipo_puesto.Id','=',2)
           ->first();

        return response()->json(['message' => $director->tipo] );
    }

    public function export($fecha_ini, $fecha_fin, $supervisor, $abierto, $cerrado)
    {
        $formulas = new Formulas();

        $fecha_ini = $formulas->fechaBD($fecha_ini);
        $fecha_fin = $formulas->fechaBD($fecha_fin);

        //si elige todos los supervisores carga todas las actividades creadas por el usuario
        if($supervisor == 0)
        {
            $sql  = DB::table('actividades')
                ->leftjoin('actividad_empleado', function ($join) {
                    $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                })
                ->leftjoin('empleados', function ($join) {
                    $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                })
                ->leftjoin('users_empleados', function ($join) {
                    $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
                })
                ->select('actividades.*');
            $sql->where('users_empleados.users_Id', "=", auth()->user()->id);
            if($abierto == 1 && $cerrado == 1)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));

            }else if($abierto == 1 && $cerrado == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153));

            }else if($abierto == 0 && $cerrado== 1)
            {
                $sql ->whereIn('actividades.status',array(154));

            }else if($abierto == 0 && $cerrado == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));
            }

            $sql ->whereBetween('actividades.fhinicio',array($fecha_ini,$fecha_fin));

            $sql->groupBy('actividades.Id');
            $data = $sql->get();

            $actividades = collect($data)->toArray();



        }else
        {
            $sql  = DB::table('actividades')
                ->leftjoin('actividad_empleado', function ($join) {
                    $join->on('actividad_empleado.actividad_Id', '=', 'actividades.Id');
                })
                ->leftjoin('empleados', function ($join) {
                    $join->on('actividad_empleado.empleados_Id', '=', 'empleados.Id');
                })
                ->leftjoin('users_empleados', function ($join) {
                    $join->on('users_empleados.empleados_Id', '=', 'empleados.Id');
                })
                ->select('actividades.*');
            $sql->where('actividad_empleado.empleados_Id', "=", $supervisor);
            $sql->where('users_empleados.users_Id', "=", auth()->user()->id);
            if($abierto == 1 && $cerrado == 1)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));

            }else if($abierto == 1 && $cerrado == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153));

            }else if($abierto == 0 && $cerrado== 1)
            {
                $sql ->whereIn('actividades.status',array(154));

            }else if($abierto == 0 && $cerrado == 0)
            {
                $sql ->whereIn('actividades.status',array(151,152,153, 154));
            }
            $sql ->whereBetween('actividades.fhinicio',array($fecha_ini,$fecha_fin));
            $sql->groupBy('actividades.Id');
            $data = $sql->get();

            $actividades = collect($data)->toArray();
        }

        if(count($actividades) != 0)
        {
            for($i = 0;$i<count($actividades);$i++)
            {
                $sups = array();
                $actId = $actividades[$i]->Id;
                $datasup =   DB::table('actividad_empleado')
                    ->leftjoin('empleados', function ($join) {
                        $join->on('empleados.Id', '=', 'actividad_empleado.empleados_Id');
                    })
                    ->leftjoin('users', function ($join) {
                        $join->on('empleados.Id', '=', 'users.empleados_Id');
                    })
                    ->select('empleados.*', 'actividad_empleado.Id as acsupId', 'users.id as user_Id')
                    ->where('actividad_empleado.actividad_Id','=',$actId)->get();
                $supervisores = collect($datasup)->toArray();
                $avance_tot = 0;
                $dias = 0;
                $no_sups = 0;
                for($j = 0;$j<count($supervisores);$j++)
                {
                    $avance = 0;
                    //aqui se va a calcular el avance por usuario
                    //sumamos el total de su avance
                    $sql  = DB::table('actividad_comentario');
                    $sql->where('actividad_comentario.actividad_Id', "=", $actividades[$i]->Id);
                    $sql->where('actividad_comentario.user_Id', "=", $supervisores[$j]->user_Id);
                    $data = $sql->get();
                    $actividades_com = collect($data)->toArray();
                    //sumamos las actividades
                    for($k = 0;$k<count($actividades_com);$k++)
                    {
                        $avance = $avance + $actividades_com[$k]->avance;
                    }
                    $no_sups++;
                    $avance_tot = $avance_tot + $avance;

                    $sups[] = array("Id" => $supervisores[$j]->Id,
                        "Supervisor" => $supervisores[$j]->nombre." ".$supervisores[$j]->apepat." ".$supervisores[$j]->apemat,
                        "Avance" => $avance);

                }
                $fecha2 = date('Y-m-d');
                $dias =  $formulas->diferenciaDias($actividades[$i]->fhinicio, $fecha2);
                $prom_avance = round($avance_tot / $no_sups, 2);

                switch ( $actividades[$i]->status)
                {
                    case 151:
                    case 152:
                    case 153:
                        $Estatus = "Abierto";
                        break;
                    case 154:
                        $Estatus = "Cerrado";
                        break;
                }

                    $acts[] = array("Id" => $actividades[$i]->Id,
                        "actividad" => $actividades[$i]->actividad,
                        "descripcion" => $actividades[$i]->descripcion,
                        "foto" => $actividades[$i]->foto,
                        "status" => $actividades[$i]->status,
                        "status_name" => $Estatus,
                        "avance" => $prom_avance,
                        "Dias"   => $dias,
                        "fhinicio"   => date('d/m/Y', strtotime($actividades[$i]->fhinicio)),
                        "supervisores" => $sups);

            }
        }else
        {
            $acts = 0;
        }

        return  Excel::download(new ActividadesExport($acts), 'actividades.xlsx');
    }
}
