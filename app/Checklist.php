<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Checklist extends Model
{
    use Notifiable;
    public $timestamps = false;
    //
    protected $table = 'checklist';

    protected $fillable = [
        'empresas_Id', 'nombre','descripcion','activo','evaluacion_tot',
    ];

}
