@extends('adminlte::pagesuc')

@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Widgets Configurados </h1>
@stop

@section('content')
    <!--widget foto-->
    <div  class="foto">

    </div>
    <!--widget FODA-->
    <div  class="foda">

    </div>








@stop
@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
    <style>

        /*a:hover {
            background-color: yellow;
        }*/

        .barcanvas {
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;


        }

    </style>


    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap  -->
    <script src="{{ asset('bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{ asset('bower_components/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('bower_components/chart.js/dist/Chart.js')}}"></script>
    <!-- ChartJS-->
    <script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.js')}}"></script>
    <script src="{{ asset('bower_components/chart.js/samples/utils.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

    <script>



        function setFechaPdf(fecha) {
            var str = fecha;
            var res = str.split("-");
            return res;
        }

        function setFechaFormat(fecha) {
            var str = fecha;
            var res = str.split("/");
            return res[0]+'-'+res[1]+'-'+res[2];
        }


        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config3 = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        15,
                        10,
                        7,
                        6,
                        2,
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green,
                        window.chartColors.blue,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    "Fachada Sucia",
                    "Servicio Postventa",
                    "Personal con Gafete",
                    "Baños Limpios",
                    "Libre de Obstáculos"
                ]
            },
            options: {
                responsive: true
            }
        };



        document.getElementById('randomizeData').addEventListener('click', function() {
            config.data.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });
            });

            window.myPie.update();
        });

        var colorNames = Object.keys(window.chartColors);



    </script>



    @routes
    <script>
        $(document).ready(function() {
            var tiendas_Id = '{{auth()->user()->empleados_Id}}';
            getFoda(tiendas_Id);

            getFoto();


        } );




        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '').replace(',', '').replace(' ', '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        }

        function getFoto()
        {
            var url = '{{route('regFotoTienda')}}';
            var src = '{{ asset('uploads/Tiendas/')}}';
             src += '/'+'{{auth()->user()->fotoUser}}';
            var text ="";

            text +=
                '          <div class="box box-default">\n' +
                '            <div class="box-header with-border">\n' +
                '              <h3 class="box-title">Foto de la Sucursal</h3>\n' +
                '            </div>\n' +
                '            <div class="box-body">';

            text += '<form action="'+url+'" method="post" style="display: none" id="avatarForm">\n' +
                '                        <input type="file" id="avatarInput" name="photo">\n' +
                '                    </form>\n' +
                '                    <div class="rutaFoto">\n' +
                '                        <img onclick="setFoto()"   class="img-responsive pad" id="avatarImage" src="'+src+'" alt="User profile picture">\n' +
                '\n' +
                '                    </div>';

            text += ' </div>\n' +
                '          </div>\n' +
                '      ';

            $('.foto').html( text );

        }

        function getFoda(id)
        {


            var url = '{{route('getFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  id }
            })
                .done(function(msg){
                    console.log(msg['foda']);
                    var text ="";

                    text += ' <div class="box box-default direct-chat direct-chat-default">\n' +
                        '        <div class="box-header with-border">\n' +
                        '            <h3 class="box-title">FODA</h3>\n' +
                        '\n' +
                        '            <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">';

                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-warning">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Fortalezas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="fortaleza"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['fortaleza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-primary">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Oportunidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="oportunidad" style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['oportunidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';
                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-success">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Debilidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="debilidad"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['debilidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-danger">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Amenazas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="amenaza"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['amenaza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';

                    text += '  </div>\n' +
                        '        <!-- /.box-body -->\n' +
                        '        <div class="box-footer">\n' +
                        '            <form action="#" method="post">\n' +
                        '                <div class="input-group">\n' +
                        '                    <span class="input-group-btn">\n' +
                        '                            <button onclick="updateFODA()" type="button" class="btn btn-primary btn-flat pull-right">Actualizar</button>\n' +
                        '                          </span>\n' +
                        '                </div>\n' +
                        '            </form>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-footer-->\n' +
                        '    </div>';
                    $('.foda').html( text );

                });


        }

        function updateFODA()
        {
            var tiendas_Id = '{{auth()->user()->empleados_Id}}';
            var url = '{{route('updateFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:  tiendas_Id,
                    fortaleza:document.getElementById('fortaleza').value,
                    oportunidad: document.getElementById('oportunidad').value,
                    debilidad: document.getElementById('debilidad').value,
                    amenaza: document.getElementById('amenaza').value }
            })
            .done(function(msg) {
                console.log(msg['foda']);
                bootbox.alert("¡FODA actualizado con éxito!")
            });

        }

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                  //  $('.foto_tienda').html('<input type="hidden" id="foto_tienda" value="' + msg['message'] + '" >')



                    updateTienda(msg['message']);


                });
            });
        }

        function updateTienda(foto)
        {
            var tiendas_Id = '{{auth()->user()->empleados_Id}}';
            var url = '{{route('updateFotoTienda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:  tiendas_Id,
                    foto: foto }
            })
                .done(function(msg) {

                });
        }

    </script>



@stop