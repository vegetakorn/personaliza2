@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Puestos</h1>
@stop

@section('content')

    <div class="box" id="listaPuestos">
        <div class="box-header">
            <button type="button" onclick="addPuesto()" class="btn  btn-success pull-right">Agregar Puesto</button>
        </div>
        <!-- /.box-header


         -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Puestos</th>

                </tr>
                </thead>


                <tbody>

                @foreach($puestos as $puesto)

                    <tr>
                        <td >
                            <!-- chat item -->
                            <div class="item">
                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right">

                                            <a   ><img data-toggle="tooltip" onclick="editar({{$puesto->Id}})" title="Editar" width="40" height="40" src="{{ asset('images/buttons/pencil.png')}}"  alt="Icono"></a >
                                            <a    ><img onclick="baja({{$puesto->Id}})" data-toggle="tooltip" title="Eliminar Puesto" width="40" height="40" src="{{ asset('images/buttons/stop.png')}}"  alt="Icono"></a >


                                        </small>


                                    </a>
                                    <a href="#" class="name">
                                        <h3>{{$puesto->clave}}&nbsp;<strong>{{$puesto->puesto}}</strong></h3>
                                    </a>
                                    {{$puesto->descripcion}}
                                </p>

                                <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="box box-warning" id="agregaPuesto">
        <div class="box-header with-border">
            <h3 class="box-title">Crear Puesto</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form">
                <!-- text input -->
                <div class="form-group">
                    <label>Clave</label>
                    <input id="clave" type="text" maxlength="3" class="form-control" placeholder="Clave del Puesto">
                </div>
                <div class="form-group">
                    <label>Nombre</label>
                    <input id="nombre" type="text" class="form-control" placeholder="Nombre del Puesto" >
                </div>
                <!-- textarea -->
                <div class="form-group">
                    <label>Descripción</label>
                    <textarea id="descripcion" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>

                <button type="button"  onclick="save()" class="btn  btn-success pull-right">Agregar</button>
                <button type="button" onclick="cancel()" class="btn  btn-danger pull-right">Cancelar</button>

            </form>


        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-primary" id="agregaInfo">
        <div class="box-header with-border">
            <h3 class="box-title">Características del Puesto</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form">
                <!-- text input -->

                <!-- checkbox -->
                <div class="form-group">
                    <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Tipo de Puesto</label>
                    <div class="checkbox">
                        <label>
                            <input  onclick="setTipo(2)" type="checkbox">
                            Director
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input onclick="setTipo(1)" type="checkbox">
                            Supervisor
                        </label>
                    </div>

                    <div class="checkbox">
                        <label>
                            <input onclick="setTipo(3)" type="checkbox" >
                            Auditor
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input onclick="setTipo(4)" type="checkbox" >
                            Gerente
                        </label>
                    </div>
                </div>
                <button type="button" onclick="cancel()" class="btn  btn-success pull-right">Terminar</button>

            </form>
        </div>
        <!-- /.box-body -->
        <div class="id_puesto" >
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        </div>
    </div>

    <div class="editar" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>

        $(document).ready(function() {

            $("#agregaPuesto").hide();
            $("#agregaInfo").hide();

            $('[data-toggle="tooltip"]').tooltip();

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })



        function addPuesto()
        {
            //alert('OK')
            $("#listaPuestos").hide();
            $("#agregaPuesto").show();

        }

        function setTipo(tipo)
        {
            //alert(document.getElementById('dir').value)


            url = '{{route('setTipo')}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {puesto_id: document.getElementById('id_puesto').value, tipo: tipo   }
            })
                .done(function(msg){


                });
        }


        function cancel()
        {
            //alert('OK')
            url = '{{route('puestos.lista')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }

        function editar(puesto_id)
        {
            $("#listaPuestos").hide();
           // alert(puesto_id)
            var url = '{{route('editPuesto')}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {id: puesto_id  }
            })
                .done(function(msg) {

                    console.log(msg['tipos']);

                    //msg['puesto']['puesto']
                    $('.id_puesto').html('<input type="hidden" id="id_puesto" value="' + msg['puesto']['Id'] + '" >')
                    var text = "";

                    var tipos = "";


                    for (var i = 1; i < 5; i++) {

                        var nombre = ""

                        switch (i) {
                            case 1:
                                nombre = 'Supervisor'
                                break;
                            case 2:
                                nombre = 'Director';
                                break;
                            case 3:
                                nombre = 'Auditor';
                                break;
                            case 4:
                                nombre = 'Gerente'
                                break;
                        }

                        tipos += '                    <div class="checkbox">\n' +
                            '                        <label>\n' +
                            '                            <input id="chkBox' + i + '"   onclick="setTipo(' + i + ')" type="checkbox">\n' +
                            '                            ' + nombre + '\n' +
                            '                        </label>\n' +
                            '                    </div>\n';
                    }





                    text += '<div class="box box-warning" id="editauesto">\n' +
                        '        <div class="box-header with-border">\n' +
                        '            <h3 class="box-title">Editar Puesto</h3>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">\n' +
                        '            <form role="form">\n' +
                        '                <!-- text input -->\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Clave</label>\n' +
                        '                    <input id="claveu" type="text" value="'+msg['puesto']['clave']+'" maxlength="3" class="form-control" placeholder="Clave del Puesto">\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Nombre</label>\n' +
                        '                    <input id="nombreu" type="text" value="'+msg['puesto']['puesto']+'" class="form-control" placeholder="Nombre del Puesto" >\n' +
                        '                </div>\n' +
                        '                <!-- textarea -->\n' +
                        '                <div class="form-group">\n' +
                        '                    <label>Descripción</label>\n' +
                        '                    <input id="descripcionu" value="'+msg['puesto']['descripcion']+'" class="form-control" rows="3" placeholder="Describe el puesto ...">\n' +
                        '                </div>\n' +
                        '\n' +
                        ' <!-- checkbox -->\n' +

                        '                <div class="form-group">\n' +
                        '                    <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Tipo de Puesto</label>\n' +
                         tipos +
                        '                </div>'+
                            ' <div class="form-group">\n' +
                        '            <label class="control-label" for="inputSuccess"><i class="fa fa-check"></i> Asigna checklist</label>\n' +
                        '            <div class="timeline-body eligeChk"> <form>\n' +
                        '\n' +
                        '            </form></div>\n' +
                        '\n' +
                        '        </div>'+
                        '                <button type="button"  onclick="update('+msg['puesto']['Id']+')" class="btn  btn-success pull-right">Actualizar</button>\n' +
                        '                <button type="button" onclick="cancel()" class="btn  btn-danger pull-right">Cancelar</button>\n' +
                        '\n' +
                        '            </form>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-body -->\n' +
                        '    </div>';
                    $('.editar').html( text )

                    var $chk = "";
                    for(var i = 0; i < msg['tipos'].length; i++) {



                        switch(msg['tipos'][i]['tipo_puesto_Id'])
                        {
                            case 1:
                                 $chk= $('#chkBox1');

                                break;
                            case 2:
                                $chk= $('#chkBox2');
                                break;
                            case 3:
                                $chk= $('#chkBox3');
                                break;
                            case 4:
                                $chk= $('#chkBox4');
                                break;
                        }

                        $chk.prop('checked', true);


                    }
                    console.log(msg['checklist']);
                    /**
                     *
                   **/
                    var txtChk = "";
                    for(var i = 0; i < msg['checklist'].length; i++) {

                        var chk = msg['checklist'][i]['Id'];
                        var nombreChk = msg['checklist'][i]['nombre'];

                        txtChk += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input id="chk'+chk+'" onclick="setChecklist('+chk+')" type="checkbox" >\n' +
                            '                     '+nombreChk+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }

                    $('.eligeChk').html( txtChk )


                    //$('#chk3').prop('checked', true)

                    var chkSel = "";
                    for(var i = 0; i < msg['chkSel'].length; i++) {

                       chkSel = $('#chk'+msg['chkSel'][i]['checklist_Id'])
                        chkSel.prop('checked', true);


                    }


                });
        }

        function save()
        {
            if(document.getElementById('nombre').value != "")
            {


                url = '{{route('addPuesto')}}';
                // alert('Muestra despues')

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {clave: document.getElementById('clave').value, nombre: document.getElementById('nombre').value, descripcion: document.getElementById('descripcion').value   }
                })
                    .done(function(msg){

                        console.log(msg['idpuesto']);

                        $('.id_puesto').html( '<input type="hidden" id="id_puesto" value="'+msg['idpuesto']+'" >' )

                        $("#agregaPuesto").hide();

                        $("#agregaInfo").show();

                });

            }else
            {
                bootbox.alert('Debes nombrar al puesto')
            }

        }

        function update(id)
        {
            if(document.getElementById('nombreu').value != "")
            {


                url = '{{route('updatePuesto')}}';
              //alert(id)

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {clave: document.getElementById('claveu').value,
                           nombre: document.getElementById('nombreu').value,
                           descripcion: document.getElementById('descripcionu').value ,
                           puestos_Id: id }
                })
                    .done(function(msg){


                        var url_go = '{{route('puestos.lista')}}';//'visitas/final/'+post_id;//
                        window.location.href = url_go;

                    });

            }else
            {
                bootbox.alert('Debes nombrar al puesto')
            }

        }

        function setChecklist(id)
        {
            var chk = $("#chk"+id);


            url = '{{route('addChkPuesto')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {checklist_Id:id, puestos_Id: document.getElementById('id_puesto').value, type:chk.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });

        }

        function baja(id)
        {
            //preguntamos antes si queremos cargar el cuestionario de la visita
            var box = bootbox.confirm({
                title: "Baja de puesto",
                message: "¿Deseas dar de baja el puesto?.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {
                        // alert('realizar cuestionario')
                        var url = '{{route('bajaPuesto')}}'
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {id:  id  }
                        })
                            .done(function(msg){

                                var url = "";
                                url = '{{route('puestos.lista')}}';//'visitas/final/'+post_id;//
                                window.location.href = url;

                            });


                    }else
                    {

                    }

                }
            });
        }

    </script>

@stop