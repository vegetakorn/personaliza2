@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Configuración de envío de correos </h1>
@stop

@section('content')


    <div id="filtro" class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Correos de visita a sucursal </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <strong><i class="fa fa-tachometer margin-r-5"></i> En este panel puedes configurar envíos de correo adicionales los que existen por defecto en una visita de supervisión</strong>
            <hr>
            <div class="radioVal">
                <label >Selecciona puesto:</label>
                <select  onchange="getEmpleados()" class="form-control select2" id="pto" name="puesto" style="width: 100%;">
                    <option  value="0" selected="selected">--Selecciona--</option>
                    @foreach($puestos as $pto)
                        <option value="{{$pto->Id}}"> {{$pto->puesto}}</option>

                    @endforeach

                </select>
                <div class="empleados">

                </div>
                <hr>
                <label >Selecciona razón:</label>

                <select onchange="getPlazas()" class="form-control select2" id="raz" name="razon" style="width: 100%;">
                    <option value="0" selected="selected"><--Todas las razones--></option>
                    @foreach($razones as $raz)
                        <option value="{{$raz->Id}}"> {{$raz->nombre}}</option>

                    @endforeach

                </select>
                <div class="plazas">
                    <input type="hidden" value="0" id="plaza" >
                </div>
                <div class="tiendas">
                    <input type="hidden" value="0" id="suc" >
                </div>
            </div>
            <hr>
            <strong>Envíos para tipo de visita</strong>
            <div class="form-group">
                <label class="">
                    <input type="checkbox" id="boxCheck" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    Checklist
                </label>
                <label class="">

                </label>
                <label class="">
                    <input  type="checkbox" id="boxSeg" class="minimal" checked="" style="position: absolute; opacity: 0;">
                    Seguimiento
                </label>

            </div>

            <hr>
            <button onclick="guardaConfig()"  class="btn btn-primary pull-right margin-bottom">Agregar</button>
        </div>
        <!-- /.box-body -->
    </div>

    <div id="correos" class="box box-primary box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Envíos configurados </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <ul class="products-list product-list-in-box">
                <div  class=" correos">

                </div>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="listadoCorreos" >

    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>
    <script>

        $(document).ready(function() {

            cargaConfiguracionesCorreo()



        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })


        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });



        });


        function getEmpleados()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaEmpleados')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('pto').value  }
            })
                .done(function(msg){
                    console.log(msg['empleados']);
                    var text = "";
                    text += '<b>Empleados del puesto seleccionado</b> <select  class="form-control select2" id="emp" name="empleados" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todos los empleados del puesto--> </option>' ;
                    for(var i = 0; i < msg['empleados'].length; i++)
                    {
                        text += ' <option value="'+msg['empleados'][i]['Id']+'">'+msg['empleados'][i]['nombre']+' '+ msg['empleados'][i]['apepat']+' '+msg['empleados'][i]['apemat']+'</option>\n';
                    }
                    text +=    '                           </select>';
                    $('.empleados').html( text )
                });
        }

        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('raz').value  }
            })
                .done(function(msg){


                    var text = "";

                    text += '<b>Selecciona una plaza:</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Selecciona la sucursal:</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las sucursales--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }

        function guardaConfig()
        {
            var puesto = document.getElementById('pto').value;
            var empleado = document.getElementById('emp').value;
            var razon = document.getElementById('raz').value;
            var plaza = document.getElementById('plaza').value;
            var sucursal = document.getElementById('suc').value;
            var tipo = 0;

            if(document.getElementById('pto').value != 0)
            {
                if(document.getElementById('boxCheck').checked) {
                    tipo = 1;
                }
                if(document.getElementById('boxSeg').checked) {
                    tipo = 2;
                }

                if(document.getElementById('boxSeg').checked || document.getElementById('boxCheck').checked ) {
                    tipo = 0;
                }

                var url = '{{route('agregaEnvio')}}'
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {puesto:puesto, empleado: empleado, razon:razon, plaza:plaza, sucursal:sucursal, tipo: tipo  }
                })
                    .done(function(msg){


                        cargaConfiguracionesCorreo()


                    });
            }else
            {
                bootbox.alert("Debes seleccionar por lo menos un puesto")
            }


        }

        function cargaConfiguracionesCorreo()
        {
            var url = '{{route('cargaConfiguracionesCorreo')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: { }
            })
                .done(function(msg){

                    console.log(msg['correos'])

                    /* */
                    var text ="";
                    var msg_txt = "  ";
                    var msg_raz = "  ";
                    var msg_pla = "  ";
                    var msg_suc = "  ";
                    for(var i = 0; i < msg['correos'].length; i++)
                    {
                        msg_txt = "  ";
                        msg_raz = "  ";
                        msg_pla = "  ";
                        msg_suc = "  ";

                       if(msg['correos'][i]['tipo'] == 0)
                        {
                            msg_txt = " Envio de visitas de checklist y seguimiento"
                        }else   if(msg['correos'][i]['tipo'] == 1)
                        {
                            msg_txt = "Envio de visitas de checklist"
                        }else   if(msg['correos'][i]['tipo'] == 2)
                        {
                            msg_txt = "Envio de visitas de seguimiento"
                        }

                        if(msg['correos'][i]['empleados_Id'] == 0)
                        {
                            msg_txt += " a todos los empleados del puesto " + msg['correos'][i]['puesto'];

                        }else
                        {
                            msg_txt += " al empleado " + msg['correos'][i]['nombre'] + ' ' + msg['correos'][i]['apepat']+' '+ msg['correos'][i]['apemat'] ;
                        }

                        if(msg['correos'][i]['razon_Id'] == 0)
                        {
                            msg_raz += " A todas las razones  ";
                        }else
                        {

                            msg_raz += " A la Razón"+' ' + msg['correos'][i]['razon'];
                        }

                        if(msg['correos'][i]['plaza_Id'] == 0)
                        {
                            msg_pla += " De todas las plazas  ";

                        }else
                        {
                            msg_pla += " De la Plaza"+' ' + msg['correos'][i]['plaza'];
                        }

                        if(msg['correos'][i]['tiendas_Id'] == 0)
                        {
                            msg_suc += " En todas las sucursales  ";

                        }else
                        {
                            msg_suc += " De la Sucursal"+' ' + msg['correos'][i]['tienda'];
                        }
                        text += '<li class="item">\n' +
                            '\n' +
                            '                    <div >\n' +
                            '                        <a onclick="bajaCorreo('+msg['correos'][i]['Id']+')" class="product-title">'+msg['correos'][i]['puesto'] +'\n' +
                            '                            <span class="label label-danger pull-right">quitar</span></a>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_txt+'     \n' +
                            '                        </span>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_raz+'     \n' +
                            '                        </span>\n' +
                            '                    </div>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_pla+'     \n' +
                            '                        </span>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_suc+'     \n' +
                            '                        </span>\n' +
                            '                    </div>\n' +
                            '                </li>';
                    }

                    $('.correos').html( text );
                });
        }

        function bajaCorreo(id)
        {

            //alert(id)
            bootbox.confirm({
                title: "Baja Correo",
                message: "¿Deseas borrar esta configuración de correo?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancelar'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Borrar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    { var url = '{{route('bajaCorreo')}}'
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {id:  id  }
                        })
                            .done(function(msg){

                                bootbox.hideAll();
                                cargaConfiguracionesCorreo()


                            });
                    }
                }
            });

        }


    </script>

@stop