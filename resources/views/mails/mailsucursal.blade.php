
<h2>Información General de Sucursal <strong>{{$data['Sucursal']}} al {{date('d/m/Y', strtotime($data['Last']))}}</strong></h2>
<hr>


<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb">
    <thead>
    <tr >
        <th style="border-right:solid; border-color: #1b6d85; font-weight: bolder; text-align: center" >Presupuesto</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Ventas</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Avance</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Tendencia </th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaVentas'] as $ventas)
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >{{$ventas['Presupuesto']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['Ventas']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['Avance']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$ventas['Tendencia']}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
<hr>
<h4>*250 ticket promedio, 36 Transacciones diarias, 2.5 No. de Art x Ticket</h4>
<hr>
<h2>Signos vitales</h2>
<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb" >
    <thead>
    <tr >

        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center"></th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Mes Actual </th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Mes Anterior</th>


    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaVentas'] as $ventas)
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >Ticket Promedio</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TckPromAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TckPromAnt']}}</td>
        </tr>
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >No. Tickets Promedio</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TicketsAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['TicketsAnt']}}</td>
        </tr>
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >Artículos x Ticket</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['NoTckAct']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$ventas['NoTckAnt']}}</td>
        </tr>
    @endforeach

    </tbody>
</table>
<hr>
<h2>Visitas realizadas a las tiendas</h2>
<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb" width="100%">
    <thead>
    <tr >

        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">#</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Sucursal</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Fecha</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Hora</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Calificación</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Tipo</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Reporte</th>

    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaVisitas'] as $visitas)
        <tr>
            <td style="background-color: #CCCCCC; border-right:solid; border-color: #707572;  text-align: center" >{{$visitas['NumSuc']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$visitas['Sucursal']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$visitas['Fecha']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$visitas['Hora']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%"  >{{$visitas['Calificacion']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%" >{{$visitas['Tipo']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="10%" ><a href="{{$visitas['Reporte']}}">{{$visitas['Label']}}</a></td></td>

        </tr>
    @endforeach

    </tbody>
</table>
<hr>
<h2>To-do's Recientes de las tiendas</h2>

<table style="border: solid; border-color: #001F3F; background-color: #9cc2cb" width="100%">
    <thead>
    <tr >
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Sucursal</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Checklist</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Categoría</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Campo</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Descripción</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Fecha</th>
        <th style="border-right: solid; border-color: #1b6d85;font-weight: bolder; text-align: center">Imagen</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data['TablaTodo'] as $todo)
        <tr>

            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$todo['Sucursal']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center">{{$todo['Checklist']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$todo['Categoria']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="20%" >{{$todo['Campo']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" width="40%" >{{$todo['Descripcion']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" >{{$todo['Fecha']}}</td>
            <td style="background-color: #CCCCCC;border-right: solid; border-color: #707572; text-align: center" ><a href="{{$todo['Imagen']}}"><img src="{{$todo['Imagen']}}" width="150px" height="150px" ></a></td>

        </tr>
    @endforeach

    </tbody>
</table>