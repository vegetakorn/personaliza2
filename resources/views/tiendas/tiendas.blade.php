@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Sucursales</h1>
@stop

@section('content')

    <div class="box "  id="listaTiendas">
        <div class="box-header pull-right">
           <a href="{{route('exportSuc')}}" class="btn btn-social-icon btn-success"><i class="fa fa-file-excel-o"></i></a>
        </div>
        <!-- /.box-header


         -->
        <div class="box-body ">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sucursal</th>


                </tr>
                </thead>


                <tbody>
                @foreach($tiendas as $tienda)

                    <tr>
                        <td >
                            <!-- chat item -->
                            <div class="item">
                                <div style="align-items: center; align-content: center" >
                                    <img src="{{ asset('uploads/Tiendas/'. $tienda->imagen)}}" width="80px" height="80px" class="img-circle" >

                                </div>

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right">
                                            <img  data-toggle="tooltip" title="Signos Vitales" width="40" height="40" src="{{ asset('images/buttons/heart.png')}}"  alt="Icono">


                                            <a href="{{ route('visitas.visita', $tienda->Id) }}"   ><img data-toggle="tooltip" title="Visitar Sucursal" width="40" height="40" src="{{ asset('images/buttons/check.png')}}"  alt="Icono"></a >

                                            <a href="{{ route('visitas.seguimiento', $tienda->Id) }}"   ><img data-toggle="tooltip" title="Seguimiento a Sucursal" width="40" height="40" src="{{ asset('images/buttons/clipboard.png')}}"  alt="Icono"></a >
                                        </small>


                                    </a>
                                    <a onclick="getSucursal({{$tienda->Id}})" class="name">
                                        <h3>{{$tienda->numsuc}}&nbsp;<strong>{{$tienda->nombre}}</strong> <small>(Editar)</small></h3>
                                    </a>

                                    @if($tienda->activo == 1)
                                        <h6>Activa</h6>
                                    @else
                                        <h6>Inactiva</h6>
                                    @endif

                                </p>

                                <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
    <div class="row" id="editarTienda">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <form action="{{route('regFotoTienda')}}" method="post" style="display: none" id="avatarForm">
                        <input type="file" id="avatarInput" name="photo">
                    </form>
                    <div class="rutaFoto">
                        <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="{{ asset('uploads/Tiendas/no_imagen.jpg')}}" alt="User profile picture">

                    </div>

                    <h3 class="profile-username text-center">Foto de Sucursal</h3>

                    <!--<p class="text-muted text-center">Software Engineer</p>-->

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item razones">

                        </li>
                        <li class="list-group-item plazas">
                            <input type="hidden" id="plaza" value="0" >
                        </li>
                        <li class="list-group-item zonas">
                            <input type="hidden" id="zona" value="0" >
                        </li>


                    </ul>


                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active"><a href="#settings" data-toggle="tab">Datos de la Sucursal</a></li>
                    <li ><a href="#empleados" data-toggle="tab">Empleados de la Sucursal</a></li>
                    <li ><a href="#foda" data-toggle="tab">FODA de la Sucursal</a></li>
                    <li><a href="#widgets" data-toggle="tab">Widgets</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputClave" class="col-sm-2 control-label">Clave</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputClave" placeholder="Clave">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName" placeholder="Nombre">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Fecha de Apertura</label>

                                <div class="col-sm-10">
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="datepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">E-mail</label>

                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Teléfono</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputTel" placeholder="Teléfono">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputExperience" class="col-sm-2 control-label">Domicilio</label>

                                <div class="col-sm-10">
                                    <textarea class="form-control" id="inputDom" placeholder="Domicilio"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputExperience" class="col-sm-2 control-label">Código Postal</label>

                                <div class="col-sm-10">
                                    <input  type="text" class="form-control" id="inputCP" onchange="setUbicacion()" placeholder="Usa el código postal para ubicarte en el mapa"></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Empleados Ideal</label>

                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="inputEmp" placeholder="Número de empleados">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Metros Cuadrados</label>

                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="inputM2" placeholder="Metros cuadrados de la sucursal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Contraseña</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPass" placeholder="Contraseña de la sucursal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input" class="col-sm-2 control-label">Ubicación</label>

                                <div class="col-sm-10">

                                    <div id="map" ></div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">




                                </div>
                            </div>
                        </form>

                    </div>
                    <!-- /.tab-pane -->
                    <div class=" tab-pane empleados" id="empleados">

                    </div>
                    <div class=" tab-pane foda" id="foda">

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane showWidgets" id="widgets">

                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->


            </div>
            <div class="btnGuardar pull-right">

                <a  class="btn btn-primary btnGuardar">Guardar</a>
                <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>

    <div class="foto_tienda" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_tienda" value="no_imagen.jpg" >
    </div>

    <div class="lan" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="lan" value="0" >
    </div>
    <div class="lng" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="lng" value="0" >
    </div>

    <div  id="asignaEmp" >
        <div class="box box-default">
            <div class="box-header with-border">
                <h3 class="box-title">Asignar Empleado a Sucursal</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group empleadosUsuario">

                        </div>
                    </div>
                    <!-- /.col -->

                </div>
                <!-- /.row -->
            </div>
            <div class="box-header">
                <div class="btnAsigna">

                </div>

                <button type="button" onclick="cancelAsig()" class="btn  btn-danger pull-right">Cancelar</button>
            </div>
        </div>
    </div>
@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 300px;
            width: 100%;
        }

    </style>

@stop

@section('js')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDigdQtAwnyVRn7wCrver_6pilN-7vfnNY&callback=myMap"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>

    <script>

        function setUbicacion()
        {
            var url = '{{route('getUbicacion')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    cp: document.getElementById('inputCP').value
                }
            })
                .done(function(msg){
                    console.log(msg['ubicacion']);

                    myMapS(msg['ubicacion']['Lat'], msg['ubicacion']['Lng']);
                    $('.lan').html('<input type="hidden" id="lan" value="' + msg['ubicacion']['Lat'] + '" >')
                    $('.lng').html('<input type="hidden" id="lng" value="' + msg['ubicacion']['Lng'] + '" >')

                });
        }

        function myMapS(lat, lng)
        {

            //alert(lat + lng)
            var options = {
                center:{lat: parseFloat(lat), lng: parseFloat(lng)},
                scrollwheel: true,
                zoom: 18
            }

            var myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng)};

            var map= new google.maps.Map(document.getElementById("map"), options);

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                draggable: true
            });



            google.maps.event.addListener(marker, 'position_changed', function()
            {
                var lat_c = marker.getPosition().lat();
                var lng_c = marker.getPosition().lng();

                $('.lan').html('<input type="hidden" id="lan" value="' + lat_c + '" >')
                $('.lng').html('<input type="hidden" id="lng" value="' + lng_c + '" >')

            });

        }

        function myMap()
        {
            var options = {
                center:{lat: 23.6260, lng: -102.5375},
                scrollwheel: true,
                zoom: 5
            }

            var map= new google.maps.Map(document.getElementById("map"), options);
        }

        $(document).ready(function() {

            getFoda();

            var existe = '{{$existe}}';

            if(existe == 0)
            {
                bootbox.alert("La tienda que deseas visitar no la tienes asignada o no existe, favor de verificar");
            }

           // myMap()
            $("#asignaEmp").hide();

           $("#editarTienda").hide();
           // $("#listaTiendas").show();
            $('[data-toggle="tooltip"]').tooltip();
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
            $('#example2').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })

            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_tienda').html('<input type="hidden" id="foto_tienda" value="' + msg['message'] + '" >')


                });
            });
        }

        function cancel()
        {
            //alert('OK')
            url = '{{route('tiendas.tiendas')}}';//'visitas/final/'+post_id;//
            window.location.href = url;
            $("#asignaEmp").hide();

        }

        function getSucursal(id)
        {
            $("#listaTiendas").hide();
            $("#editarTienda").show();
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoTiendaEditar')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id: id   }
            })
                .done(function(msg){

                    var text = "";

                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/uploads/Tiendas/'+msg['tienda']['imagen'];
                    $avatarImage = $('#avatarImage');

                    $avatarImage.attr('src', pathImg)


                    $('.foto_tienda').html('<input type="hidden" id="foto_tienda" value="' + msg['tienda']['imagen'] + '" >')

                    document.getElementById('inputClave').value = msg['tienda']['numsuc'];
                    document.getElementById('inputName').value = msg['tienda']['nombre'];
                    document.getElementById('datepicker').value = msg['tienda']['fhapertura'];
                    document.getElementById('inputDom').value = msg['tienda']['direccion'];
                    document.getElementById('inputTel').value = msg['tienda']['telefono'];
                    document.getElementById('inputEmail').value = msg['tienda']['mail'];
                    document.getElementById('inputPass').value = msg['tienda']['password'];
                    document.getElementById('inputEmp').value = msg['tienda']['emp_ideal'];
                    document.getElementById('inputM2').value = msg['tienda']['m2'];
                    document.getElementById('inputCP').value = msg['tienda']['CP'];

                    myMapS(msg['tienda']['lat'], msg['tienda']['lon'])

                    $('.lan').html('<input type="hidden" id="lan" value="' + msg['tienda']['lat'] + '" >')
                    $('.lng').html('<input type="hidden" id="lng" value="' + msg['tienda']['lon'] + '" >')

                    //document.getElementById('inputClave').value = msg['empleado']['clave'];

                    var raz  = '<option value="0" selected>Sin razón asignada</option>';
                    var plaza  = '<option value="0" selected>Sin plaza asignada</option>';
                    var zona  = '<option value="0" selected>Sin zona asignada</option>';

                    if(msg['tienda']['zona'] != 0) {

                        var txtZona = "";

                        zona = '<option value="'+msg['tienda']['zona']+'" selected>'+msg['tienda']['nombreZona'] +'</option>';


                    }

                    var txtZona = "";
                    txtZona += '<b>Zona</b> <select class="form-control select2" id="zona" name="zona" style="width: 100%;">' +zona+'' ;
                    for(var i = 0; i < msg['zonas'].length; i++)
                    {
                        txtZona += ' <option value="'+msg['zonas'][i]['Id']+'" >'+msg['zonas'][i]['zona']+'</option>\n';
                    }
                    txtZona +=    '                           </select>';
                    $('.zonas').html( txtZona );

                    if(msg['tienda']['razon'] != null)
                    {
                        // alert('No tiene puesto asignado')
                        raz = '<option value="'+msg['tienda']['razon_Id']+'" selected>'+msg['tienda']['razon'] +'</option>';


                        if(msg['tienda']['plaza'] != null)
                        {
                            plaza = '<option value="'+msg['tienda']['plaza_Id']+'" selected>'+msg['tienda']['plaza']+' </option>'
                            var url = '{{route('cargaPlazas')}}'
                            $.ajax({
                                method: 'POST',
                                url: url,
                                data: {id:  msg['tienda']['razon_Id']  }
                            })
                                .done(function(msg){


                                    var text = "";

                                    text += '<b>Plazas</b> <select class="form-control select2" id="plaza" name="plaza" style="width: 100%;">'+plaza+' ';



                                    for(var i = 0; i < msg['plazas'].length; i++)
                                    {
                                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                                    }

                                    text +=    '                           </select>';
                                    $('.plazas').html( text )


                                });


                        }
                    }



                    var razon = "";
                    razon += '<b>Razón</b> <select onchange="getPlazas()" class="form-control select2" id="razon" name="razon" style="width: 100%;">' +raz+'' ;
                    for(var i = 0; i < msg['razones'].length; i++)
                    {
                        razon += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                    }
                    razon +=    '                           </select>';
                    $('.razones').html( razon );

                    //listamos los empleados de la tienda


                    var empleados = "";
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/uploads/Empleados/';
                    var pathBtnBaja = 'http://'+URLdomain+'/images/buttons/stop.png';
                    empleados += '<table id="example2" class="table table-bordered table-striped">\n' +
                        '                            <thead>\n' +
                        '                            <tr>\n' +
                        '                                <th>Empleado</th>\n' +
                        '\n' +
                        '\n' +
                        '                            </tr>\n' +
                        '                            </thead>\n' +
                        '\n' +
                        '\n' +
                        '                            <tbody>' +
                        ' <div class="box-header">\n' +
                        '       <button type="button" onclick="asignaEmp('+id+')" class="btn  btn-success pull-right">Asignar Empleado</button>\n' +
                        ' </div>';

                    for(var i = 0; i < msg['empleados'].length; i++)
                    {
                        empleados += ' ' +

                            '' +
                            ' <td >\n' +
                            '                            <!-- chat item -->\n' +
                            '                            <div class="item">\n' +
                            '\n' +
                            '\n' +
                            '                                <img src="'+pathImg+msg['empleados'][i]['foto']+'" width="80px" height="80px" class="img-circle" >\n' +
                            '\n' +
                            '                                <p class="message">\n' +
                            '                                    <a href="#" class="name">\n' +
                            '                                        <small class="text-muted pull-right">\n' +
                            '\n' +
                            '\n' +
                            '\n' +
                            '                                            <a    ><img onclick="bajaSuc('+msg['empleados'][i]['Id']+','+id+')" data-toggle="tooltip" title="Baja de Tienda" width="40" height="40" src="'+pathBtnBaja+' "  alt="Icono"></a >\n' +
                            '                                        </small>\n' +
                            '\n' +
                            '\n' +
                            '                                    </a>\n' +
                            '                                    <a href="#" class="name">\n' +
                            '                                        <h3>'+msg['empleados'][i]['clave']+'&nbsp;<strong>'+msg['empleados'][i]['nombre']+'  </strong> '+msg['empleados'][i]['apepat'] +' ' + msg['empleados'][i]['apemat'] +'</h3>\n' +
                            '                                    </a>\n' +

                            '                                    </p>\n' +
                            '                                        <h6>'+msg['empleados'][i]['mail']+'&nbsp;</h6>\n' +
                            '                                    </p>\n' +
                            '                                    </p>\n' +
                            '                                        <h6><strong>Fecha de Asignación</strong> '+ msg['empleados'][i]['fhasignacion']+'&nbsp;</h6>\n' +
                            '                                    </p>\n' +
                            '                                    <!-- /.attachment -->\n' +
                            '                            </div>\n' +
                            '                            <!-- /.item -->\n' +
                            '                        </td>\n' +
                            '\n' +
                            '                    </tr>';
                    }
                    empleados += ' </tbody>\n' +
                        '                        </table>' ;
                    $('.empleados').html( empleados );

                    var txtWid = "";

                    for(var i = 0; i < msg['widgets'].length; i++) {

                        var mod = msg['widgets'][i]['Id'];
                        var nombreMod= msg['widgets'][i]['widget'];

                        txtWid += '<div class=" checkbox">\n' +
                            '                     <label class="checkbox-inline">\n' +
                            '                     <input onclick="setWidget('+mod+', '+id+')" type="checkbox" id="wid'+mod+'"  >\n' +
                            '                     '+nombreMod+'\n' +
                            '                     </label>\n' +
                            '                     </div>';

                    }

                    $('.showWidgets').html( txtWid );
                    var modWid = "";
                    for(var i = 0; i < msg['modWid'].length; i++) {

                        modWid = $('#wid'+msg['modWid'][i]['widgets_tiendas_Id'])
                        modWid.prop('checked', true);

                    }

                    console.log(msg['modWid']);


                });



            $('.btnGuardar').html( ' <a onclick="update('+id+')" class="btn btn-primary ">Actualizar</a>' +
                ' <a onclick="cancel()" class="btn btn-danger">Cancelar</a>' )

            getFoda(id);
        }

        function setWidget(id, user_id)
        {
            var mod = $("#wid"+id);
            url = '{{route('addWidTienda')}}';
            // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:user_id, widgets_id: id, type:mod.is( ":checked" )}
            })
                .done(function(msg){

                    console.log(msg['message']);

                });
        }


        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('razon').value  }
            })
                .done(function(msg){

                   // console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Plazas</b> <select class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona una plaza </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }


        function update(id)
        {


            if(document.getElementById('inputName').value != "")
            {
                if(document.getElementById('datepicker').value != "")
                {
                    url = '<?php echo e(route('updateTienda')); ?>';
                    // alert('Muestra despues')

                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {id: id,
                            numsuc: document.getElementById('inputClave').value,
                            nombre: document.getElementById('inputName').value,
                            fhapertura: document.getElementById('datepicker').value,
                            direccion:document.getElementById('inputDom').value,
                            telefono: document.getElementById('inputTel').value,
                            mail:document.getElementById('inputEmail').value,
                            foto: document.getElementById('foto_tienda').value,
                            plaza_Id: document.getElementById('plaza').value,
                            razon_Id: document.getElementById('razon').value,
                            zona: document.getElementById('zona').value,
                            password: document.getElementById('inputPass').value,
                            emp_ideal: document.getElementById('inputEmp').value,
                            m2: document.getElementById('inputM2').value,
                            cp: document.getElementById('inputCP').value,
                            lat: document.getElementById('lan').value,
                            lng: document.getElementById('lng').value,
                        }
                    })
                        .done(function(msg){



                            updateFoda(id);

                        });
                }else
                {
                    bootbox.alert('Debes elegir una fecha de apertura')
                }




            }else
            {
                bootbox.alert('Debes nombrar la tienda')
            }

        }


        function updateFoda(id)
        {
            var url = '{{route('updateFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id: id, fortaleza:  document.getElementById('fortaleza').value,
                        oportunidad: document.getElementById('oportunidad').value,
                        debilidad: document.getElementById('debilidad').value,
                        amenaza:  document.getElementById('amenaza').value}
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var url = "";
                    url = '<?php echo e(route('tiendas.tiendas')); ?>';//'visitas/final/'+post_id;//
                    window.location.href = url;

                });
        }

        function asignaEmp(id)
        {
            $("#listaTiendas").hide();
            $("#editarTienda").hide();
            $("#asignaEmp").show();

           var url = '{{route('cargaEmp')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  0 }
            })
                .done(function(msg){
                    console.log(msg['empleados']);
                    var text = "";
                    text += ' <label>Selecciona el empleado</label> ' +
                            ' <select class="form-control select2 select2-container--default" name="emps" id="emps" style="width: 100%;" tabindex="-1" aria-hidden="true">';
                    for(var i = 0; i < msg['empleados'].length; i++)
                    {

                         //text += '<option >--Seleccionar--</option>';
                        text += ' <option value="'+msg['empleados'][i]['Id']+'">'+msg['empleados'][i]['nombre']+' '+ msg['empleados'][i]['apepat']+' '+ msg['empleados'][i]['apemat']+'</option>\n';
                    }
                    text += '</select>';
                  //  alert(text);
                    $('.empleadosUsuario').html( text );

                    $('.btnAsigna').html( '<button type="button" onclick="asignar('+id+')" class="btn  btn-success pull-right">Asignar</button>' );

                });
        }

        function asignar(id)
        {
            var selEmp = document.getElementsByName('emps')[0].value;
            //alert(id)

            var url = '{{route('asignaEmp')}}'
           $.ajax({
                method: 'POST',
                url: url,
                data: {id:  selEmp, tiendas_Id: id }
            })
                .done(function(msg){


                   if(msg['message'] != 0)
                   {
                       bootbox.alert('Este empleado ya existe asignado en otra sucursal, dalo de baja para poder asignarlo');

                   }else
                   {
                       $("#listaTiendas").hide();
                       $("#editarTienda").show();
                       $("#asignaEmp").hide();

                       getSucursal(id);
                   }

                });
        }

        function cancelAsig()
        {
            $("#listaTiendas").hide();
            $("#editarTienda").show();
            $("#asignaEmp").hide();
        }

        function bajaSuc(id, tiendas_Id)
        {
            var url = '{{route('bajaSucEmp')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  id, tiendas_Id: tiendas_Id }
            })
                .done(function(msg){

                        getSucursal(tiendas_Id);

                });
        }


        function getFoda(id)
        {


            var url = '{{route('getFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  id }
            })
                .done(function(msg){
                    console.log(msg['foda']);
                    var text ="";

                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-warning">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Fortalezas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="fortaleza"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['fortaleza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-primary">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Oportunidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="oportunidad" style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['oportunidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';
                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-success">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Debilidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="debilidad"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['debilidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-danger">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Amenazas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                       '<textarea id="amenaza"  style="resize: none; width: 100%; height: 100px" >'+msg['foda'][0]['amenaza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';
                    $('.foda').html( text );

                });


        }
    </script>

@stop