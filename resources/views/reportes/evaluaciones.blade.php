@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Reporte de Evaluaciones </h1>
@stop

@section('content')


    <div id="filtro" class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Filtros de generación </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <strong><i class="fa fa-tachometer margin-r-5"></i> Reporte en el que podrás ver las evaluaciones del mes de un conjunto seleccionado de sucursales</strong>
            <hr>

            <strong><i class="fa fa-book margin-r-5"></i> Checklist</strong>

            <select onchange="setButton()" class="form-control select2" id="chk" name="checklist" style="width: 100%;">
                @foreach($checklist as $chk)
                    <option value="{{$chk->Id}}"> {{$chk->nombre}}</option>

                @endforeach
            </select>

            <hr>
            <div class="radioVal">

                <label >Selecciona razón:</label>

                <select onchange="getPlazas()" class="form-control select2" id="raz" name="razon" style="width: 100%;">
                    <option value="0" selected="selected"><--Todas las razones--></option>
                    @foreach($razones as $raz)
                        <option value="{{$raz->Id}}"> {{$raz->nombre}}</option>

                    @endforeach

                </select>
                <div class="plazas">
                    <input type="hidden" value="0" id="plaza" >
                </div>
                <div class="tiendas">
                    <input type="hidden" value="0" id="suc" >
                </div>
            </div>
            <hr>
            <strong><i class="fa fa-book margin-r-5"></i>Selecciona Año</strong>
            <select onchange="setButton()" class="form-control select2" id="year" name="years" style="width: 100%;">
                <option  value="{{date('Y')}}" selected="selected"><?php echo date('Y')?></option>
                <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

            </select>
            <hr>
            <strong><i class="fa fa-book margin-r-5"></i>Selecciona un Mes</strong>
            <select onchange="setButton()" class="form-control select2" id="mes" name="meses" style="width: 100%;">
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>

            </select>
            <hr>
            <div class="btnVer">


            </div>

        </div>
        <!-- /.box-body -->
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">

    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')

    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>

    <script>

        $(document).ready(function() {
            var url = route('exportEvaluaciones', {razon: document.getElementById('raz').value,
                plaza: document.getElementById('plaza').value,
                anio: document.getElementById('year').value,
                mes:document.getElementById('mes').value,
                suc:document.getElementById('suc').value,
                chk:document.getElementById('chk').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')

        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });

        });


        function generaReporte()
        {

        }

        function getPlazas()
        {

            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('raz').value  }
            })
                .done(function(msg){


                    var text = "";

                    text += '<b>Selecciona una plaza:</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las plazas--> </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )
                    setButton();

                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Selecciona la sucursal:</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected><--Todas las sucursales--> </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )

                    setButton();
                });

        }

        function  setButton()
        {
            var url = route('exportEvaluaciones', {razon: document.getElementById('raz').value,
                plaza: document.getElementById('plaza').value,
                anio: document.getElementById('year').value,
                mes:document.getElementById('mes').value,
                suc:document.getElementById('suc').value,
                chk:document.getElementById('chk').value});

            $('.btnVer').html('<a href="'+url+'" class="btn btn-block btn-primary ">Genera Reporte</a>')
        }

        function guardaConfig()
        {
            var puesto = document.getElementById('pto').value;
            var empleado = document.getElementById('emp').value;
            var razon = document.getElementById('raz').value;
            var plaza = document.getElementById('plaza').value;
            var sucursal = document.getElementById('suc').value;
            var tipo = 0;

            if(document.getElementById('pto').value != 0)
            {
                if(document.getElementById('boxCheck').checked) {
                    tipo = 1;
                }
                if(document.getElementById('boxSeg').checked) {
                    tipo = 2;
                }

                if(document.getElementById('boxSeg').checked || document.getElementById('boxCheck').checked ) {
                    tipo = 0;
                }

                var url = '{{route('agregaEnvio')}}'
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {puesto:puesto, empleado: empleado, razon:razon, plaza:plaza, sucursal:sucursal, tipo: tipo  }
                })
                    .done(function(msg){


                        cargaConfiguracionesCorreo()


                    });
            }else
            {
                bootbox.alert("Debes seleccionar por lo menos un puesto")
            }


        }

        function cargaConfiguracionesCorreo()
        {
            var url = '{{route('cargaConfiguracionesCorreo')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: { }
            })
                .done(function(msg){

                    console.log(msg['correos'])

                    /* */
                    var text ="";
                    var msg_txt = "  ";
                    var msg_raz = "  ";
                    var msg_pla = "  ";
                    var msg_suc = "  ";
                    for(var i = 0; i < msg['correos'].length; i++)
                    {
                        msg_txt = "  ";
                        msg_raz = "  ";
                        msg_pla = "  ";
                        msg_suc = "  ";

                        if(msg['correos'][i]['tipo'] == 0)
                        {
                            msg_txt = " Envio de visitas de checklist y seguimiento"
                        }else   if(msg['correos'][i]['tipo'] == 1)
                        {
                            msg_txt = "Envio de visitas de checklist"
                        }else   if(msg['correos'][i]['tipo'] == 2)
                        {
                            msg_txt = "Envio de visitas de seguimiento"
                        }

                        if(msg['correos'][i]['empleados_Id'] == 0)
                        {
                            msg_txt += " a todos los empleados del puesto " + msg['correos'][i]['puesto'];

                        }else
                        {
                            msg_txt += " al empleado " + msg['correos'][i]['nombre'] + ' ' + msg['correos'][i]['apepat']+' '+ msg['correos'][i]['apemat'] ;
                        }

                        if(msg['correos'][i]['razon_Id'] == 0)
                        {
                            msg_raz += " A todas las razones  ";
                        }else
                        {

                            msg_raz += " A la Razón"+' ' + msg['correos'][i]['razon'];
                        }

                        if(msg['correos'][i]['plaza_Id'] == 0)
                        {
                            msg_pla += " De todas las plazas  ";

                        }else
                        {
                            msg_pla += " De la Plaza"+' ' + msg['correos'][i]['plaza'];
                        }

                        if(msg['correos'][i]['tiendas_Id'] == 0)
                        {
                            msg_suc += " En todas las sucursales  ";

                        }else
                        {
                            msg_suc += " De la Sucursal"+' ' + msg['correos'][i]['tienda'];
                        }
                        text += '<li class="item">\n' +
                            '\n' +
                            '                    <div >\n' +
                            '                        <a href="javascript:void(0)" class="product-title">'+msg['correos'][i]['puesto'] +'\n' +
                            '                            <span class="label label-danger pull-right">quitar</span></a>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_txt+'     \n' +
                            '                        </span>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_raz+'     \n' +
                            '                        </span>\n' +
                            '                    </div>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_pla+'     \n' +
                            '                        </span>\n' +
                            '                        <span class="product-description">\n' +
                            '                    '+msg_suc+'     \n' +
                            '                        </span>\n' +
                            '                    </div>\n' +
                            '                </li>';
                    }

                    $('.correos').html( text );
                });
        }


    </script>

@stop