@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Ventas</h1>
@stop

@section('content')


    <!-- /.box -->
    <!-- /.box -->
    <div class="row" >
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <h3 class="profile-username text-center">Ventas por Sucursal</h3>

                    <!--<p class="text-muted text-center">Software Engineer</p>-->

                    <ul class="list-group list-group-unbordered">
                        <strong><i class="fa fa-book margin-r-5"></i> Selecciona Sucursal</strong>
                        <select class="form-control select2" id="suc" name="tiendas" style="width: 100%;">

                            @foreach($tiendas as $suc)
                                <option value="{{$suc->Id}}"> {{$suc->nombre}}</option>

                            @endforeach

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Selecciona Año</strong>
                        <select class="form-control select2" id="year" name="years" style="width: 100%;">
                            <option  value="{{date('Y')}}" selected="selected"><?php echo date('Y')?></option>
                            <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Mes</strong>
                        <select class="form-control select2" id="mes" name="meses" style="width: 100%;">

                            <option value="0"><--Todos--></option>
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>

                        </select>
                        <hr>
                        <a onclick="getVentas()" class="btn btn-block btn-primary ">Ver</a>
                        <hr>
                        <button type="button" class="btn btn-block btn-success" data-toggle="modal" data-target="#modal-success">
                            Subir Ventas Diarias
                        </button>

                        <hr>
                        <button type="button" class="btn btn-block btn-warning" data-toggle="modal" data-target="#modal-warning">
                            Subir Presupuestos
                        </button>

                    </ul>


                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <!-- /.col -->
        <div class="col-md-9 ventaMes">

        </div>
        <!-MODALS DE SUBIDA-->
        <div class="modal modal-success fade" id="modal-success" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Subir ventas diarias</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="input" class="col-sm-2 control-label">Elige el día </label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="datepicker">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                        <form action="{{route('subeArchivoVentas')}}" method="post" style="display: none" id="avatarForm">
                            <input type="file" id="avatarInput" name="photo">
                        </form>
                        <div class="rutaFoto">
                            <a onclick="setFoto()" id="avatarImage" class="btn btn-outline ">Subir</a>
                        </div>

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-MODAL DE PRESUPUESTO-->
        <div class="modal modal-warning fade" id="modal-warning">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Subir Presupuestos</h4>
                    </div>
                    <div class="modal-body">
                        <strong><i class="fa fa-book margin-r-5"></i> Selecciona Año</strong>
                        <select class="form-control select2" id="yearp" name="yearsp" style="width: 100%;">
                            <option value="0" selected="selected">Selecciona</option>
                            <option  value="{{date('Y')}}" ><?php echo date('Y')?></option>
                            <option value="<?php echo date('Y') - 1?>"><?php echo date('Y') - 1?></option>

                        </select>
                        <hr>
                        <strong><i class="fa fa-book margin-r-5"></i> Seleccionar Mes</strong>
                        <select class="form-control select2" id="mesp" name="mesesp" style="width: 100%;">

                            <option value="0">Selecciona Mes</option>
                            <option value="1">Enero</option>
                            <option value="2">Febrero</option>
                            <option value="3">Marzo</option>
                            <option value="4">Abril</option>
                            <option value="5">Mayo</option>
                            <option value="6">Junio</option>
                            <option value="7">Julio</option>
                            <option value="8">Agosto</option>
                            <option value="9">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>

                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                        <form action="{{route('subeArchivoPres')}}" method="post" style="display: none" id="avatarFormPres">
                            <input type="file" id="avatarInputPres" name="photo">
                        </form>
                        <div class="rutaFotoPres">


                            <a onclick="setFotoPres()" id="avatarImagePres" class="btn btn-outline ">Subir</a>


                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 300px;
            width: 100%;
        }

    </style>

@stop

@section('js')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDigdQtAwnyVRn7wCrver_6pilN-7vfnNY&callback=myMap"></script>
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ asset('js/script.js')}}"></script>

    <script>
        function setFoto()
        {

            if(document.getElementById('datepicker').value != "")
            {
                bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Subiendo archivo...</div>' })
                $avatarImage = $('#avatarImage');
                $avatarInput = $('#avatarInput');
                $avatarForm = $('#avatarForm');
                $rutaFoto = $('.rutaFoto');
                $avatarInput.click();
                $avatarInput.on('change', function () {
                    var formData = new FormData();
                    formData.append('excel', $avatarInput[0].files[0]);
                    formData.append('dia',document.getElementById('datepicker').value );
                    // $('#modal-info').modal('toggle');

                    $.ajax({
                        url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                        method: $avatarForm.attr('method'),
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (msg) {
                        document.getElementById('datepicker').value = "";
                        $('#modal-success').modal('hide');
                        console.log(msg['path']);


                        bootbox.alert("El archivo fue subido con éxito");

                        var url = "";
                        url = '<?php echo e(route('ventas.ventas')); ?>';//'visitas/final/'+post_id;//
                        window.location.href = url;
                        updateMeses(msg['mes'], msg['anio']);

                    });
                });
            }else
            {
                bootbox.alert("Debes elegir el día de la venta para continuar");
            }

        }

        function updateMeses(mes, anio)
        {
            //funcion que actualiza los meses
            url = '{{route('updateMeses')}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {mes: mes, anio: anio}
            })
                .done(function(msg){

                    console.log(msg['path']);


                });
        }


        function setFotoPres()
        {

            if(document.getElementById('yearp').value != 0)
            {
                if(document.getElementById('mesp').value != 0)
                {
                    //  bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Subiendo archivo...</div>' })
                    $avatarImage = $('#avatarImagePres');
                    $avatarInput = $('#avatarInputPres');
                    $avatarForm = $('#avatarFormPres');
                    $rutaFoto = $('.rutaFotoPres');
                    $avatarInput.click();
                    $avatarInput.on('change', function () {
                        var formData = new FormData();
                        formData.append('excel', $avatarInput[0].files[0]);
                        formData.append('anio', document.getElementById('yearp').value);
                        formData.append('mes', document.getElementById('mesp').value);

                        $('#modal-info').modal('toggle');

                        $.ajax({
                            url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                            method: $avatarForm.attr('method'),
                            data: formData,
                            processData: false,
                            contentType: false
                        }).done(function (msg) {
                            bootbox.hideAll()
                            $('#modal-warning').modal('hide');
                            console.log(msg['path']);

                            bootbox.alert("El archivo fue subido con éxito");
                            var url = "";
                            url = '<?php echo e(route('ventas.ventas')); ?>';//'visitas/final/'+post_id;//
                            window.location.href = url;
                        });
                    });
                }else
                {
                    bootbox.alert('Debes seleccionar el mes para subir presupuestos')
                }
            }else
            {
                bootbox.alert("Debes elegir un año");
            }



        }

        $(document).ready(function() {

          //  getVentas()
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })

            //Initialize Select2 Elements
            $('.select2').select2()



            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )



            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })

        function subirVentas()
        {
            url = '{{route('subeArchivoVentas')}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {}
            })
                .done(function(msg){

                    console.log(msg['path']);


                });
        }



        function getVentas()
        {
          //  alert(document.getElementById('suc').value+"/"+document.getElementById('year').value+"/"+document.getElementById('mes').value)

            var url = '{{route('getVentasMes')}}'

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:document.getElementById('suc').value,  anio: document.getElementById('year').value, mes:document.getElementById('mes').value}
            })
                .done(function(msg){
                    console.log(msg['ventas']);
                    var text = "";

                    var URLdomain = window.location.host;
                   // var urlx = '/exportVentaMes/'+document.getElementById('year').value;
                    var urlx = route('exportVentaMes', {anio: document.getElementById('year').value,
                                                        mes:document.getElementById('mes').value,
                                                        suc:document.getElementById('suc').value});


                    for(var i = 0; i < msg['ventas'].length; i++)
                    {
                        var ventas = msg['ventas'][i]['ventas'];
                        var presupuesto = msg['ventas'][i]['presupuesto'];

                        text +=    '  <div class="nav-tabs-custom">\n' +
                            '                <div class="box box-widget widget-user">\n' +
                            '                    <!-- Add the bg color to the header using any of the bg-* classes -->\n' +
                            '                    <div class="widget-user-header bg-aqua-active">\n' +
                            '                        <h3 class="widget-user-username">'+msg['ventas'][i]['Tienda']+'</h3>\n' +
                            '                        <h5 class="widget-user-desc">'+ mes(msg['ventas'][i]['mes']) +' '+msg['ventas'][i]['anio']+'</h5>\n' +
                            '                        <div class="btnGuardar pull-right">\n' +
                            '\n' +
                            '                            <button onclick="getVtaDiaria('+msg['ventas'][i]['Id']+','+msg['ventas'][i]['tiendas_Id']+','+msg['ventas'][i]['mes']+','+msg['ventas'][i]['anio']+')" class="btn btn-primary btnGuardar">Ventas Diarias</button>\n' +
                           // '  <a href="'+urlx+'" class="btn btn-social-icon btn-success"><i class="fa fa-file-excel-o"></i></a>'+
                            '\n' +
                            '                        </div>\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                    <div class="box-footer">\n' +
                            '                        <div class="row">\n' +
                            '                            <div class="col-sm-4 border-right">\n' +
                            '                                <div class="description-block">\n' +
                            '                                    <h5 class="description-header">'+msg['ventas'][i]['dias']+'</h5>\n' +
                            '                                    <span class="description-text">Días</span>\n' +
                            '                                </div>\n' +
                            '                                <!-- /.description-block -->\n' +
                            '                            </div>\n' +
                            '                            <!-- /.col -->\n' +
                            '                            <div class="col-sm-4 border-right">\n' +
                            '                                <div class="description-block">\n' +
                            '                                    <h5 class="description-header">'+msg['ventas'][i]['Nombre']+'</h5>\n' +
                            '                                    <span class="description-text">Clasificación</span>\n' +
                            '                                </div>\n' +
                            '                                <!-- /.description-block -->\n' +
                            '                            </div>\n' +
                            '                            <!-- /.col -->\n' +
                            '                            <div class="col-sm-4">\n' +
                            '                                <div class="description-block">\n' +
                            '                                    <h5 class="description-header">Mes Abierto</h5>\n' +
                            '                                    <span class="description-text"><a>cerrar</a></span>\n' +
                            '                                </div>\n' +
                            '                                <!-- /.description-block -->\n' +
                            '                            </div>\n' +
                            '                            <!-- /.col -->\n' +
                            '                        </div>\n' +
                            '                        <!-- /.row -->\n' +
                            '                        <div class="row" >\n' +
                            '                            <div class="col-sm-4" >\n' +
                            '                                <div class="info-box bg-aqua">\n' +
                            '                                    <span class="info-box-icon"><i class="fa fa-money"></i></span>\n' +
                            '\n' +
                            '                                    <div class="info-box-content">\n' +
                            '                                        <span class="info-box-text">Ventas</span>\n' +
                            '                                        <span class="info-box-number">$ '+CurrencyFormatted(ventas) +'</span>\n' +
                            '\n' +
                            '                                        <div class="progress">\n' +
                            '                                            <div class="progress-bar" style="width: 70%"></div>\n' +
                            '                                        </div>\n' +
                            '                                        <span class="progress-description">\n' +
                            '                                        \n' +
                            '                                    </span>\n' +
                            '                                    </div>\n' +
                            '                                    <!-- /.info-box-content -->\n' +
                            '                                </div>\n' +
                            '\n' +
                            '\n' +
                            '                            </div>\n' +
                            '                            <div class="col-sm-4" >\n' +
                            '                                <div class="info-box bg-green">\n' +
                            '                                    <span class="info-box-icon"><i class="fa  fa-calculator"></i></span>\n' +
                            '\n' +
                            '                                    <div class="info-box-content">\n' +
                            '                                        <span class="info-box-text">Presupuesto</span>\n' +
                            '                                        <span class="info-box-number">$'+CurrencyFormatted(presupuesto)+'</span>\n' +
                            '\n' +
                            '                                        <div class="progress">\n' +
                            '                                            <div class="progress-bar" style="width: 70%"></div>\n' +
                            '                                        </div>\n' +
                            '                                        <span class="progress-description">\n' +
                            '                                        \n' +
                            '                                    </span>\n' +
                            '                                    </div>\n' +
                            '                                    <!-- /.info-box-content -->\n' +
                            '                                </div>\n' +
                            '\n' +
                            '\n' +
                            '                            </div>\n' +
                            '                            <div class="col-sm-4" >\n' +
                            '                                <div class="info-box bg-yellow">\n' +
                            '                                    <span class="info-box-icon"><i class="fa  fa-tags"></i></span>\n' +
                            '\n' +
                            '                                    <div class="info-box-content">\n' +
                            '                                        <span class="info-box-text">Tickets</span>\n' +
                            '                                        <span class="info-box-number">'+msg['ventas'][i]['tickets']+'</span>\n' +
                            '\n' +
                            '                                        <div class="progress">\n' +
                            '                                            <div class="progress-bar" style="width: 70%"></div>\n' +
                            '                                        </div>\n' +
                            '                                        <span class="progress-description">\n' +
                            '                                       \n' +
                            '                                    </span>\n' +
                            '                                    </div>\n' +
                            '                                    <!-- /.info-box-content -->\n' +
                            '                                </div>\n' +
                            '\n' +
                            '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '\n' +
                            '\n' +
                            '\n' +
                            '                    </div>\n' +
                            '\n' +
                            '                </div>\n' +
                            '                <!--lista de indicadores-->\n' +
                                '<div class="ventasDiarias'+msg['ventas'][i]['Id']+'">' +
                            '' +
                            '</div>'+
                            '\n' +
                            '        <!-- /.col -->\n' +
                            '            </div>';
                    }

                    //ocultamos
                    var divId ="";




                    $('.ventaMes').html( text );



                });
        }



        function getVtaDiaria(id, tiendas_Id, mes, anio)
        {

            var $divVta = "";
            $divVta = $('.ventasDiarias'+id);

            var url = '{{route('getVtaDiaria')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:tiendas_Id, mes: mes, anio: anio}
            })
                .done(function(msg){
                    console.log(msg['ventas']);
                    var text = ' <table class="table no-margin">\n' +
                        '                  <thead>\n' +
                        '                  <tr>\n' +
                        '                    <th>Fecha</th>\n' +
                        '                    <th>Venta</th>\n' +
                        '                    <th>Tickets</th>\n' +
                        '                    <th>No. Art.</th>\n' +
                        '                    <th>Tck. Prom</th>\n' +
                        '                  </tr>\n' +
                        '                  </thead>\n' +
                        '                  <tbody>\n';
                    var sumVta = 0;
                    var sumTck = 0;
                    var sumNoTck = 0;
                    for(var i = 0; i < msg['ventas'].length; i++)
                    {


                        sumVta = parseFloat(sumVta) + parseFloat(msg['ventas'][i]['monto']);
                        sumTck = parseFloat(sumTck) + parseFloat(msg['ventas'][i]['tickets']);
                        sumNoTck = parseInt(sumNoTck) + parseInt(msg['ventas'][i]['artxticket']);
                        var ventas = msg['ventas'][i]['monto'];
                        var tckprom = msg['ventas'][i]['tckprom'];

                        text  +=     '                  <tr>\n' +
                                    '                    <td ><a >'+msg['ventas'][i]['fhventa']+'</a></td>\n' +
                                    '                    <td width="30px" style="text-align: right" >$'+CurrencyFormatted(ventas.toString())+'</td>\n' +
                                    '                    <td>'+msg['ventas'][i]['tickets']+'</td>\n' +
                                    '                    <td> '+msg['ventas'][i]['artxticket']+'</td>\n' +
                                    '                    <td width="30px" style="text-align: right" >$'+CurrencyFormatted(tckprom.toString())+' </td>\n' +
                                    '                  </tr>\n' ;
                    }

                    var tckPromS = parseFloat(sumVta) / parseFloat(sumTck);
                    sumVta = round(sumVta,2);
                    tckPromS = round(tckPromS,2);

                    text  +=     '                  <tr>\n' +
                        '                    <td ><strong><a >TOTALES</a></strong></td>\n' +
                        '                    <td width="30px" style="text-align: right" ><strong>$'+sumVta.toString()+'</strong></td>\n' +
                        '                    <td><strong>'+sumTck+'</strong></td>\n' +
                        '                    <td><strong> '+sumNoTck+'</strong></td>\n' +
                        '                    <td width="30px" style="text-align: right" ><strong>$'+tckPromS.toString()+'</strong></td>\n' +
                        '                  </tr>\n' ;

                    text  +=    '                  </tbody>\n' +
                                '                </table> ';

                    $divVta.html( text );

                });

        }


        function CurrencyFormatted(amount)
        {
            var delimiter = ","; // replace comma if desired
            var a = amount.split('.',2)
            var d = a[1];
            var i = parseInt(a[0]);
            if(isNaN(i)) { return ''; }
            var minus = '';
            if(i < 0) { minus = '-'; }
            i = Math.abs(i);
            var n = new String(i);
            var a = [];
            while(n.length > 3)
            {
                var nn = n.substr(n.length-3);
                a.unshift(nn);
                n = n.substr(0,n.length-3);
            }
            if(n.length > 0) { a.unshift(n); }
            n = a.join(delimiter);
            if(d.length < 1) { amount = n; }
            else { amount = n + '.' + d; }
            amount = minus + amount;

            return amount;
        }

        function round(value, decimals) {
            return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        }


        function mes(mes)
        {
            var m = ['',
                'Enero',
                'Febrero',
                'Marzo',
                'Abril',
                'Mayo',
                'Junio',
                'Julio',
                'Agosto',
                'Septiembre',
                'Octubre',
                'Noviembre', 'Diciembre'];
            return m[mes];

        }






    </script>

@stop