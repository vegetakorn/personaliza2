@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Zonas</h1>
@stop

@section('content')

    <div class="box" id="listaZonas">
        <div class="box-header">
            <button type="button" onclick="add()" class="btn  btn-success pull-right">Agregar Zona</button>
        </div>
        <!-- /.box-header


         -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Zona</th>


                </tr>
                </thead>


                <tbody>
                @foreach($zonas as $zona)

                    <tr>
                        <td >
                            <!-- chat item -->
                            <div class="item">


                                <img src="{{ asset('uploads/Zonas/'.$zona->foto ) }}" width="80px" height="80px" class="img-circle" >

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right">


                                            <a   ><img data-toggle="tooltip" onclick="editar({{$zona->Id}})" title="Editar" width="40" height="40" src="{{ asset('images/buttons/pencil.png')}}"  alt="Icono"></a >
                                            <a    ><img onclick="baja({{$zona->Id}})" data-toggle="tooltip" title="Eliminar Plaza" width="40" height="40" src="{{ asset('images/buttons/stop.png')}}"  alt="Icono"></a >



                                        </small>


                                    </a>
                                    <a href="#" class="name">
                                        <h3>&nbsp;<strong>{{$zona->zona}}  </strong> </h3>
                                    </a>

                                    <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <div class="row" id="agregarZona">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <form action="{{route('regFotoZona')}}" method="post" style="display: none" id="avatarForm">
                        <input type="file" id="avatarInput" name="photo">
                    </form>
                    <div class="rutaFoto">
                        <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="{{ asset('uploads/Zonas/no_imagen.jpg')}}" alt="User profile picture">

                    </div>

                    <h3 class="profile-username text-center">Foto de Zona</h3>

                    <!--<p class="text-muted text-center">Software Engineer</p>-->




                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <!-- <div class="box box-primary">
                <div class="box-header ui-sortable-handle" >
                    <i class="ion ion-clipboard"></i>

                    <h3 class="box-title">Actividades</h3>


                </div>

                <div class="box-body">

                    <ul class="todo-list ui-sortable">
                      <li>

                         </span>


                            <span class="text">Design a nice theme</span>


                      </li>
                        <li>

                            </span>


                            <span class="text">Revisar todos los materiales disponibles que deberan ser tratados</span>


                        </li>
                        <li>

                            </span>


                            <span class="text">Revisar todos los materiales disponibles que deberan ser tratados</span>


                        </li>
                    </ul>
                </div>

                <div class="box-footer clearfix no-border">
                    <div class="input-group">
                        <input class="form-control" placeholder="Agrega Actividad...">

                        <div class="input-group-btn">
                            <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active"><a href="#settings" data-toggle="tab">Información de la Zona</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="settings">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputClave" class="col-sm-2 control-label">Clave</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputClave" placeholder="Clave">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Nombre</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputName" placeholder="Nombre">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">


                                    <div class="btnGuardar">

                                        <a onclick="save()" class="btn btn-primary btnGuardar">Guardar</a>
                                        <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>

    <div class="foto_zona" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_zona" value="no_imagen.jpg" >
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            $('#example1').DataTable( {
                "pagingType": "full_numbers",
                "scrollY":        '60vh',
                "scrollCollapse": true,
                "language": {
                    "lengthMenu": "Mostrando _MENU_ registros por página ",
                    "zeroRecords": "Sin registros encontrados",
                    "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                    "infoEmpty": "Sin regustros encontrados",
                    "infoFiltered": "(filtrados de _MAX_ registros totales)",
                    "search": "Buscar:",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    }
                }
            } );
        } );
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Datemask2 mm/dd/yyyy
            $('#datemask2').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
            //Money Euro
            $('[data-mask]').inputmask()

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
            //Date range as a button
            $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                        'Today'       : [moment(), moment()],
                        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
            )

            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            //Timepicker
            $('.timepicker').timepicker({
                showInputs: false
            })
        })


        $(document).ready(function() {

            $("#agregarZona").hide();
            //$("#agregaInfo").hide();



        } );


        function update(id)
        {
            if(document.getElementById('inputName').value != "")
            {
                url = '{{route('updateZona')}}';
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {id: id,
                        clave: document.getElementById('inputClave').value,
                        nombre: document.getElementById('inputName').value,
                        foto: document.getElementById('foto_zona').value
                    }
                })
                    .done(function(msg){

                        console.log(msg['message']);
                        var url = "";
                        url = '{{route('zonas.zonas')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });

            }else
            {
                bootbox.alert('Debes indicar el nombre de la zona')
            }
        }

        function add()
        {
            //alert('OK')
            $("#listaZonas").hide();
            $("#agregarZona").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoEmpleado')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {   }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Puesto</b> <select class="form-control select2" id="puesto" name="puesto" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona un puesto </option>' ;


                    for(var i = 0; i < msg['puestos'].length; i++)
                    {
                        text += ' <option value="'+msg['puestos'][i]['Id']+'">'+msg['puestos'][i]['puesto']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.puestos').html( text )

                    var razon = "";
                    razon += '<b>Razón</b> <select onchange="getPlazas()" class="form-control select2" id="razon" name="razon" style="width: 100%;">' +
                        '<option value="0" selected>Selecciona una razón </option>' ;
                    for(var i = 0; i < msg['razones'].length; i++)
                    {
                        razon += ' <option value="'+msg['razones'][i]['Id']+'" >'+msg['razones'][i]['nombre']+'</option>\n';
                    }
                    razon +=    '                           </select>';
                    $('.razones').html( razon )
                });



        }

        function editar(id)
        {
            $("#listaZonas").hide();
            $("#agregarZona").show();

            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaInfoZonaEditar')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id: id   }
            })
                .done(function(msg){

                    console.log(msg['zona']);
                    var text = "";

                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/uploads/Zonas/'+msg['zona']['foto'];
                    $avatarImage = $('#avatarImage');
                    $avatarImage.attr('src', pathImg)
                    $('.foto_zona').html('<input type="hidden" id="foto_zona" value="' + msg['zona']['foto'] + '" >')
                    document.getElementById('inputClave').value = msg['zona']['clave'];
                    document.getElementById('inputName').value = msg['zona']['zona'];

                });



            $('.btnGuardar').html( ' <a onclick="update('+id+')" class="btn btn-primary ">Actualizar</a>' +
                ' <a onclick="cancel()" class="btn btn-danger">Cancelar</a>' )
        }


        function baja(id)
        {
            //preguntamos antes si queremos cargar el cuestionario de la visita
            var box = bootbox.confirm({
                title: "Baja de zona",
                message: "¿Deseas dar de baja a la zona?.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {
                        // alert('realizar cuestionario')
                        var url = '{{route('bajaZona')}}'
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {id:  id  }
                        })
                            .done(function(msg){

                                var url = "";
                                url = '{{route('zonas.zonas')}}';//'visitas/final/'+post_id;//
                                window.location.href = url;

                            });


                    }else
                    {

                    }

                }
            });
        }

        function getPlazas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaPlazas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('razon').value  }
            })
                .done(function(msg){

                    console.log(msg['puestos']);
                    var text = "";

                    text += '<b>Plazas</b> <select onchange="getTiendas()" class="form-control select2" id="plaza" name="plaza" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona una plaza </option>' ;


                    for(var i = 0; i < msg['plazas'].length; i++)
                    {
                        text += ' <option value="'+msg['plazas'][i]['Id']+'">'+msg['plazas'][i]['plaza']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.plazas').html( text )


                });

        }

        function getTiendas()
        {
            // alert(document.getElementById('razon').value) plazas capturadas
            //vamos a obtener el listado de cosas para asignar al empleado
            var url = '{{route('cargaTiendas')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  document.getElementById('plaza').value  }
            })
                .done(function(msg){

                    console.log(msg['tiendas']);
                    var text = "";

                    text += '<b>Sucursales</b> <select  class="form-control select2" id="suc" name="suc" style="width: 100%;">\n' +
                        '<option value="0" selected>Selecciona una sucursal </option>' ;


                    for(var i = 0; i < msg['tiendas'].length; i++)
                    {
                        text += ' <option value="'+msg['tiendas'][i]['Id']+'">'+msg['tiendas'][i]['nombre']+'</option>\n';
                    }

                    text +=    '                           </select>';
                    $('.tiendas').html( text )


                });

        }

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_zona').html('<input type="hidden" id="foto_zona" value="' + msg['message'] + '" >')


                });
            });
        }


        function cancel()
        {
            //alert('OK')
            url = '{{route('zonas.zonas')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }

        function save() {
            url = '{{route('addZona')}}';
            // alert('Muestra despues')

            if (document.getElementById('inputName').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: { clave: document.getElementById('inputClave').value,
                        nombre: document.getElementById('inputName').value,
                        foto: document.getElementById('foto_zona').value
                    }
                })
                    .done(function(msg){

                        console.log(msg['message']);
                        var url = "";
                        url = '{{route('zonas.zonas')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    });
            }else
            {
                bootbox.alert("Debes indicar el nombre de la zona");
            }


        }


    </script>


@stop