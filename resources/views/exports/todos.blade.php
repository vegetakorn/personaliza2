<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de To-Dos</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="3">
            Reporte de To-Do's
        </th>
    </tr>

    <tr>
        <th colspan="3">
            Periodo:  {{$info['FechaIni']}} al {{$info['FechaFin']}}
        </th>
    </tr>

    <tr>
        <th style="text-align: center; background-color: #4ec0d5"  width="10%">Sucursal</th>
        <th style="text-align: center; background-color: #4ec0d5" width="15%">To-Do</th>
        <th style="text-align: center; background-color: #4ec0d5" width="15%">Fecha</th>
        <th style="text-align: center; background-color: #4ec0d5" width="10%">Descripción</th>
        <th style="text-align: center; background-color: #4ec0d5" width="10%">Familia</th>
        <th style="text-align: center; background-color: #4ec0d5" width="15%">Categoría</th>
        <th style="text-align: center; background-color: #4ec0d5" width="10%">Status</th>

    </tr>

    </thead>
    <tbody>
    @foreach($info['todos'] as $todo )
        <tr>
            <td style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d" >{{$todo->tienda}}</td>
            <td style="text-align: center;color: #0b3e6f;border-style: solid; border-bottom: #9d9d9d" >{{$todo->nombre}}</td>
            <td style="text-align: center;color: #707572;border-style: solid;border-bottom: #9d9d9d" >{{date('d/m/Y', strtotime($todo->fecha_inicio))}}</td>
            <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >{{$todo->descripcion}}</td>
            <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >{{$todo->familia}}</td>
            <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >{{$todo->categoria}}</td>
            @if($todo->Status == 151)
                <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >Nuevo</td>
            @elseif($todo->Status == 152)
                <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >En Curso</td>
            @elseif($todo->Status == 153)
                <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >Pendiente</td>
            @elseif($todo->Status == 154)
                <td style="text-align: center;color: #0b3e6f;border-style: solid;border-bottom: #9d9d9d" >Concluido</td>
            @endif


        </tr>
    @endforeach

    </tbody>

</table>


</body>
</html>