<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de Ventas Diarias</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="3">
            Reporte de Ventas Diarias
        </th>
    </tr>

    <tr>
        <th colspan="3">
            Periodo:  {{date('d/m/Y', strtotime($info['fecha_ini']))}} al {{date('d/m/Y', strtotime($info['fecha_fin']))}}

        </th>
    </tr>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Clave</th>

        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Sucursal</th>
        @foreach($info['fechas'] as $fecha )
            <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large" >{{$fecha['fhventa']}}</th>
        @endforeach

    </tr>
    </thead>
    <tbody>
    @foreach($info['tiendas'] as $tiendas )
        <tr>
            <td style="text-align: center;color: #0b3e6f" >{{$tiendas['numsuc']}}</td>
            <td style="text-align: center;color: #0b3e6f" >{{$tiendas['nombre']}}</td>
            @foreach($tiendas['ventas'] as $venta )
                <td style="text-align: center;color: #0b3e6f" >${{number_format($venta['monto'], 2, '.', ',')}}</td>
            @endforeach

        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>