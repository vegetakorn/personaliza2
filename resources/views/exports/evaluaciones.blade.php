<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reporte de Evaluaciones</title>
    <meta charset="UTF-8">

</head>
<body>
<table border="1">
    <thead>
    <tr>
        <th colspan="3">
            Reporte de Evaluaciones
        </th>
    </tr>
    <tr>
        <th colspan="3">
                Checklist: {{$info['checklist']}}
        </th>
    </tr>
    <tr>
        <th colspan="3">
            Periodo:  {{$info['mes']}} de {{$info['anio']}}
        </th>
    </tr>
    <tr>
        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Clave</th>

        <th style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Sucursal</th>

        @for($i= 1; $i<= $info['dias']; $i++)
            <th style="background-color: #a9a9a9;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$i}}</th>
        @endfor
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Promedio</th>
        <th style="background-color: #d52829;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">0% - 59%</th>
        <th style="background-color: #d4d51b;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">60% - 90%</th>
        <th style="background-color: #60f774;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">91% - 100%</th>
        <th style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($info['tiendas'] as $suc)
        <tr>
            <td style="background-color: #ffffff;border-style: solid; border-bottom-color: #6f150f "  width="25%">{{$suc['numsuc']}}</td>

            <td style="background-color: #ffffff " width="25%" > {{$suc['nombre']}}</td>
            @foreach($suc['visitas'] as $vis)
                <td style="background-color: {{$vis['Color']}}" width="25%" >{{$vis['Calif']}}</td>
            @endforeach
            <td style="background-color: {{$suc['color']}} " width="25%" > {{$suc['promedio']}}</td>
            <td style="background-color: #ffffff " width="25%" > {{$suc['rojo']}}</td>
            <td style="background-color: #ffffff " width="25%" > {{$suc['amarillo']}}</td>
            <td style="background-color: #ffffff " width="25%" > {{$suc['verde']}}</td>
            <td style="background-color: #ffffff " width="25%" > {{$suc['total']}}</td>
        </tr>
    @endforeach
    <tr>
        <td style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large"></td>

        <td style="background-color: #0b93d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large"></td>

        @for($i= 1; $i<= $info['dias']; $i++)
            <td style="background-color: #a9a9a9;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$i}}</td>
        @endfor
        <td style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">Totales</td>
        <td style="background-color: #d52829;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$info['rojoGen']}}</td>
        <td style="background-color: #d4d51b;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$info['amarilloGen']}}</td>
        <th style="background-color: #60f774;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$info['verdeGen']}}</th>
        <td style="background-color: #7bb8d5;
                    border: #0f0f0f;
                    color: #ffffff;border-color: #1a2226;
                    text-align: center;
                    font-size: large">{{$info['totGen']}}</td>
    </tr>

    </tbody>
</table>


</body>
</html>