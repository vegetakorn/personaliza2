@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>
        Manual

    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Manual</a></li>
        <li class="active">Manuales de Uso</li>
    </ol>


@stop

@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- The time line -->
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-red">
                    11 Oct. 2018
                  </span>
                </li>
                <!-- /.timeline-label -->

                <!-- timeline item -->
                <li>
                    <i class="fa fa-video-camera bg-maroon"></i>

                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> </span>

                        <h3 class="timeline-header"><a href="#">Seguimiento a Sucursal</a> Explicación completa de como realizar una visita de seguimiento</h3>

                        <div class="timeline-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IDSoZBAf7Lk" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                        <div class="timeline-footer">
                           <!-- <a href="#" class="btn btn-xs bg-maroon">See comments</a>-->
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
                <!-- timeline item -->
                <li>
                    <i class="fa fa-video-camera bg-maroon"></i>

                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> </span>

                        <h3 class="timeline-header"><a href="#">Visita a Sucursal</a> Explicación completa de como realizar una visita de checklist</h3>

                        <div class="timeline-body">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/O04SSzPJzF0" frameborder="0" allowfullscreen=""></iframe>
                            </div>
                        </div>
                        <div class="timeline-footer">
                            <!-- <a href="#" class="btn btn-xs bg-maroon">See comments</a>-->
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
                <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                </li>
            </ul>
        </div>
        <!-- /.col -->
    </div>


@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

@stop