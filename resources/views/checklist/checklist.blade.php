@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>
        Checklist

    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Checklist</li>
    </ol>


@stop

@section('content')
    <div class="row" id="viewChecklist">
        <div class="col-md-3">
           <!-- <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>-->
            <!-- /. box -->
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Checklist</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body ">
                    <select class="form-control select2" id="chk"  name="checklist" style="width: 100%;">
                        <option value="0" selected="selected">--Selecciona--</option>
                        @foreach($checklist as $chk)
                            <option value="{{$chk->Id}}"> {{$chk->nombre}}</option>

                        @endforeach

                    </select>
                    <hr>
                    <button type="button" onclick="showFormChk()" id="btnAddChecklist" class="btn btn-block btn-success">Agregar Checklist</button>
                    <hr>
                    <button type="button"  onclick="showFormChkEdit()" onclick="" id="btnEditChecklist" class="btn btn-block btn-warning">Editar Checklist</button>
                    <hr>
                    <button type="button"  onclick="showFormChkDel()" id="btnDelChecklist" class="btn btn-block btn-danger">Desactivar Checklist</button>
                </div>
                <!-- /.box-body -->

            </div>

            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Categorías</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body ">
                    <ul class="nav nav-pills nav-stacked ajax-content">
                    </ul>
                    <hr>
                    <button type="button" onclick="showFormCat()" id="btnAddCategoria" class="btn btn-block btn-success">Agregar Categoria</button>
                    <hr>
                    <button type="button"  onclick="showFormCatEdit()" id="btnEditCategoria" class="btn btn-block btn-warning">Editar Categoria</button>
                    <hr>
                    <button type="button" onclick="showFormCatDel()"  id="btnDelCategoria" class="btn btn-block btn-danger">Quitar Categoria</button>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9" id="showCampos">
            <div class="box">
                <div class="box-header ">
                    <button type="button" onclick="showFormCam()" id="btnAddCampo" class="btn btn-success pull-right">Agregar Campo</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <table id="example" class=" table table-bordered table-striped display" width="100%"></table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->


    <div class="box box-primary" id="agregaChecklist">
        <div class="box-header with-border">
            <h3 class="box-title">Agregar Checklist</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre del Checklist</label>
                    <input type="text" class="form-control" id="inputChecklist" placeholder="Define el checklist">
                </div>


            </div>
            <!-- /.box-body -->

            <div class="box-footer botonesChecklist">
                <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                <button type="button" onclick="setChecklist()"  class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>

    <div class="box box-warning" id="agregaCategoria">
        <div class="box-header with-border">
            <h3 class="box-title txtCategoria">Agregar Categoría</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" class="form-control" id="inputCat" placeholder="Define categoría">
                </div>

                    <input type="hidden" class="form-control" id="inputDesc" placeholder="Describe categoría">

              

            </div>
            <!-- /.box-body -->

            <div class="box-footer botonesCategoria">
                <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                <button type="button" onclick="setCategoria()"  class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>

    <div class="box box-success editarCampo" id="agregarCampo">
        <div class="box-header with-border">
            <h3 class="box-title titleCampo">Agregar campo a la categoría</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form action="{{route('regFotoCampoChk')}}" method="post" style="display: none" id="avatarForm">
            <input type="file" id="avatarInput" name="photo">
        </form>
        <form role="form">
            <div class="box-body">
                <div class="attachment-block clearfix">
                    <div class="rutaFoto">
                        <img onclick="setFoto()" id="avatarImage"  class="attachment-img" src="{{ asset('uploads/Campos/no_imagen.jpg')}}"  alt="Attachment Image">

                    </div>


                    <div class="attachment-pushed">
                        <h4 class="attachment-heading"><a href="http://www.lipsum.com/">Foto de Campo</a></h4>

                        <div class="attachment-text">
                            Esta foto indica una representación cercana a las condiciones que debe cumplir el campo para calificarse positivo
                        </div>
                        <!-- /.attachment-text -->
                    </div>
                    <!-- /.attachment-pushed -->
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre del Campo</label>
                    <input type="text" class="form-control" id="inputCampo" placeholder="Nombra el campo">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Descripción del campo</label>
                    <input type="text" class="form-control" id="inputCampoDesc" placeholder="Describe el campo">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Valor del Campo</label>
                    <input type="number" class="form-control" id="inputValor" placeholder="Coloca el valor del campo">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Selecciona la familia</label>
                    <select class="form-control select2" id="fam"  name="familias" style="width: 100%;">
                        <option value="0" selected="selected">--Selecciona--</option>
                        @foreach($familias as $fam)
                            <option value="{{$fam->Id}}"> {{$fam->Nombre}}</option>

                        @endforeach

                    </select>
                </div>


                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="noAplica"> Seleccionar si el campo no aplica en algunas situaciones
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" id="smart"> Seleccionar si el campo tendrá formato SMART
                    </label>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <a onclick="cancel()" class="btn btn-danger">Cancelar</a>
                <button type="button" onclick="addCampo()"  class="btn btn-primary">Agregar</button>
            </div>
        </form>
    </div>
    <div class="foto_campo" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_campo" value="no_imagen.jpg" >
    </div>
    <div class="categoria_id" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="categoria_id" value="0" >
    </div>

@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script>
        $('#example1').DataTable( {
            "pagingType": "full_numbers",
            "scrollY":        '60vh',
            "scrollCollapse": true,
            "language": {
                "lengthMenu": "Mostrando _MENU_ registros por página ",
                "zeroRecords": "Sin registros encontrados",
                "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                "infoEmpty": "Sin registros encontrados",
                "infoFiltered": "(filtrados de _MAX_ registros totales)",
                "search": "Buscar:",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Último",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                }
            }
        } );


        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false
            })
        })
    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })



        })
    </script>
    <script>

        var token = '{{csrf_token()}}';
        var url   = '{{route('getcat')}}'
        var urlcam   = '{{route('getcam')}}'
    </script>
    <script>

        function addCampo()
        {
            if(document.getElementById('inputCampo').value == "")
            {
                bootbox.alert("Debes definir un nombre de campo")
            }else {

                if(document.getElementsByName('familias')[0].value == 0)
                {
                    bootbox.alert("Debes seleccionar una familia para el campo")
                }else
                {

                    var campo = document.getElementById('inputCampo').value;
                    var campo_desc = document.getElementById('inputCampoDesc').value
                    var valor = document.getElementById('inputValor').value
                    var foto = document.getElementById('foto_campo').value
                    var familia_Id = document.getElementsByName('familias')[0].value
                    var no_aplica = 0;
                    var smart = 0;
                    var categorias_Id = document.getElementById('categoria_id').value


                    if (document.getElementById('noAplica').checked)
                    {
                        no_aplica = 1;
                    }
                    if (document.getElementById('smart').checked)
                    {
                        smart = 1;
                    }



                    var url = '{{route('addCampo')}}';
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {
                            nombre: campo,
                            campo_desc: campo_desc,
                            valor: valor,
                            foto: foto,
                            familia_Id : familia_Id,
                            no_aplica: no_aplica,
                            smart: smart,
                            categorias_Id: categorias_Id
                        }
                    })
                        .done(function(msg){

                            $("#viewChecklist").show();
                            $("#agregarCampo").hide();

                            $.ajax({
                                method: 'POST',
                                url: urlcam,
                                data: {body:categorias_Id, postId: '', _token: token}
                            })
                                .done(function(msg){

                                    $("#btnAddCampo").removeAttr('disabled');

                                    // $("input:checked" ).val()
                                    console.log(msg['message'])
                                    var table = "";
                                    for(var i = 0; i < msg['message'].length; i++)
                                    {
                                        table += msg['message'][i]['nombre'];
                                        table += '<tr> '+
                                            '   <td>'+msg['message'][i]['familia']+'</td>'+
                                            '   <td>'+msg['message'][i]['nombre']+'</td>' +
                                            '   <td>'+msg['message'][i]['descripcion']+'</td>'+
                                            '   <td>'+msg['message'][i]['valor']+'</td>'+
                                            '   <td>'+msg['message'][i]['valor']+'</td>'+
                                            '   <td></td>'+
                                            '   <td></td>'+
                                            '</tr>';

                                    }
                                    $('.ajax-table').html( table );

                                    var dataSet = [];
                                    for(var i = 0; i < msg['message'].length; i++)
                                    {
                                        dataSet.push( [msg['message'][i]['familia'],
                                            msg['message'][i]['nombre'],
                                            msg['message'][i]['descripcion'],
                                            msg['message'][i]['valor'],
                                            '  <button type="button" onclick="showFormCamEdit('+msg['message'][i]['Id']+')"  class="btn btn-primary">Editar</button>',
                                            '  <button type="button" onclick="showFormCamDel('+msg['message'][i]['Id']+')"  class="btn btn-danger">Borrar</button>'
                                        ] );


                                    }

                                    table = $('#example').DataTable( {

                                        "scrollY":        '60vh',
                                        "scrollCollapse": true,

                                        destroy: true,
                                        "language": {
                                            "lengthMenu": "Mostrando _MENU_ registros por página ",
                                            "zeroRecords": "Sin registros encontrados",
                                            "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                                            "infoEmpty": "Sin registros encontrados",
                                            "infoFiltered": "(filtrados de _MAX_ registros totales)",
                                            "search": "Buscar:",
                                            "paginate": {
                                                "first":      "Primero",
                                                "last":       "Último",
                                                "next":       "Siguiente",
                                                "previous":   "Anterior"
                                            }
                                        },
                                        data: dataSet,
                                        columns: [
                                            { title: "Familia" },
                                            { title: "Nombre" },
                                            { title: "Descripcion" },
                                            { title: "Valor" },
                                            { title: "" },
                                            { title: "" },

                                        ]
                                    } );

                                });
                        } );

                }

            }
        }
        $(document).ready(function() {

            $("#viewChecklist").show();
            $("#agregaChecklist").hide();
            $("#agregaCategoria").hide();
            $("#agregarCampo").hide();
            $("#btnAddCategoria").attr("disabled", "disabled");
            $("#btnAddCampo").attr("disabled", "disabled");

        } );
        function getCampos($msj)
        {
            $('.categoria_id').html( '<input type="hidden" id="categoria_id" value="'+$msj+'" >' );
            //alert($msj);
            $.ajax({
                method: 'POST',
                url: urlcam,
                data: {body:$msj, postId: '', _token: token}
            })
                .done(function(msg){

                    $("#btnAddCampo").removeAttr('disabled');

                   // $("input:checked" ).val()
                    console.log(msg['message'])
                    var table = "";
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        table += msg['message'][i]['nombre'];
                        table += '<tr> '+
                                 '   <td>'+msg['message'][i]['familia']+'</td>'+
                                 '   <td>'+msg['message'][i]['nombre']+'</td>' +
                                 '   <td>'+msg['message'][i]['descripcion']+'</td>'+
                                 '   <td>'+msg['message'][i]['valor']+'</td>'+
                                 '   <td></td>'+
                                    '   <td></td>'+
                                 '</tr>';

                    }
                    $('.ajax-table').html( table );

                    var dataSet = [];

                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        dataSet.push( [msg['message'][i]['familia'],
                                       msg['message'][i]['nombre'],
                                       msg['message'][i]['descripcion'],
                                       msg['message'][i]['valor'],
                                        '  <button type="button" onclick="showFormCamEdit('+msg['message'][i]['Id']+')"  class="btn btn-primary">Editar</button>',
                                        '  <button type="button" onclick="showFormCamDel('+msg['message'][i]['Id']+')"  class="btn btn-danger">Borrar</button>'
                                      ] );


                    }

                   table = $('#example').DataTable( {

                       "scrollY":        '60vh',
                       "scrollCollapse": true,

                       destroy: true,
                        "language": {
                            "lengthMenu": "Mostrando _MENU_ registros por página ",
                            "zeroRecords": "Sin registros encontrados",
                            "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                            "infoEmpty": "Sin registros encontrados",
                            "infoFiltered": "(filtrados de _MAX_ registros totales)",
                            "search": "Buscar:",
                            "paginate": {
                                "first":      "Primero",
                                "last":       "Último",
                                "next":       "Siguiente",
                                "previous":   "Anterior"
                            }
                        },
                        data: dataSet,
                        columns: [
                            { title: "Familia" },
                            { title: "Nombre" },
                            { title: "Descripcion" },
                            { title: "Valor" },
                            { title: "" },
                            { title: "" }

                        ]
                    } );

                    $("#showCampos").show();

                });
        }


        $('#chk').on('change' , function () {
           // alert(document.getElementsByName('checklist')[0].value);$("#btnAddPregunta").removeAttr('disabled');

            if(document.getElementsByName('checklist')[0].value == 0)
            {
                $("#viewChecklist").show();
                $("#agregaChecklist").hide();
                $("#agregaCategoria").hide();
                $("#agregarCampo").hide();
                $("#btnAddCategoria").attr("disabled", "disabled");
            }else
            {
                $("#btnAddCategoria").removeAttr('disabled');
                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {body:document.getElementsByName('checklist')[0].value, postId: '', _token: token}
                })
                    .done(function(msg){

                        var cat = "";
                        console.log(msg['message'])
                        var text = "";
                        for(var i = 0; i < msg['message'].length; i++)
                        {
                             cat = msg['message'][i]['nombre'];
                            // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                            text += '<li> <label>  <input  type="radio" onclick="getCampos('+msg['message'][i]['Id']+');"   value="'+msg['message'][i]['nombre']+'" name="r1" class="iradio_flat-green"> '+msg['message'][i]['nombre']+ '</label> </li>'
                        }
                        $('.ajax-content').html( text )
                    } );
            }

        });


        function cancel()
        {
            //alert('OK')
            url = '{{route('checklist.checklist')}}';//'visitas/final/'+post_id;//
            window.location.href = url;

        }

        function setChecklist()
        {
            var url_r   = '{{route('addChecklist')}}'
            if(document.getElementById('inputChecklist').value == "")
            {
                bootbox.alert("Debes definir el nombre del checklist");
            }else
            {
                $.ajax({
                    method: 'POST',
                    url: url_r,
                    data: {name:document.getElementById('inputChecklist').value }
                })
                    .done(function(msg){
                        console.log(msg['message'])
                        var url = "";
                        url = '{{route('checklist.checklist')}}';//'visitas/final/'+post_id;//
                        window.location.href = url;

                    } );
            }
        }

        function updChecklist()
        {
            if(document.getElementById('chk').value != 0)
            {
                var url_r   = '{{route('updChecklist')}}'
                if(document.getElementById('inputChecklist').value == "")
                {
                    bootbox.alert("Debes definir el nombre del checklist");
                }else
                {
                    $.ajax({
                        method: 'POST',
                        url: url_r,
                        data: {name:document.getElementById('inputChecklist').value,
                            id:document.getElementById('chk').value}
                    })
                        .done(function(msg){
                            console.log(msg['message'])
                            bootbox.alert("La modificación del checklist la verás reflejada cuando actualices la página")
                            $("#viewChecklist").show();
                            $("#agregaChecklist").hide();
                        } );
                }
            }else
            {
                bootbox.alert("Debes seleccionar un checklist primero");
            }

        }

        function setCategoria()
        {
            var chk_id = $('#chk option:selected').val();

          // alert(chk_id)
            var url_r   = '{{route('addCategoria')}}'
            if(document.getElementById('inputCat').value == "")
            {
                bootbox.alert("Debes definir el nombre de la categoría");
            }else
            {
                $.ajax({
                    method: 'POST',
                    url: url_r,
                    data: {name:document.getElementById('inputCat').value, desc: document.getElementById('inputDesc').value , chk_Id: chk_id }
                })
                    .done(function(msg){
                        console.log(msg['message'])

                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {body:document.getElementsByName('checklist')[0].value, postId: '', _token: token}
                        })
                            .done(function(msg){
                                console.log(msg['message'])
                                var text = "";
                                for(var i = 0; i < msg['message'].length; i++)
                                {
                                    // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                                    text += '<li> <label>  <input  type="radio" onclick="getCampos('+msg['message'][i]['Id']+'); "  value="'+msg['message'][i]['nombre']+'"   name="r1" class="iradio_flat-green"> '+msg['message'][i]['nombre']+ '</label> </li>'
                                }
                                $('.ajax-content').html( text )

                                $("#viewChecklist").show();
                                $("#agregaChecklist").hide();
                                $("#agregaCategoria").hide();
                                $("#agregarCampo").hide();
                            } );


                    } );
            }
        }

        function showFormChk()
        {
            $("#viewChecklist").hide();
            $("#agregaChecklist").show();


        }

        function showFormChkEdit()
        {

            if(document.getElementById('chk').value != 0)
            {
                $("#viewChecklist").hide();
                $("#agregaChecklist").show();
                document.getElementById('inputChecklist').value = $("#chk option:selected").text()

                var txt = '<a onclick="cancel()" class="btn btn-danger">Cancelar</a>\n' +
                    '                <button type="button" onclick="updChecklist()"  class="btn btn-primary">Actualizar</button>';
                $('.botonesChecklist').html(txt)
            }else
            {
                bootbox.alert("Debes seleccionar un checklist primero");
            }

        }

        function showFormChkDel()
        {

            if(document.getElementById('chk').value != 0)
            {
                var box = bootbox.confirm({
                    title: "Baja de Checklist",
                    message: "¿Deseas dar de baja el checklist seleccionado?, sólo podrá ser reactivado por solicitud a soporte de ventum.",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> No'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Si'
                        }
                    },
                    callback: function (result) {
                        console.log(result);

                        if(result == true)
                        {

                            var post_id = document.getElementById('chk').value;

                            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })
                            var url = '{{route('delChecklist')}}';

                            $.ajax({
                                method: 'POST',
                                url: url,
                                data: {id:post_id, }
                            })
                                .done(function(msg){
                                    bootbox.hideAll();
                                    bootbox.alert("Checklist dado de baja, para reactivarlo, pide apoyo del personal de ventum")
                                    url = '{{route('checklist.checklist')}}';//'visitas/final/'+post_id;//
                                    window.location.href = url;
                                });

                        }else
                        {
                            bootbox.hideAll();
                        }

                    }
                });
            }else
            {
                bootbox.alert("Debes seleccionar un checklist primero");
            }



        }

        function showFormCat()
        {
            $("#viewChecklist").hide();
            $("#agregaCategoria").show();

            $('.txtCategoria').html( 'Agregar categoría al checklist ' + $('#chk option:selected').text() )
        }

        function showFormCatEdit()
        {

            if(document.getElementById('categoria_id').value != 0)
            {
                $("#viewChecklist").hide();
                $("#agregaCategoria").show();
                $('.txtCategoria').html( 'Editar categoría al checklist ' + $('#chk option:selected').text() )
                document.getElementById('inputCat').value = $("input:checked" ).val();

                var txt = '<a onclick="cancel()" class="btn btn-danger">Cancelar</a>\n' +
                    '                <button type="button" onclick="updCategoria()"  class="btn btn-primary">Actualizar</button>';
                $('.botonesCategoria').html(txt)
               // alert(document.getElementById('categoria_id').value +""+ $("input:checked" ).val());
            }else
            {
                bootbox.alert("Debes seleccionar una categoría primero")
            }

        }

        function updCategoria()
        {
            var url_r   = '{{route('updCategoria')}}'
            if(document.getElementById('inputCat').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url_r,
                    data: {name:document.getElementById('inputCat').value, id: document.getElementById('categoria_id').value }
                })
                    .done(function(msg){
                        console.log(msg['message'])

                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {body:document.getElementsByName('checklist')[0].value, postId: '', _token: token}
                        })
                            .done(function(msg){
                                console.log(msg['message'])
                                var text = "";
                                for(var i = 0; i < msg['message'].length; i++)
                                {
                                    // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                                    text += '<li> <label>  <input  type="radio" onclick="getCampos('+msg['message'][i]['Id']+'); "  value="'+msg['message'][i]['nombre']+'"   name="r1" class="iradio_flat-green"> '+msg['message'][i]['nombre']+ '</label> </li>'
                                }
                                $('.ajax-content').html( text )

                                $("#viewChecklist").show();
                                $("#agregaChecklist").hide();
                                $("#agregaCategoria").hide();
                                $("#agregarCampo").hide();
                                $("#showCampos").show();
                            } );


                    } );
            }else
            {
                bootbox.alert('Debes definir el nombre de la categoría');
            }

        }

        function showFormCatDel()
        {


            if(document.getElementById('categoria_id').value != 0)
            {

                var box = bootbox.confirm({
                    title: "Baja de Categoría",
                    message: "¿Deseas dar de baja la categoría seleccionada?, sólo podrá ser reactivada por solicitud a soporte de ventum.",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> No'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Si'
                        }
                    },
                    callback: function (result) {
                        console.log(result);

                        if(result == true)
                        {

                            var post_id = document.getElementById('categoria_id').value;

                            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })
                            var url_d = '{{route('delCategoria')}}';

                            $.ajax({
                                method: 'POST',
                                url: url_d,
                                data: {id:post_id, }
                            })
                                .done(function(msg){
                                    bootbox.hideAll();
                                    bootbox.alert("Categoría dada de baja, para reactivarlo, pide apoyo del personal de ventum")

                                    $.ajax({
                                        method: 'POST',
                                        url: url,
                                        data: {body:document.getElementsByName('checklist')[0].value, postId: '', _token: token}
                                    })
                                        .done(function(msg){
                                            console.log(msg['message'])
                                            var text = "";
                                            for(var i = 0; i < msg['message'].length; i++)
                                            {
                                                // text += ' <li><a href="#"><i class="fa fa-circle-o text-red"></i>'+msg['message'][i]['nombre']+'</a></li>'
                                                text += '<li> <label>  <input  type="radio" onclick="getCampos('+msg['message'][i]['Id']+'); "  value="'+msg['message'][i]['nombre']+'"   name="r1" class="iradio_flat-green"> '+msg['message'][i]['nombre']+ '</label> </li>'
                                            }
                                            $('.ajax-content').html( text )

                                            $("#viewChecklist").show();
                                            $("#agregaChecklist").hide();
                                            $("#agregaCategoria").hide();
                                            $("#agregarCampo").hide();
                                            $("#showCampos").hide();
                                        } );

                                });

                        }else
                        {
                            bootbox.hideAll();
                        }

                    }
                });

            }else
            {
                bootbox.alert("Debes seleccionar una categoría primero")
            }
        }

        function showFormCam()
        {
            $("#viewChecklist").hide();
            $("#agregarCampo").show();
            $('.titleCampo').html("Agregar campo a " + $("input:checked" ).val() )
            //$("input:checked" ).val()
        }

        function showFormCamEdit(id)
        {
            $("#viewChecklist").hide();
            $("#agregarCampo").show();

            //$("input:checked" ).val()

            var url_r   = '{{route('editCampo')}}'

            $.ajax({
                method: 'POST',
                url: url_r,
                data: {id:id}
            })
                .done(function(msg){
                    console.log(msg['message'])
                   var text = "";
                    var fotoUrl = '{{route('regFotoCampoChk')}}';
                    var fotoAvatar = '{{ asset('uploads/Campos/')}}'

                    var noaplica = "";
                    var smart  = "";

                    if(msg['message']['no_aplica'] != 0)
                    {
                        noaplica = "checked";
                    }
                    if(msg['message']['smart'] != 0)
                    {
                        smart = "checked";
                    }
                    text += '<div class="box-header with-border">\n' +
                        '            <h3 class="box-title titleCampo">Agregar campo a la categoría</h3>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <!-- form start -->\n' +
                        '        <form action="'+fotoUrl+'" method="post" style="display: none" id="avatarForm">\n' +
                        '            <input type="file" id="avatarInput" name="photo">\n' +
                        '        </form>\n' +
                        '        <form role="form">\n' +
                        '            <div class="box-body">\n' +
                        '                <div class="attachment-block clearfix">\n' +
                        '                    <div class="rutaFoto">\n' +
                        '                        <img onclick="setFoto()" id="avatarImage"  class="attachment-img" src="'+fotoAvatar+'/'+msg['message']['foto']+'"  alt="Attachment Image">\n' +
                        '\n' +
                        '                    </div>\n' +
                        '\n' +
                        '\n' +
                        '                    <div class="attachment-pushed">\n' +
                        '                        <h4 class="attachment-heading"><a >Foto de Campo</a></h4>\n' +
                        '\n' +
                        '                        <div class="attachment-text">\n' +
                        '                            Esta foto indica una representación cercana a las condiciones que debe cumplir el campo para calificarse positivo\n' +
                        '                        </div>\n' +
                        '                        <!-- /.attachment-text -->\n' +
                        '                    </div>\n' +
                        '                    <!-- /.attachment-pushed -->\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label for="exampleInputEmail1">Nombre del Campo</label>\n' +
                        '                    <input type="text" class="form-control" id="inputCampo" value="'+msg['message']['nombre']+'" placeholder="Nombra el campo">\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label for="exampleInputEmail1">Descripción del campo</label>\n' +
                        '                    <input type="text" class="form-control" id="inputCampoDesc" value="'+msg['message']['descripcion']+'" placeholder="Describe el campo">\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label for="exampleInputEmail1">Valor del Campo</label>\n' +
                        '                    <input type="number" class="form-control" id="inputValor" value="'+msg['message']['valor']+'" placeholder="Coloca el valor del campo">\n' +
                        '                </div>\n' +
                        '                <div class="form-group">\n' +
                        '                    <label for="exampleInputEmail1">Selecciona la familia</label>\n' +
                        '                    <select class="form-control select2" id="fam"  name="familias" style="width: 100%;">\n' +
                        '                        <option value="'+msg['message']['familias_Id']+'" selected="selected">'+msg['message']['familia']+'</option>\n';

                        for(var i = 0; i < msg['familias'].length; i++)
                        {
                            text += '  <option value="'+msg['familias'][i]['Id']+'"> '+msg['familias'][i]['Nombre']+'</option>';
                        }

                       text +=  '                    </select>\n' +
                        '                </div>\n' +
                        '\n' +
                        '\n' +
                        '                <div class="checkbox">\n' +
                        '                    <label>\n' +
                        '                        <input type="checkbox" '+noaplica+' id="noAplica"> Seleccionar si el campo no aplica en algunas situaciones\n' +
                        '                    </label>\n' +
                        '                </div>\n' +
                        '                <div class="checkbox">\n' +
                        '                    <label>\n' +
                        '                        <input type="checkbox"  '+smart+' id="smart"> Seleccionar si el campo tendrá formato SMART\n' +
                        '                    </label>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '\n' +
                        '            <div class="box-footer">\n' +
                        '                <a onclick="cancel()" class="btn btn-danger">Cancelar</a>\n' +
                        '                <button type="button" onclick="updCampo('+id+')"  class="btn btn-primary">Actualizar</button>\n' +
                        '            </div>\n' +
                        '        </form>';


                    $('.editarCampo').html( text);

                    $('.titleCampo').html("Editar campo a " + $("input:checked" ).val() )
                } );

        }

        function updCampo(id)
        {
            if(document.getElementById('inputCampo').value == "")
            {
                bootbox.alert("Debes definir un nombre de campo")
            }else {

                if(document.getElementsByName('familias')[0].value == 0)
                {
                    bootbox.alert("Debes seleccionar una familia para el campo")
                }else
                {

                    var campo = document.getElementById('inputCampo').value;
                    var campo_desc = document.getElementById('inputCampoDesc').value
                    var valor = document.getElementById('inputValor').value
                    var foto = document.getElementById('foto_campo').value
                    var familia_Id = document.getElementsByName('familias')[0].value
                    var no_aplica = 0;
                    var smart = 0;
                    var categorias_Id = document.getElementById('categoria_id').value


                    if (document.getElementById('noAplica').checked)
                    {
                        no_aplica = 1;
                    }
                    if (document.getElementById('smart').checked)
                    {
                        smart = 1;
                    }



                    var url = '{{route('updateCampo')}}';
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: { id: id,
                            nombre: campo,
                            campo_desc: campo_desc,
                            valor: valor,
                            foto: foto,
                            familia_Id : familia_Id,
                            no_aplica: no_aplica,
                            smart: smart,
                            categorias_Id: categorias_Id
                        }
                    })
                        .done(function(msg){

                            $("#viewChecklist").show();
                            $("#agregarCampo").hide();

                            $.ajax({
                                method: 'POST',
                                url: urlcam,
                                data: {body:categorias_Id, postId: '', _token: token}
                            })
                                .done(function(msg){

                                    $("#btnAddCampo").removeAttr('disabled');

                                    // $("input:checked" ).val()
                                    console.log(msg['message'])
                                    var table = "";
                                    for(var i = 0; i < msg['message'].length; i++)
                                    {
                                        table += msg['message'][i]['nombre'];
                                        table += '<tr> '+
                                            '   <td>'+msg['message'][i]['familia']+'</td>'+
                                            '   <td>'+msg['message'][i]['nombre']+'</td>' +
                                            '   <td>'+msg['message'][i]['descripcion']+'</td>'+
                                            '   <td>'+msg['message'][i]['valor']+'</td>'+
                                            '   <td>'+msg['message'][i]['valor']+'</td>'+
                                            '   <td></td>'+
                                            '   <td></td>'+
                                            '</tr>';

                                    }
                                    $('.ajax-table').html( table );

                                    var dataSet = [];
                                    for(var i = 0; i < msg['message'].length; i++)
                                    {
                                        dataSet.push( [msg['message'][i]['familia'],
                                            msg['message'][i]['nombre'],
                                            msg['message'][i]['descripcion'],
                                            msg['message'][i]['valor'],
                                            '  <button type="button" onclick="showFormCamEdit('+msg['message'][i]['Id']+')"  class="btn btn-primary">Editar</button>',
                                            '  <button type="button" onclick="showFormCamDel('+msg['message'][i]['Id']+')"  class="btn btn-danger">Borrar</button>'
                                        ] );


                                    }

                                    table = $('#example').DataTable( {

                                        "scrollY":        '60vh',
                                        "scrollCollapse": true,

                                        destroy: true,
                                        "language": {
                                            "lengthMenu": "Mostrando _MENU_ registros por página ",
                                            "zeroRecords": "Sin registros encontrados",
                                            "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                                            "infoEmpty": "Sin registros encontrados",
                                            "infoFiltered": "(filtrados de _MAX_ registros totales)",
                                            "search": "Buscar:",
                                            "paginate": {
                                                "first":      "Primero",
                                                "last":       "Último",
                                                "next":       "Siguiente",
                                                "previous":   "Anterior"
                                            }
                                        },
                                        data: dataSet,
                                        columns: [
                                            { title: "Familia" },
                                            { title: "Nombre" },
                                            { title: "Descripcion" },
                                            { title: "Valor" },
                                            { title: "" },
                                            { title: "" },

                                        ]
                                    } );

                                });
                        } );

                }

            }
        }

        function showFormCamDel(id)
        {
          //alert(id)
            //$("input:checked" ).val()
            var box = bootbox.confirm({
                title: "Baja de Campo",
                message: "¿Deseas dar de baja el campo seleccionado?, sólo podrá ser reactivado por solicitud a soporte de ventum.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {

                        var post_id = id;

                        bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })
                        var url_d = '{{route('delCampo')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_d,
                            data: {id:post_id }
                        })
                            .done(function(msg){
                                bootbox.hideAll();
                                bootbox.alert("Campo dado de baja, para reactivarlo, pide apoyo del personal de ventum")
                                $("#viewChecklist").show();
                                $("#agregarCampo").hide();

                                var categorias_Id = document.getElementById('categoria_id').value

                                $.ajax({
                                    method: 'POST',
                                    url: urlcam,
                                    data: {body:categorias_Id, postId: '', _token: token}
                                })
                                    .done(function(msg){

                                        $("#btnAddCampo").removeAttr('disabled');

                                        // $("input:checked" ).val()
                                        console.log(msg['message'])
                                        var table = "";
                                        for(var i = 0; i < msg['message'].length; i++)
                                        {
                                            table += msg['message'][i]['nombre'];
                                            table += '<tr> '+
                                                '   <td>'+msg['message'][i]['familia']+'</td>'+
                                                '   <td>'+msg['message'][i]['nombre']+'</td>' +
                                                '   <td>'+msg['message'][i]['descripcion']+'</td>'+
                                                '   <td>'+msg['message'][i]['valor']+'</td>'+
                                                '   <td>'+msg['message'][i]['valor']+'</td>'+
                                                '   <td></td>'+
                                                '   <td></td>'+
                                                '</tr>';

                                        }
                                        $('.ajax-table').html( table );

                                        var dataSet = [];
                                        for(var i = 0; i < msg['message'].length; i++)
                                        {
                                            dataSet.push( [msg['message'][i]['familia'],
                                                msg['message'][i]['nombre'],
                                                msg['message'][i]['descripcion'],
                                                msg['message'][i]['valor'],
                                                '  <button type="button" onclick="showFormCamEdit('+msg['message'][i]['Id']+')"  class="btn btn-primary">Editar</button>',
                                                '  <button type="button" onclick="showFormCamDel('+msg['message'][i]['Id']+')"  class="btn btn-danger">Borrar</button>'
                                            ] );


                                        }

                                        table = $('#example').DataTable( {

                                            "scrollY":        '60vh',
                                            "scrollCollapse": true,

                                            destroy: true,
                                            "language": {
                                                "lengthMenu": "Mostrando _MENU_ registros por página ",
                                                "zeroRecords": "Sin registros encontrados",
                                                "info": "Mostrando página  _PAGE_ de _PAGES_ páginas",
                                                "infoEmpty": "Sin registros encontrados",
                                                "infoFiltered": "(filtrados de _MAX_ registros totales)",
                                                "search": "Buscar:",
                                                "paginate": {
                                                    "first":      "Primero",
                                                    "last":       "Último",
                                                    "next":       "Siguiente",
                                                    "previous":   "Anterior"
                                                }
                                            },
                                            data: dataSet,
                                            columns: [
                                                { title: "Familia" },
                                                { title: "Nombre" },
                                                { title: "Descripcion" },
                                                { title: "Valor" },
                                                { title: "" },
                                                { title: "" },

                                            ]
                                        } );

                                    });


                            });

                    }else
                    {
                        bootbox.hideAll();
                    }

                }
            });
        }

        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_campo').html('<input type="hidden" id="foto_campo" value="' + msg['message'] + '" >')


                });
            });
        }
    </script>

@stop