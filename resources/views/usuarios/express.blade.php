@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>To-Do Express</h1>
@stop

@section('content')

    <!-- /.box -->
    <div class="row" id="agregarEmpleado">


            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">

                    <form action="{{route('regFotoCampo')}}" method="post" style="display: none" id="avatarForm">
                        <input type="file" id="avatarInput" name="photo">
                    </form>
                    <div class="rutaFoto">
                        <img onclick="setFoto()" class="profile-user-img img-responsive img-circle" id="avatarImage" src="{{ asset('uploads/FotoTodo/no_imagen.jpg')}}" alt="User profile picture">

                    </div>

                    <h3 class="profile-username text-center">Foto de To-Do</h3>

                    <!--<p class="text-muted text-center">Software Engineer</p>-->

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item tiendas">
                            <strong><i class="fa fa-book margin-r-5"></i> Elige Tienda</strong>

                            <select class="form-control select2" id="suc" name="sucursal" style="width: 100%;">
                                <option value="0" selected="selected">--Selecciona--</option>
                                @foreach($tiendas as $tienda)
                                    <option value="{{$tienda->Id}}" >{{$tienda->nombre}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="list-group-item supervisores">
                            <strong><i class="fa fa-book margin-r-5"></i>Asignar supervisor (opcional)</strong>

                            <select class="form-control select2" id="sup" name="supervisor" style="width: 100%;">
                                <option value="0" selected="selected">--Selecciona--</option>
                                @foreach($supervisores as $supervisor)
                                    <option value="{{$supervisor->Id}}" >{{$supervisor->nombre." ".$supervisor->apepat." ".$supervisor->apemat}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="list-group-item ">
                            <strong><i class="fa fa-book margin-r-5"></i>Describe el To-Do</strong>

                            <input class="form-control" type="text" placeholder="Describe el to-do" id="inputTodo" />
                        </li>
                        <li class="list-group-item  pull-right">
                            <a onclick="save()" class="btn btn-primary btnGuardar">Guardar</a>
                            <a href="{{route('home')}}" class="btn btn-danger">Cancelar</a>
                        </li>

                    </ul>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

    </div>
    <div class="foto_perfil" >
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <input type="hidden" id="foto_perfil" value="no_imagen.jpg" >
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>


    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()


            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })


        })


        $(document).ready(function() {

            // $("#agregarEmpleado").hide();
            //$("#agregaInfo").hide();



        } );

        function save()
        {
            //document.getElementById('id_visita').value

            var url = '{{route('setTodoExpress')}}';

            if(document.getElementById('suc').value != 0)
            {
                if(document.getElementById('inputTodo').value != "")
                {
                    var tienda = document.getElementById('suc').value;
                    var supervisor = document.getElementById('sup').value;
                    var descripcion = document.getElementById('inputTodo').value;
                    var imagen = document.getElementById('foto_perfil').value;

                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {tienda: tienda, supervisor: supervisor, descripcion: descripcion, imagen: imagen}
                    })
                        .done(function(msg){
                            console.log(msg['message']);
                            bootbox.alert("To-Do Express Agregado con Éxito")
                            var url_exp = '{{route('usuarios.express')}}'
                            window.location.href = url_exp;

                        });
                }else
                {
                    bootbox.alert("Debes describir el To-Do");
                }

            }else
            {
                bootbox.alert("Debes elegir una sucursal");
            }

        }



        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


                });
            });
        }




    </script>


@stop