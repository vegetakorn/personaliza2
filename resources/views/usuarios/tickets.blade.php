@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Tickets de Servicio</h1>
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Levanta un ticket de soporte técnico</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">¿Que módulo es el que presenta el problema?</label>
                    <select class="form-control select2" id="mod" name="modulo" style="width: 100%;">

                        <option value="0">Selecciona Modulo</option>
                        <option value="1">Visitas</option>
                        <option value="2">Ventas</option>
                        <option value="3">Empleados</option>
                        <option value="4">Otros</option>


                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Describe tu problema</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">¿Algún teléfono de contacto?</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">¿Algún correo electrónico adicional donde contactarte?</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Sube archivo</label>
                    <input type="file" id="exampleInputFile">

                    <p class="help-block">Para darte un mejor servicio, puedes adjuntar un archivo que soporte tu petición</p>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Avisarme cuando esté resuelto a mi correo
                    </label>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </form>
    </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>


    </script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()


            //Date picker
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
            })


        })


        $(document).ready(function() {

            // $("#agregarEmpleado").hide();
            //$("#agregaInfo").hide();



        } );


        function setFoto()
        {
            $avatarImage = $('#avatarImage');
            $avatarInput = $('#avatarInput');
            $avatarForm = $('#avatarForm');
            $rutaFoto = $('.rutaFoto');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    $('.foto_perfil').html('<input type="hidden" id="foto_perfil" value="' + msg['message'] + '" >')


                });
            });
        }




    </script>


@stop