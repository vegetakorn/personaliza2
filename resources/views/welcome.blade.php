<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('flatty/img/favicon.png')}}">

    <title>Ventum Supervisión - Página Principal</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('flatty/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('flatty/css/main.css')}}" rel="stylesheet">

    <!-- Fonts from Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" style="background-color: #97d3eb">
    <div class="container" style="background-color: #97d3eb">
        <div class="navbar-header" >
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><b><img src="{{ asset('images/logo_ventum.png')}}"></b></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{route('login')}}" style="color: #001F3F">Ingresa al sistema</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div id="headerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h1>Haz de la supervisión<br/>
                    una agradable experiencia.</h1>
                <form class="form-inline" role="form">
                    <div class="form-group">
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Envíanos tu correo">
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg">¡Contáctenme!</button>
                </form>
            </div><!-- /col-lg-6 -->
            <div class="col-lg-6">
                <img class="img-responsive" src="{{ asset('flatty/img/ipad-hand.png')}}" alt="">
            </div><!-- /col-lg-6 -->

        </div><!-- /row -->
    </div><!-- /container -->
</div><!-- /headerwrap -->


<div class="container">
    <div class="row mt centered">
        <div class="col-lg-6 col-lg-offset-3">
            <h1>La supervisión de tus tiendas<br/>nunca había sido tan divertido</h1>
            <h3>Ventum es una poderosa herramienta que ofrece valor agregado para todas aquellas personas que están a cargo de sucursales.</h3>
            <h3> Ofrece mecanismos de control y supervisión para permitir que las tiendas se dediquen para lo que fueron establecidas: vender.</h3>
        </div>
    </div><!-- /row -->

    <div class="row mt centered">
        <div class="col-lg-4">
            <img src="{{ asset('flatty/img/ser01.png')}}" width="180" alt="">
            <h4>1 - Multiplataforma</h4>
            <p>No importa en cual dispositivo visites la plataforma, ¡podrás acceder al sistema siempre!</p>
        </div><!--/col-lg-4 -->

        <div class="col-lg-4">
            <img src="{{ asset('flatty/img/ser02.png')}}" width="180" alt="">
            <h4>2 - Seguimiento Puntual</h4>
            <p>A través de correos automatizados lleva un control preciso de tu personal y/o tiendas asignadas</p>

        </div><!--/col-lg-4 -->

        <div class="col-lg-4">
            <img src="{{ asset('flatty/img/ser03.png')}}" width="180" alt="">
            <h4>3 - Reportes de Valor</h4>
            <p>No pierdas tiempo generando reportes de la supervisión de tu tienda, ¡Ventum lo hace por ti!</p>

        </div><!--/col-lg-4 -->
    </div><!-- /row -->
</div><!-- /container -->

<div class="container">
    <hr>
    <div class="row centered">
        <div class="col-lg-6 col-lg-offset-3">
            <form class="form-inline" role="form">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Envíanos tu correo">
                </div>
                <button type="submit" class="btn btn-warning btn-lg">¡Contáctenme!</button>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div><!-- /row -->
    <hr>
</div><!-- /container -->

<div class="container">
    <div class="row mt centered">
        <div class="col-lg-6 col-lg-offset-3">
            <h1>Ventum es para todos.</h1>
            <h3>No importa el tamaño de tu organización, Ventum se ajusta a tus necesidades de supervisión</h3>
        </div>
    </div><!-- /row -->

    <! -- CAROUSEL -->
    <div class="row mt centered">
        <div class="col-lg-6 col-lg-offset-3">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="{{ asset('flatty/img/screen1.png')}}" alt="">
                    </div>
                    <div class="item">
                        <img src="{{ asset('flatty/img/screen2.png')}}" alt="">
                    </div>
                    <div class="item">
                        <img src="{{ asset('flatty/img/screen3.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div><!-- /col-lg-8 -->
    </div><!-- /row -->
</div><! --/container -->

<div class="container">
    <hr>
    <div class="row centered">
        <div class="col-lg-6 col-lg-offset-3">
            <form class="form-inline" role="form">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Envíanos tu correo">
                </div>
                <button type="submit" class="btn btn-warning btn-lg">¡Contáctenme!</button>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div><!-- /row -->
    <hr>
</div><!-- /container -->

<div class="container">
    <div class="row mt centered">
        <div class="col-lg-6 col-lg-offset-3">
            <h1>Nuestro equipo de trabajo.<br/>Siempre a tu servicio.</h1>
            <h3>Siempre estaremos atentos a sus dudas y encontraremos la mejor solución de supervisión para su organización</h3>
        </div>
    </div><!-- /row -->

    <div class="row mt centered">
        <div class="col-lg-4">
            <img class="img-circle" src="{{ asset('flatty/img/alex.jpeg')}}" width="140" alt="">
            <h4>Alejandro López</h4>
            <p>Director General</p>
            <p><i class="glyphicon glyphicon-send"></i> <i class="glyphicon glyphicon-phone"></i> <i class="glyphicon glyphicon-globe"></i></p>
        </div><!--/col-lg-4 -->

        <div class="col-lg-4">
            <img class="img-circle" src="{{ asset('flatty/img/moi.jpeg')}}" width="140" alt="">
            <h4>Moisés Peña</h4>
            <p>Líder de Proyecto</p>
            <p><i class="glyphicon glyphicon-send"></i> <i class="glyphicon glyphicon-phone"></i> <i class="glyphicon glyphicon-globe"></i></p>
        </div><!--/col-lg-4 -->

        <div class="col-lg-4">
            <img class="img-circle" src="{{ asset('flatty/img/hector.jpg')}}" width="140" alt="">
            <h4>Héctor Arroyo</h4>
            <p>Desarrollador de Ventum</p>
            <p><i class="glyphicon glyphicon-send"></i> <i class="glyphicon glyphicon-phone"></i> <i class="glyphicon glyphicon-globe"></i></p>
        </div><!--/col-lg-4 -->
    </div><!-- /row -->
</div><!-- /container -->

<div class="container">
    <hr>
    <div class="row centered">
        <div class="col-lg-6 col-lg-offset-3">
            <form class="form-inline" role="form">
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Envíanos tu correo">
                </div>
                <button type="submit" class="btn btn-warning btn-lg">¡Contáctenme!</button>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div><!-- /row -->
    <hr>
    <p class="centered">Ventum Supervisión - 2018</p>
</div><!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="{{ asset('flatty/js/bootstrap.min.js')}}"></script>
</body>
</html>
