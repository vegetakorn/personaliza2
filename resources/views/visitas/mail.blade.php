

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>[SUBJECT]</title>
    <style type="text/css">
        body {background: #ffffff; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif; font-size:16px; line-height:150%; color:#444; margin:0; padding:0;}
        p,li {margin-bottom:14px; margin-top:0;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:16px;color:#999999; line-height:150%;}
        td, div {font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:13px; }
        a {color:#013275;text-decoration: none;font-weight: bold;}
        h1 {margin-top:15px;padding:0; color:#0066cc; font-size:30px; line-height:34px;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight: normal; text-align: center}
        h2 {margin-top:0; color:#4da6ff; font-size:18px;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:normal;}
        h3 {margin:0;font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, sans-serif;}
        .bgItem{background: #4da6ff;}
        ul {list-style-image: url(images/bullet.png);}


        @media only screen and (max-width:480px)

        {

            table[class="MainContainer"], td[class="cell"]
            {
                width: 100% !important;
                height:auto !important;
            }
            td[class="specbundle"]
            {
                width: 100% !important;
                float:left !important;
                font-size:13px !important;
                line-height:17px !important;
                display:block !important;
                padding-bottom:15px !important;
            }
            td[class="specbundle2"]
            {
                width:90% !important;
                float:left !important;
                font-size:14px !important;
                line-height:18px !important;
                display:block !important;
                padding-left:5% !important;
                padding-right:5% !important;
            }
            td[class="specbundle3"]
            {
                width:90% !important;
                float:left !important;
                font-size:14px !important;
                line-height:18px !important;
                display:block !important;
                padding-left:5% !important;
                padding-right:5% !important;
                padding-bottom:20px !important;
            }

            td[class="spechide"]
            {
                display:none !important;
            }
            img[class="banner"]
            {
                width: 100% !important;
                height: auto !important;
            }
            td[class="left_pad"]
            {
                padding-left:15px !important;
                padding-right:15px !important;
            }

        }

        @media only screen and (max-width:540px)

        {

            table[class="MainContainer"], td[class="cell"]
            {
                width: 100% !important;
                height:auto !important;
            }
            td[class="specbundle"]
            {
                width: 100% !important;
                float:left !important;
                font-size:13px !important;
                line-height:17px !important;
                display:block !important;
                padding-bottom:15px !important;
            }
            td[class="specbundle2"]
            {
                width:90% !important;
                float:left !important;
                font-size:14px !important;
                line-height:18px !important;
                display:block !important;
                padding-left:5% !important;
                padding-right:5% !important;
            }
            td[class="specbundle3"]
            {
                width:90% !important;
                float:left !important;
                font-size:14px !important;
                line-height:18px !important;
                display:block !important;
                padding-left:5% !important;
                padding-right:5% !important;
                padding-bottom:20px !important;
            }

            td[class="spechide"]
            {
                display:none !important;
            }
            img[class="banner"]
            {
                width: 100% !important;
                height: auto !important;
            }
            td[class="left_pad"]
            {
                padding-left:15px !important;
                padding-right:15px !important;
            }

            .font{
                font-size:26px !important;
                line-height:29px !important;

            }
        }

    </style>
    <script type="colorScheme" class="swatch active">
        {
          "name":"Default",
          "bgBody":"#ffffff",
          "link":"#013275",
          "color":"303841",
          "bgItem":"D70000",
          "title":"#D70000"
        }
      </script>
</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4"  style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
<!-- main container -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td class='movableContentContainer' align="center">
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                    <tbody>
                    <tr>
                        <td align="center" colspan="9" height="20">
                        </td>
                    </tr>
                    <tr>
                        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr>
                                    <td class="specbundle3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td valign="top" height="60" width="75">
                                                    <div class="contentEditableContainer contentImageEditable">
                                                        <div class="contentEditable">
                                                            <img data-default="placeholder" data-max-width="150" data-max-height="80" src="http://ventumsupervision.com/images/logo_ventum.png" width="200" height="80" ></div>
                                                    </div>
                                                </td>
                                                <td width="15">&nbsp;</td>
                                                <td width="330" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable">
                                                            <h1 style="text-align:left;">
                                                                </h1>
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="20" class="specbundle3"></td>
                                    <td class="specbundle3" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="65" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable">

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td width="65" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable">

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10">&nbsp;</td>
                                                <td width="65" valign="middle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                        <div class="contentEditable">

                                                        </div>
                                                    </div>
                                                </td>
                                                <td valign="top" height="60" width="75">
                                                    <div class="contentEditableContainer contentImageEditable">
                                                        <div class="contentEditable">
                                                            <img data-default="placeholder" data-max-width="150" data-max-height="80" src="http://ventumsupervision.com/uploads/Razones/{{$data['visita']->razonFoto}}" width="200" height="80" ></div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="9" height="20">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- END: Header: Logo; company name; website nav bar - Image + text -->

            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                    <tr>
                        <td class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h1 style="text-align:center;">Resultados de la visita de supervisión a la sucursal {{$data['visita']->numsuc}} {{$data['visita']->tienda}}</h1>
                                    <p style="color:#666666;text-align:left;">
                                        Visita realizada el día <strong><?php echo date('d/m/Y' , strtotime($data['visita']->HoraFin)) ?></strong>
                                        por el usuario <strong>{{$data['visita']->name}}</strong>. Evaluando con el checklist
                                        <a >{{$data['visita']->checklist}}</a>. con una duración de <strong>{{$data['visita']->Duracion}}</strong> </p>

                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                </table>
            </div>
            <!-- END: Featured Image -->

            <!-- Title -->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class='bgItem'>
                    <tr>
                        <td align="center" class='bgItem'>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="45">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                                <tr>
                                    <td width="180">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                    <td wdith="30">
                                    </td>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <h1 style="color:#ffffff;">Calificación</h1>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="30">
                                    </td>
                                    <td width="180" height="30">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                </tr>
                                <!-- .Section Title: Benefits-->

                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="15">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!--Benefits: 3column: Images-->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class='bgItem'>
                    <tr>
                        <td align="center" class='bgItem'>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">

                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="15">
                                    </td>
                                </tr>
                                <!-- .Spacing-->

                                <!-- Images section-->
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>

                                                <td width="30" class="specbundle3">&nbsp;</td>
                                                <td valign="top" class="specbundle3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td width="180" align="center">
                                                                <!--image-->
                                                                <div class="contentEditableContainer contentImageEditable">
                                                                    <div class="contentEditable">
                                                                        <label style="font-size: 80px; font-weight: bold; color: #ffffff">{{round($data['calif'],1)}}%</label>
                                                                    </div>
                                                                </div>
                                                                <!--.image-->
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="180" align="center">
                                                                <div class="contentEditableContainer contentTextEditable">
                                                                    <div class="contentEditable">
                                                                        <h2 style="padding-bottom:7px; color:#ffffff;">({{$data['sum_final']}} de {{$data['sum_total']}})</h2>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30" class="spechide"></td>
                                                        </tr>

                                                        </tbody>
                                                    </table></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- .Images section-->


                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <!--END: Benefits: 3column: Images-->

            <!--Benefits: 3column: Text-->

            <!--Section Header Section-->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                    <tr>
                        <td colspan="3" height="15">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:center;">
                                        Resultados Generales</h2>

                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="20">
                        </td>
                    </tr>
                    <tr>


                        <td valign="top" class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:center;">
                                        Categoría</h2>

                                </div>
                            </div>
                        </td>
                        <td valign="top" class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:center;">
                                        Posible</h2>

                                </div>
                            </div>
                        </td>
                        <td valign="top" class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:center;">
                                        Resultado</h2>

                                </div>
                            </div>
                        </td>
                    </tr>
                    @foreach($data['cats'] as $cat )
                        <tr>
                            <td style="color: #0b3e6f; text-align:center;">
                                {{$cat['nombre']}}
                            </td>
                            <td style="color: #535353; text-align:center;">{{$cat['sum_tot']}}</td>
                            <td style="color: #535353; text-align:center;">{{$cat['sum_real']}}</td>

                        </tr>


                    @endforeach
                    <tr>
                        <td colspan="3" height="20">
                        </td>
                    </tr>
                </table>
            </div>
            <!-- END: Featured Product -->

            <!-- Gallery -->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class='bgItem'>
                    <tr>
                        <td align="center" class='bgItem'>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="45">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                                <tr>
                                    <td width="180">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                    <td wdith="30">
                                    </td>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <h1 style="color:#ffffff;">Fotos de la Visita</h1>
                                            </div></div>
                                    </td>
                                    <td width="30">
                                    </td>
                                    <td width="180" height="30">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                </tr>
                                <!-- .Section Title: Benefits-->

                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="15">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            @foreach($data['fotos'] as $foto )


                <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                    <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                        <tr>
                            <td colspan="3" height="20">
                                Foto de Visita
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" width="320" class="specbundle3">
                                <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable">
                                        <h2 style="text-align:left;"> {{$foto->Descripcion}}</h2>
                                    </div>
                                </div>
                            </td>
                            <td width="30" class="specbundle3">
                            </td>
                            <td valign="top" width="250" class="specbundle3" align="center">
                                <div class="contentEditableContainer contentImageEditable">
                                    <div class="contentEditable">
                                        <img data-default="placeholder" data-max-width="250" src="http://ventumsupervision.com/uploads/FotoVisita/{{$foto->Foto}}" width="250"/>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" height="20">
                            </td>
                        </tr>
                    </table>
                </div>

            @endforeach

    @foreach($data['cats'] as $cat )

        <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                            <!-- Spacing-->
                            <tr>
                                <td colspan="5" height="20">
                                </td>
                            </tr>
                            <!-- .Spacing-->
                            <tr>
                                <td width="180">
                                    <!--h1Line-->
                                    <div class="movableContent">
                                        <div class="contentEditableContainer contentImageEditable">
                                            <div class="contentEditable">
                                                <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/line.png" width="180" height="30"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!--.h1Line-->
                                </td>
                                <td wdith="30">
                                </td>
                                <td width="180" align="center">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable">
                                            <h1>{{$cat['nombre']}}</h1>
                                        </div></div>
                                </td>
                                <td width="30">
                                </td>
                                <td width="180" height="30">
                                    <!--h1Line-->
                                    <div class="movableContent">
                                        <div class="contentEditableContainer contentImageEditable">
                                            <div class="contentEditable">
                                                <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/line.png" width="180" height="30"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!--.h1Line-->
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" height="20">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        @foreach($cat['todo'] as $todo )


            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                    <tr>
                        <td colspan="3" height="20">
                            To-Do Generado
                        </td>
                    </tr>

                    <tr>
                        <td valign="top" width="320" class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:left;"> {{$todo['nombre']}}</h2>
                                    <p style="text-align:left;">{{$todo['descripcion']}}</p>
                                    <ul>
                                        <li>Familia: {{$todo['familia']}}</li>

                                    </ul>
                                </div>
                            </div>
                        </td>
                        <td width="30" class="specbundle3">
                        </td>
                        <td valign="top" width="250" class="specbundle3" align="center">
                            <div class="contentEditableContainer contentImageEditable">
                                <div class="contentEditable">
                                    <img data-default="placeholder" data-max-width="250" src="http://ventumsupervision.com/uploads/FotoTodo/{{$todo['ImagenIni']}}" width="250"/>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3" height="20">
                        </td>
                    </tr>
                </table>
            </div>

        @endforeach
                @foreach($cat['campos'] as $campo )


                    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                        <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                            <tr>
                                <td colspan="3" height="20">
                                    Campo que no genera To-Do
                                </td>
                            </tr>

                            <tr>
                                <td valign="top" width="320" class="specbundle3">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable">
                                            <h2 style="text-align:left;"> {{$campo['nombre']}}</h2>
                                            <p style="text-align:left;">{{$campo['descripcion']}}</p>
                                            <ul>
                                                <li>Familia: {{$campo['familia']}}</li>

                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td width="30" class="specbundle3">
                                </td>
                                <td valign="top" width="250" class="specbundle3" align="center">
                                    <div class="contentEditableContainer contentImageEditable">
                                        <div class="contentEditable">
                                            <img data-default="placeholder" data-max-width="250" src="http://ventumsupervision.com/uploads/FotoTodo/{{$campo['ImagenIni']}}" width="250"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" height="20">
                                </td>
                            </tr>
                        </table>
                    </div>

                @endforeach
                @foreach($cat['no_aplica'] as $na )


                    <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                        <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                            <tr>
                                <td colspan="3" height="20">
                                    Campos que no aplican
                                </td>
                            </tr>

                            <tr>
                                <td valign="top" width="320" class="specbundle3">
                                    <div class="contentEditableContainer contentTextEditable">
                                        <div class="contentEditable">
                                            <h2 style="text-align:left;"> {{$na['nombre']}}</h2>
                                            <p style="text-align:left;">{{$na['descripcion']}}</p>
                                            <ul>
                                                <li>Familia: {{$na['familia']}}</li>

                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td width="30" class="specbundle3">
                                </td>
                                <td valign="top" width="250" class="specbundle3" align="center">
                                    <div class="contentEditableContainer contentImageEditable">
                                        <div class="contentEditable">
                                            <img data-default="placeholder" data-max-width="250" src="http://ventumsupervision.com/uploads/FotoTodo/{{$na['ImagenIni']}}" width="250"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" height="20">
                                </td>
                            </tr>
                        </table>
                    </div>

                @endforeach


    @endforeach

        <!-- Gallery -->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class='bgItem'>
                    <tr>
                        <td align="center" class='bgItem'>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="45">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                                <tr>
                                    <td width="180">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                    <td wdith="30">
                                    </td>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <h1 style="color:#ffffff;">Cuestionario de la Visita</h1>
                                            </div></div>
                                    </td>
                                    <td width="30">
                                    </td>
                                    <td width="180" height="30">
                                        <!--h1Line-->
                                        <div class="movableContent">
                                            <div class="contentEditableContainer contentImageEditable">
                                                <div class="contentEditable">
                                                    <img class="banner" data-default="placeholder" data-max-width="180" src="http://ventumsupervision.com/images/whiteline.png" width="180" height="30"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--.h1Line-->
                                    </td>
                                </tr>
                                <!-- .Section Title: Benefits-->

                                <!-- Spacing-->
                                <tr>
                                    <td colspan="5" height="15">
                                    </td>
                                </tr>
                                <!-- .Spacing-->
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            @foreach($data['cuestionario'] as $cuest )


                <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                    <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                        <tr>
                            <td colspan="3" height="20">
                                Pregunta
                            </td>
                        </tr>

                        <tr>
                            <td valign="top" width="320" class="specbundle3">
                                <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable">
                                        <h2 style="text-align:left;"> {{$cuest->Pregunta}}</h2>
                                    </div>
                                </div>
                            </td>
                            <td width="30" class="specbundle3">
                            </td>
                            <td valign="top" width="250" class="specbundle3" align="center">
                                <div class="contentEditableContainer contentTextEditable">
                                    <div class="contentEditable">
                                        @if($cuest->tipo == 1)

                                            <h2 style="text-align:left;"> R: {{$cuest->respuesta}}</h2>
                                        @else

                                            <h2 style="text-align:left;"> R: {{$cuest->Abierta}}</h2>
                                        @endif

                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3" height="20">
                            </td>
                        </tr>
                    </table>
                </div>

            @endforeach


            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" width="600" valign="middle" cellpadding="0" style='margin:0;' cellspacing="0" border="0" class="MainContainer">
                    <tr>
                        <td colspan="3" height="20">
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" width="250" class="specbundle3" align="center">
                            <div class="contentEditableContainer contentGmapEditable">
                                <div class="contentEditable">
                                    <img  style="border: 1px solid #0b58a2;border-radius: 10px;" src="http://maps.google.com/maps/api/staticmap?center={{$data['visita']->lat}},{{$data['visita']->lon}}&zoom=19&size=150x150&markers=color:blue"  />
                                </div>
                            </div>
                        </td>
                        <td width="30" class="specbundle3">
                        </td>
                        <td valign="top" width="320" class="specbundle3">
                            <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable">
                                    <h2 style="text-align:left;">Ubicación aproximada de la realización de la visita</h2>
                                    <p style="text-align:left;">Descarga el reporte en la liga que está debajo
                                    <p style="text-align:left;">
                                        <a target='_blank' href="http://ventumsupervision.com/uploads/Reportes//{{$data['visita']->Archivo}}">Reporte</a>
                                    </p>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3" height="20">
                        </td>
                    </tr>
                </table>
            </div>
            <!-- END: Contact Us: Address -->

            <!--Footer: Social Media-->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#013275;">
                    <tbody>
                    <tr>
                        <td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                                <tr>
                                    <td height="40">
                                    </td>
                                </tr>
                                <tr>
                                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td valign="top" class="specbundle3" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td width="20"class="spechide">&nbsp;</td>
                                                            <td align="center" valign="middle" width="20">
                                                                <!--Twitter-->
                                                                <div class="contentEditableContainer contentTwitterEditable" style="min-height: 0px;">
                                                                    <div class="contentEditable">
                                                                        <img src="http://ventumsupervision.com/images/whiteTweet.png" title="Síguenos en Twitter" width="20" data-customIcon="true" data-max-width='20'>
                                                                    </div>
                                                                </div>
                                                                <!--.Twitter-->
                                                            </td>
                                                            <td width="57">&nbsp;</td>
                                                            <td align="center" width="20">
                                                                <!--Facebook-->
                                                                <div class="contentEditableContainer contentFacebookEditable" style="min-height: 0px;">
                                                                    <div class="contentEditable">
                                                                        <img src="http://ventumsupervision.com/images/whitebook.png" title="Búscanos en Facebook" width="20" data-customIcon="true" data-max-width='20'>
                                                                    </div>
                                                                </div>
                                                                <!--.Facebook-->
                                                            </td>
                                                            <td width="57">&nbsp;</td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="57" class="specbundle3">&nbsp;</td>
                                                <td valign="top" class="specbundle3" align="center"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>


                                                            <td width="58">&nbsp;</td>
                                                            <td align="center" width="20">
                                                                <!--YouTube-->
                                                                <div class="contentEditableContainer contentImageEditable" style="min-height: 0px;">
                                                                    <div class="contentEditable">
                                                                        <img src="http://ventumsupervision.com/images/whiteTube.png" title="Ve nuestros videos" width="20" >
                                                                    </div>
                                                                </div>
                                                                <!--.YouTube-->
                                                            </td>
                                                            <td width="20"class="spechide">&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="10">
                                    </td>
                                </tr>
                            </table></td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <!-- END: Footer: Social Media-->

            <!-- Footer: Email links-->
            <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:#013275;">
                    <tr>
                        <td height="10">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#013275;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" class="MainContainer">
                                <tr>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <p>
                                                    <a target='_blank' href="[UNSUBSCRIBE]" style="font-size: 12px; text-align:center; color:#ffffff">
                                                        </a>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="30"class="spechide"></td>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <p>
                                                    <a target='_blank' href="[SHOWEMAIL]" style="font-size: 12px; text-align:center; color:#ffffff">
                                                        Ventum Supervisión 2018 - Todos los Derechos Reservados/a>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="30"class="spechide"></td>
                                    <td width="180" align="center">
                                        <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable">
                                                <p>
                                                    <a target='_blank' href="[FORWARD]/contact" style="font-size: 12px; text-align:center; color:#ffffff">
                                                        </a>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20">
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>




</body>
</html>