@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>
        Resultados de la visita
    </h1>

@stop

@section('content')
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h1 style="font-size: 50px;" class="text-center">{{round($calif,1)}}%</h1>

                    <h3 class="profile-username text-center">{{$sum_final}} de {{$sum_total}}</h3>

                    <p class="text-muted text-center">Calificación Final</p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Fecha</b> <a class="pull-right">{{$visita->HoraFin}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Duracion</b> <a class="pull-right">{{$visita->Duracion}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Checklist</b> <a class="pull-right">{{$visita->nombre}}</a>
                        </li>
                    </ul>

                    <a type="button"  href="{{route('reporteVisita', $visita_id)}}"  class="btn btn-primary btn-block" style="margin-right: 5px;">
                        <i class="fa fa-download pull-left"></i> Generar PDF
                    </a>
                        <button type="button" class="btn btn-danger btn-block" onclick="cancelarVisita({{$visita_id}})"  style="margin-right: 5px;">
                        <i class="fa fa-remove pull-left"></i> Cancelar
                    </button>
                    <button type="button" class="btn btn-success btn-block" onclick="gotoTiendas()" style="margin-right: 5px;">
                        <i class="fa  fa-check pull-left"></i> Finalizar
                    </button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información General</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-shopping-cart margin-r-5"></i> Sucursal</strong>

                    <p class="text-muted">
                        {{$visita->tienda}}
                    </p>

                    <hr>

                    <strong><i class="fa fa-user margin-r-5"></i> Supervisor</strong>

                    <p class="text-muted"> {{$visita->name}}</p>

                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>

                    <div class="map">
                        <iframe src="https://maps.google.com/maps?q={{$visita->lat}},{{$visita->lon}}&hl=en&z=18&amp;output=embed" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                    <hr>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="box box-danger box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">To-Do's Generados</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="timeline-item">

                        @foreach($cats as $cat )
                            <span style="color: #475f69" class="pull-right">Posible: {{$cat['sum_tot']}} -  Resultado: {{$cat['sum_real']}}</span>
                            <h3 class="timeline-header"><a href="#">{{$cat['nombre']}}</a></h3>

                            <div class="timeline-body">
                                @foreach($cat['todo'] as $todo )
                                    <div class="attachment-block clearfix">
                                        <img class="attachment-img" src="{{ asset('uploads/FotoTodo/')}}/{{$todo['ImagenIni']}}" alt="Attachment Image">

                                        <div class="attachment-pushed">
                                            <h4 class="attachment-heading"><a href="http://www.lipsum.com/">{{$todo['nombre']}}</a> </h4>

                                            <div class="attachment-text">
                                                <span style="color: #797998" ><i class="fa  fa-check-square-o"></i> Familia: {{$todo['familia']}}</span>
                                                </br>
                                                {{$todo['descripcion']}}
                                                </br>

                                            </div>
                                            <!-- /.attachment-text -->
                                        </div>
                                        <!-- /.attachment-pushed -->
                                    </div>
                                @endforeach
                            </div>
                            <div class="timeline-footer">

                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-9">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Fotos de la Visita</h3>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="timeline-item">
                        <div class="timeline-body">
                            @foreach($fotos as $foto)
                                <img src="{{ asset('uploads/FotoVisita/')}}/{{$foto->Foto}}" width="150px" height="100px" alt="..." class="margin">
                            @endforeach

                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>


@endsection

@section('css')

    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

@stop

@section('js')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    @routes
    <script >
        function gotoTiendas()
        {
            url = route('tiendas.tiendas');
            window.location.href = url;
        }

        function setSrcImage(foto)
        {
            alert(foto);
        }

        function cancelarVisita(id)
        {
            url = '{{route('tiendas.tiendas')}}'
            bootbox.confirm({
                title: "Cancelar Visita",
                message: "¿Deseas cancelar visita? no podrás retomarla más adelante.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Salir'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Cancelar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    {
                        //   window.location.href = url;
                        var post_id = id;
                        var time = "";

                        var url_can = '{{route('cancelaVisita')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_can,
                            data: {id:post_id, time: time}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                window.location.href = url;
                            });
                    }

                }
            });
        }

    </script>
@stop