
@extends('adminlte::page')
@section('title', 'Ventum 2.0')

@section('content_header')
    <h1>Visita de Supervisión de la Sucursal {{$info->nombre}}  </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Tiendas</li>
        <li class="active">Iniciar Visita</li>
    </ol>
@stop

@section('content')


<div class="row">
    <div class="col-md-3" id="panelVisita">
        <!-- <a href="compose.html" class="btn btn-primary btn-block margin-bottom">Compose</a>-->
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title" name="Ancla">Inicia Visita</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <input type="hidden" id="id_cuestionario" value="{{$cuestionario->Id}}" >
                <strong><i class="fa fa-book margin-r-5"></i> Checklist</strong>

                <select class="form-control select2" id="chk" name="checklist" style="width: 100%;">
                    <option value="0" selected="selected">--Selecciona--</option>
                    @foreach($checklist as $chk)
                        <option value="{{$chk->Id}}"> {{$chk->nombre}}</option>

                    @endforeach
                </select>

               <hr>
                <div id="valUbicacion">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                    <div id="googleMap" style="width:100%;height:150px;"></div>
                    <hr>
                </div>

                <div  id="valEscaneo" >
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Escaneo</strong>
                    <div class="input-group">
                        <input id="scanner_input" class="form-control" disabled placeholder="Presiona aquí para escanear código" type="text" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#livestream_scanner">
                                <i class="fa fa-barcode"></i>
                            </button>
                        </span>
                    </div><!-- /input-group -->
                    <hr>
                </div>

                <div id="valPass">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Contraseña</strong>
                    <div class="input-group">
                        <input id="pass" type="password" class="form-control" placeholder="Introduce contraseña de tienda"    />
                        <span class="input-group-btn">
                            <button  id="passBtn" class="btn btn-default" onclick="valPass()" disabled >
                                <i class="fa fa-check-square"></i>
                            </button>
                        </span>
                    </div><!-- /input-group -->
                    <button id="passRec"  type="button" class="btn btn-block btn-default" data-toggle="modal" data-target="#modal-default" disabled>
                        Recuperar contraseña
                    </button>
                    <hr>
                </div>
                <strong><i class="fa fa-pencil margin-r-5"></i> Tiempo:</strong>

                <p>
                    <input type="text" id="t" value="00:00" class="btn bg-orange btn-block margin-bottom" disabled />

                </p>

                <hr>

                <p>
                    <button  id="btn" class="btn btn-primary btn-block margin-bottom">Iniciar</button>


                    <a   onclick="cancelar()" id="btnCan" class="btn btn-danger  btn-block margin-bottom">Cancelar</a>

                </p>

                <hr>

                <div id="fotos"  class="panel panel-primary">
                    <div class="panel-heading" name="1">
                        Fotos de Visita
                    </div>
                    <div class="panel-body" name="2">
                        <form action="{{route('uploadImgVisita')}}" method="POST" files="true" id="myDropzone" class="dropzone" >

                            <div class="dz-message" style="height:200px;" name="3">
                                Click o arrastrar para agregar imagenes
                            </div>
                            <div class="dropzone-previews"></div>


                        </form>
                    </div>

                </div>
                <div class="editFotosVisita" >

                </div>

                <!-- fotos LIST -->
                <div id="fotolist" class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edición Fotos de Visita</h3>

                        <div class="box-tools pull-right">

                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <ul class="products-list product-list-in-box">
                            <!-- /.item -->
                            <div class="foto-list" >
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

                            </div>

                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <a  onclick="endImgVisita()"   class="btn btn-success btn-block pull-right">Termina Edición</a>
                    </div>
                    <!-- /.box-footer -->
                </div>


            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

    </div>



    <div class="col-md-9" >
        <!-- dashboard -->
            <div class="box" id="infoVis">
                <div class="box-header with-border">
                    <h3 class="box-title">Información Previa a la Visita</h3>

                    <div class="box-tools pull-right">

                    </div>
                </div>


                <div class="box-body">
                    <div class="carousel-widget">

                    </div>

                    <div class="pull-left">
                        <label>Últimas Visitas Realizadas</label>
                    </div>
                </div>
                <!-- ./box-body -->

                <div class="box-footer">

                    <div class="row">

                        @foreach($last_visitas as $vis)
                           @php
                            $color = "";
                            if($vis->Calif >= 95 && $vis->Calif <= 100)
                            {
                                $color = 'text-green';
                            }else if($vis->Calif >= 90 && $vis->Calif <= 94.99)
                            {
                                $color = 'text-yellow';
                            }else if($vis->Calif >= 0 && $vis->Calif <= 89.99)
                            {
                                $color = 'text-red';
                            }
                            @endphp

                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                                    <span class="description-percentage {{$color}}"><i></i> <h3>{{$vis->Calif}}%</h3></span>
                                    <h5 class="description-header">{{date('d/m/Y', strtotime($vis->HoraInicio) )}}</h5>
                                    <span class="description-text">{{$vis->name}}</span>
                                </div>
                                <!-- /.description-block -->
                            </div>

                        @endforeach
                            <div class="row">
                                <div id="button-car"  class="pull-right" style="padding-right: 40px; padding-top: 20px" >
                                    <a  href="#carousel-widgets" data-slide="prev">
                                        <button class="btn btn-primary block"> <</button>
                                    </a>
                                    <a class="pause-car"  >
                                        <button onclick="stopCarousel()" class="btn btn-primary fa fa-pause"> </button>
                                    </a>
                                    <a   href="#carousel-widgets" data-slide="next">
                                        <button class="btn btn-primary  block"> ></button>
                                    </a >
                                </div>
                            </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>


            <!-- /.box -->
        <!-- seccion del cuestionarrio-->
        <div id="cuestionario" class="cargaCuestionario">
            <!-- aqui se carga el cuestionario de la visita -->
        </div>

        <!-- seccion de avance-->
        <div id="avance" class="cargaAvance">
            <!-- aqui se carga el avance de la visita -->
        </div>


        <!-- FINAL DEL CUESTIONARIO -->

        <div class="id_visita" >
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

        </div>
        <div class="id_smart" >
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

        </div>

        <div class="campos_total" >
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

        </div>
        <div class="campos_visita" >
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

        </div>
        <div id="btnFin" >

        </div>

        <div id="carousel" class="carousel slide " data-ride="carousel" data-interval="false" >
            <div class="ajax-content" >
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

            </div>

        </div>
        <button id="pager2"  onclick="showDespues()"  class="btn btn-success btn-block pull-right">Continuar</button>
       <ul class="pager" id="pager" >

           <li class="previous"><a href="#carousel" data-slide="prev" onclick="setPos()" >Anterior</a></li>

            <li class="next"><a  href="#carousel" data-slide="next" onclick="setPos()" >Siguiente</a></li>
       </ul>

        <!-- PRODUCT LIST -->
        <div id="todolist" class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Listado de To-Do's Generados</h3>

                <div class="box-tools pull-right">

                </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    <!-- /.item -->
                    <div class="todo-list" >
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->

                    </div>

                </ul>

            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a   onclick="finalizarVisita()"  class="btn btn-success btn-block pull-right">Terminar Visita</a>
            </div>
            <!-- /.box-footer -->
        </div>
        <div id="editTodo">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user-2 infoTodo">

            </div>
            <!-- /.widget-user -->
        </div>


    </div>

</div>
<!-- /.row -->

    <!--modals-->
<div class="modal modal-info fade" id="modal-info">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cargando Imagen</h4>
            </div>
            <div class="modal-body">
                <div class="progress progress-xs">
                    <div class="progress-bar progress-bar-green" style="width: 100%"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">

                    </div>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal" id="livestream_scanner">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Escanea Código</h4>
            </div>
            <!-- <div class="modal-body" style="position: static">
                <div id="interactive" class="viewport"></div>
                <div class="error"></div>
            </div>-->
            <div class="modal-footer">
                <label class="btn btn-default pull-left">
                    <i class="fa fa-camera"></i> Tomar captura del código
                    <input type="file" accept="image/*;capture=camera" capture="camera" class="hidden" />
                </label>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="planSmart" id="planSmart" >


</div>


<!--modal recuperacion contraseña-->
<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recuperar contraseña de tienda</h4>
            </div>
            <div class="modal-body">
                <p>Usuario que desea cambiar la contraseña: <strong>{{auth()->user()->name}}</strong></p>
                <strong><i class="fa fa-book margin-r-5"></i> Motivo del cambio</strong>

                <select class="form-control select2" id="motivo" name="motivo" style="width: 100%;">
                    <option value="0" selected="selected">--Selecciona--</option>
                    <option value="1" >Olvidé la contraseña</option>
                    <option value="2" >No tengo acceso a la contraseña</option>
                    <option value="3" >Soy el gerente de tienda y deseo cambiarla</option>

                </select>
                <hr>
                <strong><i class="fa fa-book margin-r-5"></i>Nueva contraseña</strong>
                <input type="text"  maxlength="10" class="form-control" id="inputPass" placeholder="Escribe la nueva contraseña para tienda">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
                <a onclick="resetPass()" type="button" class="btn btn-primary">Recuperar</a>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


@endsection

@section('css')
    <link href="{{ asset('dist/dropzone.css') }}" rel="stylesheet">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <style>
        .badge-important {

        }
        .image-upload > input
        {
            display: none;
        }

    </style>

    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

    </style>

    <style>
        #interactive.viewport {position: relative; width: 100%; height: auto; overflow: hidden; text-align: center;}
        #interactive.viewport > canvas, #interactive.viewport > video {max-width: 100%;width: 100%;}
        canvas.drawing, canvas.drawingBuffer {position: absolute; left: 0; top: 0;}
    </style>

@stop


@section('js')



    <script src="{{ asset('dist/dropzone.js') }}" ></script>

    <script>
        Dropzone.options.myDropzone = {
            paramName: 'file',
            autoProcessQueue: true,
            uploadMultiple: true,
            maxFilesize: 50, // MB,
            parallelUploads: 1,
            timeout: 180000,
            maxFiles: 100,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            addRemoveLinks: true,
            dictRemoveFile: "Eliminar",

            maxfilesexceeded: function (file, response) {

                this.removeAllFiles();
                this.addFile(file);

                console.log(file['name'])
            },

            init: function() {
                this.on("success", function(file, response) {
                    var url_foto_vis = '{{route('setFotoVis')}}';

                    console.log(response)
                    for(var j = 0; j < response.length; j++)
                    {
                        $.ajax({
                            url: url_foto_vis,
                            method: 'POST',
                            data: { visita_Id: document.getElementById('id_visita').value, Nombre: response[j]},
                        }).done(function (msg) {

                            console.log(msg['message']);

                        });
                    }


                });
                this.on("error", function(file, response) {


                });
            }
        };
    </script>


    <script src="{{ asset('dist/timer.jquery.min.js')}}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('bower_components/chart.js/dist/Chart.js')}}"></script>
    <!-- ChartJS -->
    <script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.js')}}"></script>
    <script src="{{ asset('bower_components/chart.js/samples/utils.js')}}"></script>
    <script src="https://cdn.rawgit.com/serratus/quaggaJS/0420d5e0/dist/quagga.min.js"></script>
    <script src="{{ asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>


    <script type="text/javascript">
        function resetPass()
        {
            if(document.getElementsByName('checklist')[0].value != 0)
            {
                var pass = document.getElementById('inputPass').value;
                var url = '{{route('resetPassTienda')}}';
                var tiendas_Id = '{{$info->Id}}';
                var motivo =  document.getElementById('motivo').value;

                if(pass != "")
                {
                    if(motivo != 0)
                    {
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {pass:pass, tiendas_Id :tiendas_Id, motivo: motivo, tipo:1}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                $('#modal-default').modal('hide');
                                bootbox.alert('¡Has cambiado la contraseña!, Valídala antes de iniciar la visita');
                                document.getElementById('inputPass').value = "";

                            });
                    }else
                    {
                        bootbox.alert("Debes seleccionar el motivo del cambio de contraseña");
                    }
                }else
                {
                    bootbox.alert("Debes escribir una contraseña nueva para continuar");
                }

            }else
            {
                bootbox.alert("Debes elegir un checklist para continuar");
                $('#modal-default').modal('hide');
            }

        }


        $(function() {
            // Create the QuaggaJS config object for the live stream
            var liveStreamConfig = {
                inputStream: {
                    type : "LiveStream",
                    constraints: {
                        width: {min: 640},
                        height: {min: 480},
                        aspectRatio: {min: 1, max: 100},
                        facingMode: "environment" // or "user" for the front camera
                    }
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: (navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 4),
                decoder: {
                    "readers":[
                        {"format":"ean_reader","config":{}}
                    ]
                },
                locate: true
            };
            // The fallback to the file API requires a different inputStream option.
            // The rest is the same
            var fileConfig = $.extend(
                {},
                liveStreamConfig,
                {
                    inputStream: {
                        size: 800
                    }
                }
            );
            // Start the live stream scanner when the modal opens
            $('#livestream_scanner').on('shown.bs.modal', function (e) {
                Quagga.init(
                    liveStreamConfig,
                    function(err) {
                        if (err) {
                            $('#livestream_scanner .modal-body .error').html('<div class="alert alert-danger"><strong><i class="fa fa-exclamation-triangle"></i> '+err.name+'</strong>: '+err.message+'</div>');
                            Quagga.stop();
                            return;
                        }
                        Quagga.start();
                    }
                );
            });

            // Make sure, QuaggaJS draws frames an lines around possible
            // barcodes on the live stream
            Quagga.onProcessed(function(result) {
                var drawingCtx = Quagga.canvas.ctx.overlay,
                    drawingCanvas = Quagga.canvas.dom.overlay;

                if (result) {
                    if (result.boxes) {
                        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                        result.boxes.filter(function (box) {
                            return box !== result.box;
                        }).forEach(function (box) {
                            Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                        });
                    }

                    if (result.box) {
                        Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
                    }

                    if (result.codeResult && result.codeResult.code) {
                        Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
                    }
                }
            });

            // Once a barcode had been read successfully, stop quagga and
            // close the modal after a second to let the user notice where
            // the barcode had actually been found.
            Quagga.onDetected(function(result) {
                if (result.codeResult.code){

                   //buscamos si existe el código de barras

                    var code = result.codeResult.code;
                    var url = '{{route('valCodeTienda')}}';
                    var tiendas_Id = '{{$info->Id}}';

                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {code:code, tiendas_Id :tiendas_Id}
                    })
                        .done(function(msg){
                            console.log(msg['message']);

                            if(msg['message'] == 0)
                            {
                                bootbox.alert('El código escaneado no coincide con el de la sucursal, favor de volver a intentarlo');

                            }else
                            {
                                bootbox.alert('¡Código correcto!, Ahora inicia la visita');
                                $("#btn").removeAttr('disabled');
                                $('#scanner_input').val(result.codeResult.code);
                                Quagga.stop();
                                setTimeout(function(){ $('#livestream_scanner').modal('hide'); }, 1000);

                            }

                        });


                }else
                {
                    bootbox.alert('Código no encontrado')
                }
            });

            // Stop quagga in any case, when the modal is closed
            $('#livestream_scanner').on('hide.bs.modal', function(){
                if (Quagga){
                    Quagga.stop();
                }
            });




            // Call Quagga.decodeSingle() for every file selected in the
            // file input
            $("#livestream_scanner input:file").on("change", function(e) {
                if (e.target.files && e.target.files.length) {
                    Quagga.decodeSingle($.extend({}, fileConfig, {src: URL.createObjectURL(e.target.files[0])}),
                        function(result)
                        {
                            //alert(result.codeResult.code);

                        });
                }
            });
        });
    </script>

    <script>

        /***/

        function valPass()
        {
            if(document.getElementsByName('checklist')[0].value != 0)
            {
                var pass = document.getElementById('pass').value;
                var url = '{{route('valPassTienda')}}';
                var tiendas_Id = '{{$info->Id}}';

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {pass:pass, tiendas_Id :tiendas_Id}
                })
                    .done(function(msg){
                        console.log(msg['message']);

                        if(msg['message'] == 0)
                        {
                            bootbox.alert('La contraseña de tienda introducida es incorrecta, vuelve a intentarlo');
                            $("#pass").removeAttr('disabled');
                        }else
                        {
                            bootbox.alert('¡Contraseña correcta!, Ahora inicia la visita');
                            $("#btn").removeAttr('disabled');
                            $("#pass").attr("disabled", "disabled");
                        }

                    });
            }else
            {
                bootbox.alert("Debes elegir un checklist antes de continuar")
            }

        }
        /***/




        function genChart()
        {

            var url = '{{route('genGraficaTodo')}}';
            var tiendas_Id = '{{$info->Id}}';



            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id :tiendas_Id}
            })
                .done(function(msg){
                    console.log(msg['meses']);
                   // var MONTHS = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
                   // var mesLabels = ["Enero 17", "Febrero 17", "Marzo 17", "Abril 17", "Mayo 17", "Junio 17", "Julio 17","Agosto 17","Septiembre 17","Octubre 17","Noviembre 17","Diciembre 17","Enero 18"];
                    var mesLabels = [];
                    var todosCre = [];
                    var todosFin = [];
                    for(var i = 0; i < msg['meses'].length; i++)
                    {
                        mesLabels.push(msg['meses'][i]);
                        todosCre.push(msg['todos'][i]);
                        todosFin.push(msg['terminados'][i]);
                    }

                    /*var todosCre = [
                        16,
                        10,
                        5,
                        5,
                        20,
                        10,
                        25,
                        10,
                        5,
                        9,
                        1

                    ];
                    var todosFin = [
                        3,
                        2,
                        5,
                        1,
                        2,
                        10,
                        9,
                        1,
                        0,
                        17,
                        2

                    ];*/
                    var config = {
                        type: 'line',
                        data: {
                            labels: mesLabels,
                            datasets: [{
                                label: "Terminados",
                                backgroundColor: window.chartColors.blue,
                                borderColor: window.chartColors.green,
                                data: todosFin,
                                fill: false,
                            }, {
                                label: "Creados",
                                fill: false,
                                backgroundColor: window.chartColors.orange,
                                borderColor: window.chartColors.red,
                                data: todosCre,
                            }]
                        },
                        options: {
                            responsive: true,

                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Mes'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'To-Do (conteo)'
                                    }
                                }]
                            }
                        }
                    };


                    var ctx = document.getElementById("canvas").getContext("2d");
                    window.myLine = new Chart(ctx, config);


                });

        }




    </script>


    @routes
    <script>


        function cancelar()
        {
            url = '{{route('tiendas.tiendas')}}'
            bootbox.confirm({
                title: "Cancelar Visita",
                message: "¿Deseas cancelar visita? no podrás retomarla más adelante.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Salir'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Cancelar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    {
                     //   window.location.href = url;
                        var post_id = document.getElementById('id_visita').value;
                        var time = "";

                        var url_can = '{{route('cancelaVisita')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_can,
                            data: {id:post_id, time: time}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                window.location.href = url;
                            });
                    }

                }
            });
        }

        function finalizarVisita()
        {
            $("#todolist").hide();
            //preguntamos antes si queremos cargar el cuestionario de la visita
            var box = bootbox.confirm({
                title: "Cuestionario de  Visita",
                message: "¿Deseas realizar el cuestionario de la visita?.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> No'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Si'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result == true)
                    {
                           // alert('realizar cuestionario')
                        box.modal('hide');
                        goCuestionario();

                    }else
                    {
                        var post_id = document.getElementById('id_visita').value;
                        var time = document.getElementById('t').value;
                        bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })
                        var url = '{{route('setTimeFinal')}}';

                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {id:post_id, time: time}
                        })
                            .done(function(msg){

                                console.log(msg['message']);
                                goFinal();
                            });
                    }

                }
            });




        }
        function goCuestionario()
        {
            var text = "";
            $("#cuestionario").show();

            var url = '{{route('getCuestionario')}}';

;

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:document.getElementById('id_cuestionario').value}
            })
                .done(function(msg){

                    console.log(msg['message']);
                    text += ' <div class="box box-default" >\n' +
                        '            <div class="box-header with-border">\n' +
                        '                <h3 class="box-title">Cuestionario de la Visita</h3>\n' +
                        '            </div>\n' +
                        '\n' +
                        '            <div class="box-body">\n' +
                        '                <div class="row">\n' +
                        '                   <div class="col-md-6">\n';


                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        text +=  '                        <div class="form-group">\n' +
                            '                            <label >'+msg['message'][i]['pregunta']+'</label>\n';
                        if(msg['message'][i]['tipo'] == 0)
                        {
                            text +=   '  <input maxlength="100" type="text" id="abierta'+msg['message'][i]['Id']+'" class="form-control" onchange="setAbierta('+msg['message'][i]['Id']+')"  placeholder="Pregunta abierta">\n' ;
                        }else {

                            for(var j = 0; j < msg['message'][i]['respuestas'].length; j++)
                            {
                                text +=   '</br> <label> <input type="radio" name="optionsRadios'+msg['message'][i]['Id']+'" onclick="setOption('+msg['message'][i]['respuestas'][j]['Id']+','+msg['message'][i]['Id']+')" id="optionsRadios2" value="'+msg['message'][i]['respuestas'][j]['Id']+'"> '+msg['message'][i]['respuestas'][j]['respuesta']+' </label>\n' ;
                            }

                        }


                        text +=      '                        </div>';
                    }



                    text += '\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <!-- /.row -->\n' +
                        '                            </div>\n' +
                        '                            <!-- /.box-body -->\n' +
                        '                            <div class="box-footer">\n' +
                        '                 <button   onclick="finalCuestionario()"  class="btn btn-success btn-block pull-right">Continuar</button>\n' +
                        '                            </div>\n' +
                        '                        </div>';

                    $('.cargaCuestionario').html( text );
                });


          //  document.getElementById("fotolist ").scrollIntoView();

        }
        function finalCuestionario()
        {
            var post_id = document.getElementById('id_visita').value;
            var time = document.getElementById('t').value;
            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })

            var url = '{{route('setTimeFinal')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:post_id, time: time}
            })
                .done(function(msg){
                    console.log(msg['message']);
                    goFinal();
                });
        }

        function goFinal()
        {
            var post_id = document.getElementById('id_visita').value;
            url = route('visitas.final',post_id);//'visitas/final/'+post_id;//
            window.location.href = url;
        }

        function setOption(respuesta, pregunta)
        {
            //alert(respuesta+'---'+ pregunta)
            var url = '{{route('setCuestionario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {pregunta:pregunta, tipo:1, respuesta:respuesta, visitas_Id:document.getElementById('id_visita').value}
            })
                .done(function(msg){
                    console.log(msg['message'])

                });
        }

        function setAbierta(id)
        {
            var abiertaId = "abierta"+id;

            //alert(document.getElementById(abiertaId).value);
            var url = '{{route('setCuestionario')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {pregunta:id, tipo:0, respuesta:document.getElementById(abiertaId).value, visitas_Id: document.getElementById('id_visita').value}
            })
                .done(function(msg){
                    console.log(msg['message'])
                });
        }



       function setPos()
       {
           //alert('Campos total '+ document.getElementById('campos_tot').value + ' campos visita' + document.getElementById('campos_vis').value);
           if(document.getElementById('campos_tot').value == document.getElementById('campos_vis').value)
           {

               document.getElementById("fotos").scrollIntoView();
           }else
           {
               document.getElementById("avance").scrollIntoView();
           }
           //$("#boton_termina").hide;

       }
        (function(){

            $("#btn").click(function(){
                switch($(this).html().toLowerCase())
                {

                    case "iniciar":

                        iniciarVisita();

                        break;

                    case "resume":
                        $("#t").timer('resume');
                        $(this).html("Pause")
                        $("#t").addClass("badge-important");
                        break;

                    case "pause":
                        //you can specify action via object
                        $("#t").timer({action: 'pause'});
                        $(this).html("Resume")
                        $("#t").removeClass("badge-important");
                        break;

                }
            });

            $("#btnCan").click(function(){
               // $("#t").timer('reset');
               // $("#t").timer('pause');
            });

        })();

        //pause an existing timer
        $("#div-id").timer('pause');

        //resume a paused timer
        $("#div-id").timer('resume');

        //remove an existing timer
        $("#div-id").timer('remove');  //leaves the display intact

        //get elapsed time in seconds
        $("#div-id").data('seconds');
    </script>

    <script>
        var x = document.getElementById("demo");

    </script>
    <script>
        $(function () {


        });

        function editImgVisita(chk)
        {
            $("#fotolist").show();
            $("#fotos").hide();
            document.getElementById("fotolist").scrollIntoView();
            url = '{{route('visitas.fotos')}}';
            // alert('Muestra fotos')

            $.ajax({
                method: 'POST',
                url: url,
                data: { visita_Id: document.getElementById('id_visita').value}
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/uploads/FotoVisita/';
                    text += '<table border="0">';
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        text += '<tr>' +
                                    '<td width="15%" >' +
                                    '       <img height="80px" width="80px" style="padding-top: 5px; padding-bottom: 5px; padding-right: 5px" src="'+pathImg+msg['message'][i]['Foto']+'"  alt="Visita Image">\n' +
                                    '</td>'+
                                    '<td>' +
                                    '       <input onclick="clearInput('+msg['message'][i]['Id']+')" onchange="setDescripcionFotoVis('+msg['message'][i]['Id']+')"  class="form-control"  id="com'+msg['message'][i]['Id']+'" type="text" value="'+msg['message'][i]['Descripcion']+'" /> \n' +
                                    '</td>'+
                                '</tr>';

                    }
                    text += '</table>';

                    $('.foto-list').html( text );

                });
        }

        function setFotoCampo(cam, vis)
        {
                var color = "cam"+cam;
                var name_color = document.getElementById(color).style.border;
                var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;

            if(name_color == '2px solid rgb(204, 204, 204)')
            {
                bootbox.alert("Elige una opción antes de subir una foto");
            }else
            {
                $avatarImage = $('#avatarImage'+cam);
                $avatarInput = $('#avatarInput'+cam);
                $avatarForm = $('#avatarForm'+cam);
                $rutaFoto = $('.rutaFoto'+cam);
                $avatarInput.click();
                $avatarInput.on('change', function () {
                    var formData = new FormData();
                    formData.append('photo', $avatarInput[0].files[0]);

                    $('#modal-info').modal('toggle');

                    $.ajax({
                        url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                        method: $avatarForm.attr('method'),
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (msg) {
                        // alert(cam);
                        /*if (data.success)
                            $avatarImage.attr('src', data.path);*/
                        var URLdomain = window.location.host;
                        var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                        $avatarImage.attr('src', pathImg)
                        console.log(msg['message']);
                        $('#modal-info').modal('toggle');
                        //console.log(msg['path']);
                        //console.log(pathImg);
                        var url = '{{route('putFotoCampo')}}'
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {campo_Id:cam, visita_Id: vis, foto: msg['message']}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                            });

                    });
                });
            }



        }


        function setFotoCampoTodo(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


                $avatarImage = $('#avatarImageT'+cam);
                $avatarInput = $('#avatarInputT'+cam);
                $avatarForm = $('#avatarFormT'+cam);
                $rutaFoto = $('.rutaFotoT'+cam);
                $avatarInput.click();
                $avatarInput.on('change', function () {
                    var formData = new FormData();
                    formData.append('photo', $avatarInput[0].files[0]);

                    $('#modal-info').modal('toggle');

                    $.ajax({
                        url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                        method: $avatarForm.attr('method'),
                        data: formData,
                        processData: false,
                        contentType: false
                    }).done(function (msg) {
                        // alert(cam);
                        /*if (data.success)
                            $avatarImage.attr('src', data.path);*/
                        var URLdomain = window.location.host;
                        var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                        $avatarImage.attr('src', pathImg)
                        console.log(msg['message']);
                        $('#modal-info').modal('toggle');
                        //console.log(msg['path']);
                        //console.log(pathImg);
                        var url = '{{route('putFotoCampo')}}'
                        $.ajax({
                            method: 'POST',
                            url: url,
                            data: {campo_Id:cam, visita_Id: vis, foto: msg['message']}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                            });

                    });
                });

        }

        function setFotoCampoTodoCom(cam, vis)
        {

            var $avatarImage, $avatarInput, $avatarForm, $rutaFoto;


            $avatarImage = $('#avatarImageTc'+cam);
            $avatarInput = $('#avatarInputTc'+cam);
            $avatarForm = $('#avatarFormTc'+cam);
            $rutaFoto = $('.rutaFotoTc'+cam);
            var foto_com_id = "fotocomid"+cam;

            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                //$('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                   // $('#modal-info').modal('toggle');
                    console.log(msg['message']);

                    document.getElementById(foto_com_id).value = msg['message'];


                });
            });

        }




        function endImgVisita()
        {
            $("#fotolist").hide();
            $("#fotos").show();

            document.getElementById("fotos").scrollIntoView();
        }


        function reanudaVisita(id_vis)
        {
            $("#button-car").show();
            var token = '{{csrf_token()}}';
            var url   = '{{route('recuperaVis')}}';
            var user = '{{ Auth::user()->id }}';
            var selChk = document.getElementsByName('checklist')[0].value;
            var tiendas_Id = '{{$info->Id}}';
            $.ajax({
                method: 'POST',
                url: url,
                data: {body:selChk, user: user,tienda: tiendas_Id, vis:id_vis, _token: token}
            })
                .done(function(msg) {
                    console.log(msg['campos']);
                    console.log(msg['message']);
                    var text = "";
                    var carousel = "";

                    $('.editFotosVisita').html( '<a  onclick="editImgVisita('+id_vis+')" class="btn btn-primary btn-block margin-bottom">Editar</a>' );
                    carousel += '<!-- Indicators -->\n' +
                        '                         <ol class="carousel-indicators">\n' +
                        '                         <li data-target="#carousel" data-slide-to="0" class="active"></li>' ;

                    for(var i = 1; i < msg['message'].length; i++)
                    {
                        carousel += ' <li data-target="#carousel" data-slide-to="'+i+'" ></li>';
                    }
                    carousel +=  '                         </ol>';
                    var url_fot = '{{route('regFotoCampo')}}';

                    var tot_reg = msg['message'].length;
                    tot_reg = tot_reg - 1;

                    text += '<div class="carousel-inner">';
                    var src_ini = '{{ asset('uploads/FotoTodo/no_imagen.jpg')}}';
                    var campos_id = "";
                    var $avatarImage;
                    for(var i = 0; i < msg['message'].length; i++)
                    {

                        var campos = "";

                        campos += ' <table  class="table table-striped">\n' ;

                        for(var j = 0; j < msg['message'][i]['campos'].length; j++)
                        {
                            campos_id = msg['message'][i]['campos'][j]['Id'];
                            $avatarImage = $('#avatarImage'+campos_id);
                            var valor = 0;
                            valor = msg['message'][i]['campos'][j]['valor'];

                            var txtNA = "";
                            if(msg['message'][i]['campos'][j]['no_aplica'] == 1)
                            {
                                txtNA = '<button OnClick="btnClick('+campos_id+','+id_vis+', 3, '+valor+')" class="btn bg-yellow margin  ">N/A</button>';
                            }

                            var txtFotoCampo = '';
                            var txtFotoDesc = '';
                            var URLdomain = window.location.host;
                            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodo/';
                            var style_color = '';
                            if(msg['message'][i]['campos'][j]['info_campo']['existe'] == 1)
                            {
                                txtFotoCampo =  '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarForm'+msg['message'][i]['campos'][j]['Id']+'">'+
                                                '       <input type="file" id="avatarInput'+msg['message'][i]['campos'][j]['Id']+'" name="photo">'+
                                                '    </form>'+
                                                '    <div class="rutaFoto">'+
                                                '   <img onclick="setFotoCampo('+campos_id+','+id_vis+')" class="direct-chat-img" src="'+pathImg+msg['message'][i]['campos'][j]['info_campo']['foto']+'" id="avatarImage'+msg['message'][i]['campos'][j]['Id']+'">'+
                                                '    </div>';
                                var txtFotoDesc =  '  <h4> '+msg['message'][i]['campos'][j]['nombre']+'</h4>' +

                                                    '   <input type="text" maxlength="140" value="'+msg['message'][i]['campos'][j]['info_campo']['comentario']+'"  onchange="btnClick('+campos_id+','+id_vis+', 2, '+valor+')" id="inputCom'+msg['message'][i]['campos'][j]['Id']+'" name="coment'+msg['message'][i]['campos'][j]['Id']+'" placeholder="comentario" class="form-control" />    ' ;

                                if(msg['message'][i]['campos'][j]['info_campo']['gentodo'] == 1)
                                {
                                    style_color = ' border: 2px solid #DF0101; background-color: #eb7567';
                                }else
                                {
                                    if(msg['message'][i]['campos'][j]['info_campo']['no_aplica'] == 1)
                                    {
                                        style_color = ' border: 2px solid #FFC300; background-color: #e5eb8b';
                                    }else
                                    {
                                        style_color = ' border: 2px solid rgb(64, 255, 0); background-color: #80eb7f';
                                    }
                                }


                            }else
                            {
                                txtFotoCampo =  '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarForm'+msg['message'][i]['campos'][j]['Id']+'">'+
                                                '       <input type="file" id="avatarInput'+msg['message'][i]['campos'][j]['Id']+'" name="photo">'+
                                                '    </form>'+
                                                '    <div class="rutaFoto">'+
                                                '   <img onclick="setFotoCampo('+campos_id+','+id_vis+')" class="direct-chat-img" src="'+src_ini+'" id="avatarImage'+msg['message'][i]['campos'][j]['Id']+'">'+
                                                '    </div>';
                                var txtFotoDesc =  '  <h4> '+msg['message'][i]['campos'][j]['nombre']+'</h4>' +

                                                    '   <input type="text" maxlength="250" placeholder="Describe el campo"  onchange="btnClick('+campos_id+','+id_vis+', 2, '+valor+')" id="inputCom'+msg['message'][i]['campos'][j]['Id']+'" name="coment'+msg['message'][i]['campos'][j]['Id']+'" placeholder="comentario" class="form-control" />    ' ;
                                style_color = "";
                            }



                            campos +=   '        <tr id="cam'+msg['message'][i]['campos'][j]['Id']+'" style="border: 2px solid #CCCCCC;'+style_color+'">\n' +
                                '<td width="5%" style="   vertical-align:middle;horizontal-align:center;">' +txtFotoCampo+

                                '</td>\n' +
                                '<td  style="text-align:left" style="border: 1px solid #CCCCCC;"  width="50%">' +txtFotoDesc+

                                '</td>\n' +
                                '<td width="25%">' +
                                '      <button  class="btn bg-green margin " OnClick="btnClick('+campos_id+','+id_vis+', 0, '+valor+')">SI</button>' +
                                '      <button OnClick="btnClick('+campos_id+','+id_vis+', 1, '+valor+')"  class="btn bg-red margin  ">NO </button>' +txtNA+


                                '</td>\n' +
                                '                </tr>\n' ;

                        }

                        campos += '    </table> ';

                        if(i == 0)
                        {
                            text += '<div class="item active">';

                        }else if(i == tot_reg)
                        {
                            text += '<div class="item">';
                        }else
                        {
                            text += '<div class="item ">';
                        }

                        text += ' <div class="box box-solid " id="categoria0">\n' +
                            '            <div class="box-header with-border">\n' +
                            '                <h3 class="box-title"><strong>'+msg['message'][i]['nombre']+'</strong></h3>\n' +
                            '            </div>\n' +
                            '            <!-- /.box-header -->\n' +
                            '            <div class="box-body ">\n' +
                            '                <div class="box-group" >\n' +
                            '                    '+campos+'\n' +
                            '                </div>\n' +
                            '              \n' +
                            '              <div id="paginator'+i+'" >' +
                            '                <ul class="pager" >\n' +
                            '                </ul>\n' +
                            '             </div>' +
                            '\n' +

                            '            </div>\n' +
                            '            <!-- /.box-body -->\n' +
                            '        </div>' +

                            ' </div>';
                    }
                    text += '</div>';



                   // $('.campos_total').html( '<input type="hidden" id="campos_tot" value="'+msg['campos']+'" >' );
                   // $('.campos_visita').html( '<input type="hidden" id="campos_vis" value="0" >' );


                    var av = msg['cam_cal'];
                    var tot = msg['campos'];
                    var por = av / tot * 100;
                    //alert(por)

                    //var campos_tot = document.getElementById('campos_tot').value;


                    var avance = '<div class="box box-solid">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Has evaluado '+msg['cam_cal']+' de '+msg['campos']+' campos </h3>\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '              <div class="progress">\n' +
                        '                <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="'+msg['cam_cal']+'" aria-valuemin="0" aria-valuemax="'+msg['campos']+'" style="width: '+por+'%">\n' +
                        '                  <span class="sr-only">90% Complete (success)</span>\n' +
                        '                </div>\n' +
                        '              </div>\n' +
                        '              \n' +
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +


                        '          </div>';

                    $('.cargaAvance').html( avance )
                    $('.campos_visita').html( '<input type="hidden" id="campos_vis" value="'+msg['message']+'" >' )
                    $('.campos_total').html( '<input type="hidden" id="campos_tot" value="'+msg['campos']+'" >' )

                    var campos_tot = msg['campos'];
                    var campos_cal = msg['cam_cal'];

                    if(campos_cal >  campos_tot)
                    {
                        campos_cal = campos_tot;
                    }

                    if(campos_cal == campos_tot)
                    {
                        var avanceFin = '<div class="box box-solid">\n' +
                            '            <div class="box-header with-border">\n' +
                            '              <h3 class="box-title">Has evaluado '+campos_cal+' de '+msg['campos']+' campos </h3>\n' +
                            '            </div>\n' +
                            '            <!-- /.box-header -->\n' +
                            '            <div class="box-body">\n' +
                            '              <div class="progress">\n' +
                            '                <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="'+msg['cam_cal']+'" aria-valuemin="0" aria-valuemax="'+msg['campos']+'" style="width: '+por+'%">\n' +
                            '                  <span class="sr-only">90% Complete (success)</span>\n' +
                            '                </div>\n' +
                            '              </div>\n' +
                            '              \n' +
                            ' <button id="pager2"  onclick="showDespues()"  class="btn btn-success btn-block pull-right">Continuar</button>'+
                            '            </div>\n' +
                            '            <!-- /.box-body -->\n' +


                            '          </div>';
                        $('.cargaAvance').html( avanceFin );
                        document.getElementById("avance").scrollIntoView();

                        //$("#pager2").show();

                    }
                    $('.ajax-content').html( text );






                   // $('.cargaAvance').html(document.getElementById('campos_vis').value+" de " + document.getElementById('campos_tot').value +" campos evaluados");
                   // $("#infoVis").hide();
                    getLocation();


                });

        }

        function iniciarVisita()
        {
            var token = '{{csrf_token()}}';
            var url   = '{{route('genVis')}}';
            var user = '{{ Auth::user()->id }}';
            var selChk = document.getElementsByName('checklist')[0].value;
            var tiendas_Id = '{{$info->Id}}';
            //
            if(selChk != 0)
            {
                $("#t").timer({
                    action: 'start',
                    seconds: 0
                });

                $("#btn").attr("disabled", "disabled");
                $("#chk").attr("disabled", "disabled");
                $("#btnCan").attr("enabled", "enabled");

                $("#fotos").show();
                $("#pager").show();
                $("#t").addClass("badge-important");
                $("#button-car").show();

                $.ajax({
                    method: 'POST',
                    url: url,
                    data: {body:selChk, user: user,tienda: tiendas_Id, _token: token}
                })
                    .done(function(msg){
                        console.log(msg['campos']);
                        //alert(msg['id_visita']['id_visita']);
                        var text = "";
                        var carousel = "";
                        var id_visita = msg['id_visita']['id_visita'];

                        $('.editFotosVisita').html( '<a  onclick="editImgVisita('+id_visita+')" class="btn btn-primary btn-block margin-bottom">Editar</a>' )

                        carousel += '<!-- Indicators -->\n' +
                            '                         <ol class="carousel-indicators">\n' +
                            '                         <li data-target="#carousel" data-slide-to="0" class="active"></li>' ;

                        for(var i = 1; i < msg['message'].length; i++)
                        {
                            carousel += ' <li data-target="#carousel" data-slide-to="'+i+'" ></li>';
                        }
                        carousel +=  '                         </ol>';
                        var url_fot = '{{route('regFotoCampo')}}';

                        var tot_reg = msg['message'].length;
                        tot_reg = tot_reg - 1;

                        text += '<div class="carousel-inner">';
                        var src_ini = '{{ asset('uploads/FotoTodo/no_imagen.jpg')}}';
                        var campos_id = "";
                        var $avatarImage;
                        for(var i = 0; i < msg['message'].length; i++)
                        {

                            var campos = "";

                            campos += ' <table  class="table table-striped">\n' ;

                            for(var j = 0; j < msg['message'][i]['campos'].length; j++)
                            {
                                campos_id = msg['message'][i]['campos'][j]['Id'];
                                $avatarImage = $('#avatarImage'+campos_id);
                                var valor = 0;
                                valor = msg['message'][i]['campos'][j]['valor'];

                                var txtNA = "";
                                if(msg['message'][i]['campos'][j]['no_aplica'] == 1)
                                {
                                    txtNA = '<button OnClick="btnClick('+campos_id+','+id_visita+', 3, '+valor+')" class="btn bg-yellow margin  ">N/A</button>';
                                }


                                campos +=   '        <tr id="cam'+msg['message'][i]['campos'][j]['Id']+'" style="border: 2px solid #CCCCCC;">\n' +
                                    '<td width="5%" style="vertical-align:middle;horizontal-align:center;">' +

                                    '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarForm'+msg['message'][i]['campos'][j]['Id']+'">'+
                                    '       <input type="file" id="avatarInput'+msg['message'][i]['campos'][j]['Id']+'" name="photo">'+
                                    '    </form>'+
                                    '    <div class="rutaFoto">'+
                                    '   <img onclick="setFotoCampo('+campos_id+','+id_visita+')" class="direct-chat-img" src="'+src_ini+'" id="avatarImage'+msg['message'][i]['campos'][j]['Id']+'">'+
                                    '    </div>'+
                                    '</td>\n' +
                                    '<td  style="text-align:left" style="border: 1px solid #CCCCCC;"  width="50%">' +
                                    '  <h4> '+msg['message'][i]['campos'][j]['nombre']+'</h4>' +

                                    '  <div class="comentario"> <input maxlength="250" type="text" placeholder="Describe el campo"  onchange="btnClick('+campos_id+','+id_visita+', 2, '+valor+')" id="inputCom'+msg['message'][i]['campos'][j]['Id']+'" name="coment'+msg['message'][i]['campos'][j]['Id']+'" placeholder="comentario" class="form-control" />  </div>  ' +

                                    '</td>\n' +
                                    '<td width="25%">' +
                                    '      <button  class="btn bg-green margin " OnClick="btnClick('+campos_id+','+id_visita+', 0, '+valor+')">SI</button>' +
                                    '      <button OnClick="btnClick('+campos_id+','+id_visita+', 1, '+valor+')"  class="btn bg-red margin  ">NO </button>' +txtNA+



                                    '</td>\n' +
                                    '                </tr>\n' ;

                            }

                            campos += '    </table> ';

                            if(i == 0)
                            {
                                text += '<div class="item active">';

                            }else if(i == tot_reg)
                            {
                                text += '<div class="item">';
                            }else
                            {
                                text += '<div class="item ">';
                            }

                            text += ' <div class="box box-solid " id="categoria0">\n' +
                                '            <div class="box-header with-border">\n' +
                                '                <h3 class="box-title"><strong>'+msg['message'][i]['nombre']+'</strong></h3>\n' +
                                '            </div>\n' +
                                '            <!-- /.box-header -->\n' +
                                '            <div class="box-body ">\n' +
                                '                <div class="box-group" >\n' +
                                '                    '+campos+'\n' +
                                '                </div>\n' +
                                '              \n' +
                                '              <div id="paginator'+i+'" >' +
                                '                <ul class="pager" >\n' +
                                '                </ul>\n' +
                                '             </div>' +
                                '\n' +

                                '            </div>\n' +
                                '            <!-- /.box-body -->\n' +
                                '        </div>' +

                                ' </div>';
                        }
                        text += '</div>';

                        /*  '<div class="todo-count" >\n' +
                                '           </div>'*/

                        $('.campos_total').html( '<input type="hidden" id="campos_tot" value="'+msg['campos']+'" >' )
                        $('.campos_visita').html( '<input type="hidden" id="campos_vis" value="0" >' )

                        var av = document.getElementById('campos_vis').value;
                        var tot = document.getElementById('campos_tot').value;
                        var por = av / tot * 100;


                        var avance = '<div class="box box-solid">\n' +
                            '            <div class="box-header with-border">\n' +
                            '              <h3 class="box-title">Has evaluado '+document.getElementById('campos_vis').value+' de '+document.getElementById('campos_tot').value+' campos </h3>\n' +
                            '            </div>\n' +
                            '            <!-- /.box-header -->\n' +
                            '            <div class="box-body">\n' +
                            '              <div class="progress">\n' +
                            '                <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="'+document.getElementById('campos_vis').value+'" aria-valuemin="0" aria-valuemax="'+document.getElementById('campos_tot').value+'" style="width: '+por+'%">\n' +
                            '                  <span class="sr-only">90% Complete (success)</span>\n' +
                            '                </div>\n' +
                            '              </div>\n' +
                            '              \n' +
                            '            </div>\n' +
                            '            <!-- /.box-body -->\n' +


                            '          </div>';

                        $('.cargaAvance').html(avance);
                        $('.ajax-content').html( text );
                        //$("#infoVis").hide();
                        $('.id_visita').html( '<input type="hidden" id="id_visita" value="'+id_visita+'" >' );
                        getLocation();
                    } );
            }else
            {
                bootbox.alert("Debes elegir un checklist para contiinuar");
            }

        }
    </script>
    <script>


        function clearInput(cam)
        {
            var comentario_Id = "com"+cam;
            document.getElementById(comentario_Id).value = "";
        }

        function setDescripcionFotoVis(cam)
        {

            var comentario_Id = "com"+cam;
            var comentario = document.getElementById(comentario_Id).value;
            var url = '{{route('setDescFoto')}}'

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:cam,  comentario: comentario}
            })
                .done(function(msg){
                    console.log(msg['message']);
                });
        }

        function clearComentTodo(cam)
        {
            var comentario_Id = "comentario"+cam;
            document.getElementById(comentario_Id).value = "";
        }

        function btnClick( campo,  chk, gentodo, valor) {
            bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Guardando cambios...</div>' })
            var color = "cam"+campo;
            var coment = "inputCom"+campo;
            var foto = "avatarInput"+campo;;
            var file = 'file-input'+campo;


            if(document.getElementById(coment).value == '')
            {
                var input_coment = 'No se agregó comentario';
            }else
            {
                var input_coment = document.getElementById(coment).value;
            }

            if(document.getElementById(foto).value == '')
            {
                var input_foto = 'no_image.jpg';
            }else
            {
                var input_foto = document.getElementById(foto).value;
            }

            var url = '{{route('regCampo')}}';
            var name_color = document.getElementById(color).style.border;
            //alert(document.getElementById(color).style.border)
            if(gentodo == 1)
            {
                document.getElementById(color).style.border = "2px solid #DF0101" ;//rojo
                document.getElementById(color).style.backgroundColor = "#eb7567" ;//rojo
            }else if (gentodo == 0)
            {
                document.getElementById(color).style.border = "2px solid rgb(64, 255, 0)" ; //verde
                document.getElementById(color).style.backgroundColor = "#80eb7f" ;//verde
            }else if(gentodo == 2)
            {
               // $('.comentario').html('<input type="text" value="'+input_coment+'" placeholder="Describe el campo"  onchange="btnClick('+campo+','+chk+', 2, '+valor+')" id="inputCom'+campo+'" name="coment'+campo+'" placeholder="comentario" class="form-control" /> ');
                if(name_color == '2px solid rgb(204, 204, 204)')
                {
                    gentodo = 0;
                    document.getElementById(color).style.border = "2px solid rgb(64, 255, 0)" ;
                    //document.getElementById(color).style.backgroundColor = "#80eb7f" ;
                }else if(name_color == '2px solid rgb(64, 255, 0)')
                {
                    gentodo = 0;
                   // document.getElementById(color).style.backgroundColor = "#80eb7f" ;
                }else if(name_color == '2px solid rgb(223, 1, 1)')
                {
                    gentodo = 1;
                  //  document.getElementById(color).style.backgroundColor = "#eb7567" ;
                }else if(name_color == '2px solid rgb(255, 195, 0)')
                {
                    gentodo = 3;
                }


            }else if(gentodo == 3)
            {
                document.getElementById(color).style.border = "2px solid #FFC300" ;//amarillo
                document.getElementById(color).style.backgroundColor = "#e5eb8b" ;//amarillo


                //$('.comentario').html('<input type="text" value="'+input_coment+'" placeholder="Describe el campo"  onchange="btnClick('+campo+','+chk+', 3, '+valor+')" id="inputCom'+campo+'" name="coment'+campo+'" placeholder="comentario" class="form-control" /> ');

            }


            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo, visita_Id: chk, comentario: input_coment, foto: input_foto, gentodo: gentodo, valor: valor}
            })
                .done(function(msg){
                    bootbox.hideAll();
                    console.log(msg['message']);
                    var av = msg['message'];
                    var tot = document.getElementById('campos_tot').value;
                    var por = av / tot * 100;
                    //alert(por)

                    var campos_tot = document.getElementById('campos_tot').value;

                    var avance = '<div class="box box-solid">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Has evaluado '+msg['message']+' de '+document.getElementById('campos_tot').value+' campos </h3>\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '              <div class="progress">\n' +
                        '                <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="'+msg['message']+'" aria-valuemin="0" aria-valuemax="'+document.getElementById('campos_tot').value+'" style="width: '+por+'%">\n' +
                        '                  <span class="sr-only">90% Complete (success)</span>\n' +
                        '                </div>\n' +
                        '              </div>\n' +
                        '              \n' +
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +


                        '          </div>';

                    $('.cargaAvance').html( avance )
                    $('.campos_visita').html( '<input type="hidden" id="campos_vis" value="'+msg['message']+'" >' )

                    var campos_tot = parseInt(document.getElementById('campos_tot').value)
                    var campos_cal = parseInt(document.getElementById('campos_vis').value)

                    if(campos_cal > campos_tot)
                    {
                        campos_cal = campos_tot;
                    }

                    if(campos_cal == campos_tot)
                    {
                        //validamos que no haya campos repetidos
                        var avanceFin = '<div class="box box-solid">\n' +
                            '            <div class="box-header with-border">\n' +
                            '              <h3 class="box-title">Has evaluado '+campos_cal+' de '+document.getElementById('campos_tot').value+' campos </h3>\n' +
                            '            </div>\n' +
                            '            <!-- /.box-header -->\n' +
                            '            <div class="box-body">\n' +
                            '              <div class="progress">\n' +
                            '                <div class="progress-bar progress-bar-blue" role="progressbar" aria-valuenow="'+msg['message']+'" aria-valuemin="0" aria-valuemax="'+document.getElementById('campos_tot').value+'" style="width: '+por+'%">\n' +
                            '                  <span class="sr-only">90% Complete (success)</span>\n' +
                            '                </div>\n' +
                            '              </div>\n' +
                            '              \n' +
                            ' <button id="pager2"  onclick="showDespues()"  class="btn btn-success btn-block pull-right">Continuar</button>'+
                            '            </div>\n' +
                            '            <!-- /.box-body -->\n' +


                            '          </div>';
                        $('.cargaAvance').html( avanceFin );
                        document.getElementById("avance").scrollIntoView();

                        //$("#pager2").show();

                    }
                });
        }
         function setTodoCom(cam)
         {
                var foto_com_id = "fotocomid"+cam;
                var com_value = "comvalue"+cam;
                var com  = document.getElementById(com_value).value;
                var foto  = document.getElementById(foto_com_id).value;
                var url = '{{route('putFotoCom')}}';
             if(com == "")
             {
                 bootbox.alert("Debes escribir un comentario");
             }else
             {
                 //bootbox.alert(foto);


                 $.ajax({
                     method: 'POST',
                     url: url,
                     data: {campo_Id:cam, foto: foto, comentario: com}
                 })
                     .done(function(msg){


                         console.log(msg['message']);
                         var text = "";
                         var comentarios = 'comentario'+cam;
                         var url_cam = '{{ asset('images/buttons/camera.png')}}';
                         var URLdomain = window.location.host;
                         var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                         text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                         for(var i = 0; i < msg['message'].length; i++)
                         {
                             text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                         }
                         text += '</ul>\n';

                         //text += '<li><a href="#">Comentario 1 <span class="pull-right "><img  width="20px" height="20px" src="'+url_cam+'" ></span></a></li>';
                         $('.setListCom').html( text );

                         document.getElementById(com_value).value = "";
                         document.getElementById(foto_com_id).value = "no_imagen.jpg";
                     });
             }
         }

        function showInfoToDo(campo, campos_id)
        {
            $("#todolist").hide();
            $("#editTodo").show();

            var text = "";
            var URLdomain = window.location.host;
            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodo/';

            var url_cam = '{{ asset('images/buttons/camera.png')}}';

            url = '{{route('getInfoTodo')}}';
            var vis = document.getElementById('id_visita').value;
            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    var status_id = msg['message']['Status'];
                    var status_name = "";
                    switch(status_id)
                    {
                        case 151:
                            status_name = "Nuevo";
                            break;
                        case 152:
                            status_name = "En curso";
                            break;
                        case 153:
                            status_name = "Pendiente";
                            break;
                        case 154:
                            status_name = "Concluida";
                            break;
                    }
                    var url_fot = '{{route('regFotoCampo')}}';
                    var url_fot_com = '{{route('regFotoCampoCom')}}';

                    text += '<div class="widget-user-header bg-blue">\n' +
                        '    <form action="'+url_fot+'" method="post" style="display: none" id="avatarFormT'+campos_id+'">'+
                        '       <input type="file" id="avatarInputT'+campos_id+'" name="photo">'+
                        '    </form>'+
                                    '    <div class="widget-user-image rutaFotoT">'+
                        '   <img onclick="setFotoCampoTodo('+campos_id+','+vis+')"  src="'+pathImg+msg['message']['ImagenIni']+'"id="avatarImageT'+campos_id+'">'+
                        '    </div>'+
                       // '                    <div class="widget-user-image"  >\n' +
                        //'                        <img  src="'+pathImg+msg['message']['ImagenIni']+'" alt="User Avatar">\n' +
                        //'                    </div>\n' +
                        '                    <!-- /.widget-user-image -->\n' +
                        '\n' +
                        '                    <h3  class="widget-user-username nombre_campo">'+msg['message']['nombre']+'</h3>\n' +
                        '                    <div style="padding-top: 20px">\n' +
                        '                        <strong><i class="fa fa-book margin-r-5"></i> Descripción</strong>\n' +
                        '                        <input onclick="clearComentTodo('+campo+')" id="comentario'+campo+'" type="text" value="'+msg['message']['descripcion']+'" class="form-control">\n' +
                        '                        <strong><i class="fa fa-flag-o margin-r-5"></i> Status</strong>\n' +
                        '                        <select class="form-control select2" id="stat'+campo+'" name="status" style="width: 100%;">\n' +
                        '                            <option value="'+status_id+'" selected="selected">'+status_name+'</option>\n' +
                        '                            <option  value="152">En Curso</option>\n' +
                        '                            <option value="153">Pendiente</option>\n' +
                        '                            <option value="154">Concluida</option>\n' +
                        '\n' +
                        '                        </select>\n' +
                        '                        <strong><i class="fa fa-commenting margin-r-5"></i> Comentarios</strong>\n' +
                        '                        <table width="100%">\n' +
                        '                            <tr>\n' +
                        '                                <td width="10%">\n' +
                        //'                                    <img  class="direct-chat-img" src="'+url_cam+'" >\n' +
                                                            '    <form action="'+url_fot_com+'" method="post" style="display: none" id="avatarFormTc'+campo+'">\n'+
                                                            '       <input type="file" class="direct-chat-img" id="avatarInputTc'+campo+'" name="photo">\n'+
                                                            '    </form>\n'+
                                                            '    <div class="widget-user-image rutaFotoTc">\n'+
                                                            '   <img onclick="setFotoCampoTodoCom('+campo+','+vis+')"  src="'+url_cam+'" id="avatarImageTc'+campo+'">\n'+
                                                            '    </div>\n'+
                        '                                </td>\n' +
                        '                                <td width="80%">\n' +
                        '                                    <input id="comvalue'+campo+'" type="text" placeholder="comentario" class="form-control">\n' +
                        '                                </td>\n' +
                        '                                <td width="10%">\n' +
                        '                                <input type="hidden" id="fotocomid'+campo+'" value="no_imagen.jpg">\n' +
                        '                                    <a onclick="setTodoCom('+campo+')"   class="btn btn-success btn-block pull-right">Agregar</a>\n' +
                        '                                </input>\n' +
                        '                            </tr>\n' +
                        '                        </table>\n' +
                        '                    </div>\n' +
                        '\n' +
                        '                </div>';
                    text += '<div class="box-footer no-padding">\n' +
                        '\n' +
                            '<div class="setListCom">'+

                        '</div >'+
                        '                    <a onclick="saveTodo('+campo+')"   class="btn btn-primary btn-block pull-right">Regresar</a>\n' +
                        ' </div>';

                    $('.infoTodo').html( text )


                    document.getElementById("editTodo").scrollIntoView();
                    var url_coms = '{{route('getFotoComs')}}'

                    $.ajax({
                        method: 'POST',
                        url: url_coms,
                        data: {campo_Id:campo}
                    })
                        .done(function(msg){

                            console.log(msg['message']);
                            var text = "";
                            var comentarios = 'comentario'+campo;
                            var url_cam = '{{ asset('images/buttons/camera.png')}}';
                            var URLdomain = window.location.host;
                            var pathImg = 'http://'+URLdomain+'/uploads/FotoTodoCom/';

                            text += '<ul class="nav nav-stacked " style="padding-bottom: 20px">\n';
                            for(var i = 0; i < msg['message'].length; i++)
                            {
                                text +=  '<li><a href="#">'+msg['message'][i]['descripcion']+' <span class="pull-right "><img  width="20px" height="20px" src="'+pathImg+msg['message'][i]['foto']+'" ></span></a></li>';
                            }
                            text += '</ul>\n';
                            $('.setListCom').html( text );
                        });
                });
        }

        function saveTodo(campo)
        {

            var coment = "comentario"+campo;
            var select = "stat"+campo;;
            var input_coment = document.getElementById(coment).value;
            var input_select = document.getElementById(select).value;
            var url = '{{route('updateTodo')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {campo_Id:campo, comentario: input_coment, status: input_select}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    showDespues();
                });
        }

        function showDespues()
        {
            $("#pager").hide();
            $("#pager2").hide();
            $("#carousel").hide();
            $("#avance").hide();
            $("#todolist").show();
            $("#editTodo").hide();
            $('.planSmart').html( '' );
            $("#panelVisita").show();
            $("#infoVis").hide();
            $("#button-car").show();



            document.getElementById("todolist").scrollIntoView();
            url = '{{route('getToDoVis')}}';
           // alert('Muestra despues')

            $.ajax({
                method: 'POST',
                url: url,
                data: { visita_Id: document.getElementById('id_visita').value}
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    var button = '{{ asset('images/buttons/browser.png')}}';
                    //text = msg['message'][1]['nombre'];
                    var campo = "";
                    var desc = "";
                    var status_id = msg['message']['Status'];
                    var status_name = "";
                    var status_class = "";
                    var smart = "";


                   for(var i = 0; i < msg['message'].length; i++)
                    {

                        status_id = msg['message'][i]['Status'];

                        switch(status_id)
                        {
                            case 151:
                            case '151':
                                status_name = "Nuevo";
                                status_class = "label-danger";
                                break;
                            case 152:
                            case '152':
                                status_name = "En curso";
                                status_class = "label-warning";
                                break;
                            case 153:
                            case '153':
                                status_name = "Pendiente";
                                status_class = "label-info";
                                break;
                            case 154:
                            case '154':
                                status_name = "Concluida";
                                status_class = "label-success";
                                break;
                        }

                        smart = '<a  class="product-title">'+msg['message'][i]['nombre']+' <span class="label '+status_class+'   pull-right">'+status_name+'</span></a>';
                        if(msg['message'][i]['smart'] == 1)
                        {
                            smart = '<a  class="product-title">'+msg['message'][i]['nombre']+' <a onclick="valSmart('+msg['message'][i]['Id']+')" class="label label-primary">Smart</a> <span class="label '+status_class+'   pull-right">'+status_name+'</span></a>';
                        }
                        //msg['message'][i]['nombre']
                       // text += msg['message'][i]['descripcion'];
                        text += ' <!-- /.item -->\n' +
                            '                    <li class="item">\n' +
                            '                        <div class="product-img">\n' +
                            '                            <img data-toggle="tooltip" onclick="showInfoToDo('+ msg['message'][i]['Id']+','+msg['message'][i]['campos_Id']+')"  title="Agregar Información" src="'+button+'"  alt="Product Image">\n' +
                            '                        </div>\n' +
                            '                        <div class="product-info">\n' +
                            ''+smart+ ''+
                            '                            <span class="product-description">\n' +
                            '                          '+msg['message'][i]['descripcion']+'\n' +
                            '                        </span>\n' +
                            '                        </div>\n' +
                            '                    </li>';
                    }

                    $('.todo-list').html( text )

                });


        }

    </script>

    <script>
        $("#btnFin").hide;
        var token = '{{csrf_token()}}';
        var url   = '{{route('genVis')}}';
        $('#chk').on('change' , function () {
            // alert(document.getElementsByName('checklist')[0].value);
            var selChk = document.getElementsByName('checklist')[0].value;
            $("#passBtn").removeAttr('disabled');
            $("#passRec").removeAttr('disabled');
            var val = '{{$config->valida_vis}}';
            //alert(val)
            switch(val)
            {
                case 0: case "0":


                $("#btn").removeAttr('disabled');
                break;
                case 1: case "1":
                    bootbox.alert('Antes de iniciar la visita, debes introducir tu contraseña de tienda');

                break;
                case 2: case "2":
                    bootbox.alert('Antes de iniciar la visita, debes escanear el código de barras de la tienda');
                break;
                case 3: case "3":
                $("#btn").removeAttr('disabled');
                break;
            }

        });


    </script>
    <script type="text/javascript">

        //validamos si ya existe el registro de plan smart, si no, enviamos directo a su vista

        function valSmart(id)
        {

            var url_smart = '{{route('validaReg')}}';
            $.ajax({
                method: 'POST',
                url: url_smart,
                data: {todo_Id: id}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    if(msg['message'] == 1)
                    {
                        getSmart(id);
                    }else
                    {
                        setSmart(id);
                    }

                    // var visita_Id = document.getElementById('id_visita').value;

                });
        }

        function setSmart(id)
        {

            //alert(id)
            bootbox.confirm({
                title: "Plan Smart",
                message: "Si continuas, se creará el registro correspondiente del plan smart, ¿Deseas proceder?",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancelar'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Iniciar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    {
                        var post_id = id;
                        var url_can = '{{route('setSmart')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_can,
                            data: {campos_id:post_id}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                $('.id_smart').html( '<input type="hidden" value="'+msg['message']+'" id="id_smart" >' )

                                getSmart(id);

                            });
                    }
                }
            });
        }

        function getSmart(id)
        {

            // var visita_Id = document.getElementById('id_visita').value;
            var url_smart = '{{route('getSmart')}}';

            $.ajax({
                method: 'POST',
                url: url_smart,
                data: {todo_Id:id}
            })
                .done(function(msg){
                    console.log(msg['message']);

                    $("#pager").hide();
                    $("#pager2").hide();
                    $("#carousel").hide();
                    $("#avance").hide();
                    $("#todolist").hide();
                    $("#editTodo").hide();
                    $("#panelVisita").hide();

                    var text = "";

                    text +=  '    <!--PLAN SMART-->\n' +
                        '\n' +
                        '<div class="box box-warning ">\n' +
                        '    <div class="box-header with-border">\n' +
                        '        <h3 class="box-title">Plan smart para campo</h3>\n' +
                        '    </div>\n' +
                        '    <!-- /.box-header -->\n' +
                        '    <div class="box-body">\n' +
                        '        <form role="form">\n' +
                        '            <!-- text input -->\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Objetivo</label>\n' +
                        '                <input type="text" class="form-control" value="'+msg['message']['objetivo']+'" id="inputObjetivo" placeholder="Describe el objetivo del plan">\n' +
                        '            </div>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <label>Indicador Actual</label>\n' +
                        '                        <input type="text" class="form-control" value="'+msg['message']['indicador_actual']+'" id="inputActual" placeholder="Describe el indicador actual">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <label>Indicador Deseado</label>\n' +
                        '                        <input type="text" class="form-control" value="'+msg['message']['indicador_deseado']+'" id="inputDeseado" placeholder="Describe el indicador deseado">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '            </div><hr>\n' +
                        '            <label>Listado de Acciones</label>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputAccion" placeholder="Describe la accion">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputTarea" placeholder="Describe la tarea">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '\n' +
                        '            </div>\n' +
                        '            <div class="row">\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                        <input type="text" class="form-control" id="inputResponsable" placeholder="Indica el responsable">\n' +
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="col-md-6">\n' +
                        '                    <div class="form-group">\n' +
                        '                                   <div class="input-group date">\n' +
                        '                                        <div class="input-group-addon">\n' +
                        '                                            <i class="fa fa-calendar"></i>\n' +
                        '                                        </div>\n' +
                        '                                        <input type="text" placeholder="Fecha de Entrega" data-date-format="yyyy-mm-dd" class="form-control pull-right" id="datepicker">\n' +
                        '                                    </div>'+
                        '                    </div>\n' +
                        '                </div>\n' +
                        '                <div class="form-group" style="padding-right: 20px">\n' +
                        '                    <a onclick="setAcciones()" class="btn btn-success pull-right" >Agregar</a>\n' +
                        '                </div>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '            <hr>\n' +
                        '            <div class="box-body">\n' +
                        '                <h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">\n' +
                        '                    Acciones\n' +
                        '                </h4>\n';

                   text +=      '    <div class="listaAcciones"  ></div>        <!-- textarea -->\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Requerimientos</label>\n' +
                        '                <textarea class="form-control" rows="3"  id="inputReq" placeholder="Detalla los requerimientos ...">'+msg['message']['requerimientos']+'</textarea>\n' +
                        '            </div>\n' +
                        '            <div class="form-group">\n' +
                        '                <label>Comentarios</label>\n' +
                        '                <textarea class="form-control"  rows="3" id="inputComs" placeholder="Comentarios adicionales ...">'+msg['message']['comentarios']+'</textarea>\n' +
                        '            </div>\n' +
                        '\n' +
                        '\n' +
                        '\n' +
                        '        </form>\n' +
                        ' <a onclick="showDespues()"  class="btn btn-danger  margin-bottom pull-right">Cancelar</a>'+
                        ' <a  onclick="updSmart('+id+')" class="btn btn-primary  margin-bottom pull-right">Guardar</a>'+

                        '    </div>\n' +
                        '    <!-- /.box-body -->\n' +
                        '</div>';

                        $('.id_smart').html( '<input type="hidden" value="'+msg['message']['Id']+'" id="id_smart" >' )



                        $('.planSmart').html( text );

                        document.getElementById("planSmart").scrollIntoView();

                        getAcciones(msg['message']['Id']);



                    //Date picker
                    $('#datepicker').datepicker({
                        autoclose: true,
                        format: 'yyyy-mm-dd'
                    })


                    // var visita_Id = document.getElementById('id_visita').value;

                });
        }

        function setAcciones()
        {
            var url_can = '{{route('setAccion')}}';


            if(document.getElementById('inputAccion').value != "")
            {
                if(document.getElementById('inputAccion').value != "")
                {
                    var visitas_smart_Id = document.getElementById('id_smart').value;
                    var accion = document.getElementById('inputAccion').value;
                    var tareas =  document.getElementById('inputTarea').value;
                    var responsable =  document.getElementById('inputResponsable').value;
                    var fhentrega = document.getElementById('datepicker').value;


                    $.ajax({
                        method: 'POST',
                        url: url_can,
                        data: {visitas_smart_Id:visitas_smart_Id,
                            accion:accion,
                            tareas:tareas,
                            responsable:responsable,
                            fhentrega:fhentrega
                        }
                    })
                        .done(function(msg){
                            console.log(msg['message']);

                            document.getElementById('inputAccion').value = "";
                            document.getElementById('inputTarea').value = "";
                            document.getElementById('inputResponsable').value = "";
                            document.getElementById('datepicker').value = "";
                            getAcciones(visitas_smart_Id);

                        });
                }else
                {
                    bootbox.alert("Debes definir una fecha de entrega para continuar")
                }

            }else
            {
                bootbox.alert("Debes describir una acción para continuar")
            }


        }

        function getAcciones(id)
        {
            var url_can = '{{route('getAcciones')}}';
            $.ajax({
                method: 'POST',
                url: url_can,
                data: {visitas_smart_Id:id,

                }
            })
                .done(function(msg){
                    console.log(msg['message']);
                    var text = "";
                    for(var i = 0; i < msg['message'].length; i++)
                    {
                        text += '   <!--Inicio for acciones -->            <div class="media">\n' +
                            '                    <div class="media-body">\n' +
                            '                        <div class="clearfix">\n' +
                            '                            <p class="pull-right">\n' +
                            '                                <!--<a href="https://themequarry.com/theme/ample-admin-the-ultimate-dashboard-template-ASFEDA95" class="btn btn-success btn-sm ad-click-event">\n' +
                            '                                    LEARN MORE\n' +
                            '                                </a>-->\n' +
                            '                            </p>\n' +
                            '\n' +
                            '                            <h4 style="margin-top: 0">Acción: '+msg['message'][i]['accion']+' <span style="color: #0A4DB7">('+msg['message'][i]['responsable']+')</span> <strong>'+msg['message'][i]['avance']+'%</strong> </h4>\n' +
                            '\n' +
                            '                            <p>Tareas: '+msg['message'][i]['tareas']+' </p>\n' +
                            '                            <p style="margin-bottom: 0">\n' +
                            '                                <i class="fa fa-calendar-check-o margin-r5"></i> Entrega: '+msg['message'][i]['fhentrega']+'\n' +
                            '                            </p>\n' +
                            ' <hr>\n' +

                            '                            <p style="margin-bottom: 0">\n' +
                            '                                <div class="col-sm-6">\n' +
                            '                                    <!-- Progress bars -->\n' +
                            '                                    <p>\n' +

                            '                                           </p>\n' +
                            '                                    <p>\n' +

                            '                                    </p>\n' +
                            '\n' +
                            '\n' +

                            '                                </div>\n' +
                            '\n' +
                            '                            </p>\n' +
                            '                        </div>\n' +
                            '                    </div>\n' +
                            '                </div>\n' +
                            '            </div>\n' +
                            ' <!--Fin de for acciones-->\n';

                        $('.listaAcciones').html( text );

                    }

                });
        }

        function updSmart(id)
        {
            var url_can = '{{route('updSmart')}}';


            var objetivo = document.getElementById('inputObjetivo').value;
            var indicador_actual = document.getElementById('inputActual').value;
            var indicador_deseado =  document.getElementById('inputDeseado').value;
            var requerimientos =  document.getElementById('inputReq').value;
            var comentarios = document.getElementById('inputComs').value;

            if(document.getElementById('inputObjetivo').value != "")
            {
                $.ajax({
                    method: 'POST',
                    url: url_can,
                    data: {objetivo:objetivo,
                        indicador_actual:indicador_actual,
                        indicador_deseado:indicador_deseado,
                        requerimientos:requerimientos,
                        comentarios:comentarios,
                        todo_Id: id
                    }
                })
                    .done(function(msg){
                        console.log(msg['message']);
                        showDespues();

                    });
            }else
            {
                bootbox.alert('Debes definir un objetivo del plan');
            }

        }



        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()


            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            })
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass   : 'iradio_minimal-red'
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })


        })

        $(document).ready(function() {


            var url_ser = '';

            $("#fotos").hide();
            $("#pager").hide();
            $("#pager2").hide();
            $("#btn").attr("disabled", "disabled");
            $("#todolist").hide();
            $("#fotolist").hide();
            $("#editTodo").hide();
            $("#infoVis").show();
           // goCuestionario()
            $("#cuestionario").hide();
            $("#pass").removeAttr('disabled');



            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })

            $('[data-toggle="tooltip"]').tooltip();
            var StatusProc = '{{$en_proceso->Status}}';
            var visitas_Id = '{{$en_proceso->Id}}'
            getEnProceso(StatusProc, visitas_Id);
           // alert('{{$en_proceso->Status}}');
          //mostrar tipo de validación de visita
            var val = '{{$config->valida_vis}}';
            //alert(val)
            switch(val)
            {
                case 0: case "0":
                    $("#valUbicacion").hide();
                    $("#valEscaneo").hide();
                    $("#valPass").hide();
                    break;
                case 1: case "1":
                    $("#valUbicacion").hide();
                    $("#valEscaneo").hide();
                    $("#valPass").show();

                    break;
                case 2: case "2":
                    $("#valUbicacion").hide();
                    $("#valEscaneo").show();
                    $("#valPass").hide();
                    break;
                case 3: case "3":
                    $("#valUbicacion").show();
                    $("#valEscaneo").hide();
                    $("#valPass").hide();
                    break;
            }


            showWidgets();

        } );


        function getFoda(id)
        {


            var url = '{{route('getFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {id:  id }
            })
                .done(function(msg){
                    console.log(msg['foda']);
                    var text ="";

                    text += ' <div style="width=80%" class="box box-default direct-chat direct-chat-default">\n' +
                        '        <div class="box-header with-border">\n' +
                        '            <h3 class="box-title">FODA</h3>\n' +
                        '\n' +
                        '            <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-header -->\n' +
                        '        <div class="box-body">';

                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-warning">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Fortalezas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="fortaleza"  style="resize: none; width: 100%; height: 50px" >'+msg['foda'][0]['fortaleza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-primary">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Oportunidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="oportunidad" style="resize: none; width: 100%; height: 50px" >'+msg['foda'][0]['oportunidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';
                    text += '<div class="row">' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-success">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Debilidades</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="debilidad"  style="resize: none; width: 100%; height: 50px" >'+msg['foda'][0]['debilidad']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '<div class="col-md-6">\n' +
                        '          <div class="box box-danger">\n' +
                        '            <div class="box-header with-border">\n' +
                        '              <h3 class="box-title">Amenazas</h3>\n' +
                        '\n' +
                        '              <div class="box-tools pull-right">\n' +
                        '                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>\n' +
                        '                </button>\n' +
                        '              </div>\n' +
                        '              <!-- /.box-tools -->\n' +
                        '            </div>\n' +
                        '            <!-- /.box-header -->\n' +
                        '            <div class="box-body">\n' +
                        '<textarea id="amenaza"  style="resize: none; width: 100%; height: 50px" >'+msg['foda'][0]['amenaza']+'</textarea>'+
                        '            </div>\n' +
                        '            <!-- /.box-body -->\n' +
                        '          </div>\n' +
                        '          <!-- /.box -->\n' +
                        '        </div>' +
                        '</div>';

                    text += '  </div>\n' +
                        '        <!-- /.box-body -->\n' +
                        '        <div class="box-footer">\n' +
                        '            <form action="#" method="post">\n' +
                        '                <div class="input-group">\n' +
                        '                    <span class="input-group-btn">\n' +
                        '                            <button onclick="updateFODA()" type="button" class="btn btn-primary btn-flat pull-right">Actualizar</button>\n' +
                        '                          </span>\n' +
                        '                </div>\n' +
                        '            </form>\n' +
                        '        </div>\n' +
                        '        <!-- /.box-footer-->\n' +
                        '    </div>';
                    $('.foda').html( text );

                });


        }

        function updateFODA()
        {
            var tiendas_Id = '{{$info->Id}}';
            var url = '{{route('updateFoda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:  tiendas_Id,
                    fortaleza:document.getElementById('fortaleza').value,
                    oportunidad: document.getElementById('oportunidad').value,
                    debilidad: document.getElementById('debilidad').value,
                    amenaza: document.getElementById('amenaza').value }
            })
                .done(function(msg) {
                    console.log(msg['foda']);
                    bootbox.alert("¡FODA actualizado con éxito!")
                });

        }

        function getEnProceso(status, visita_id)
        {
            $('.id_visita').html( '<input type="hidden" id="id_visita" value="'+visita_id+'" >' );

            if(status == 133)
            {
                bootbox.confirm({
                    title: "Visita en Proceso",
                    message: "Existe una visita en proceso, ¿Qué deseas hacer?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancelar Visita'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Recuperar visita'
                        }
                    },
                    callback: function (result) {
                        console.log(result);

                        if(result === true)
                        {
                            var post_id = visita_id;
                            var url_can = '{{route('recuperaVisita')}}';

                            $.ajax({
                                method: 'POST',
                                url: url_can,
                                data: {id:post_id}
                            })
                                .done(function(msg){
                                    console.log(msg['message']);
                                    $("#t").timer({
                                        action: 'start',
                                        seconds: msg['segundos']
                                    });

                                    $("#btn").attr("disabled", "disabled");
                                    $("#chk").attr("disabled", "disabled");
                                    $("#btnCan").attr("enabled", "enabled");
                                    $("#fotos").show();
                                    $("#pager").show();
                                    $("#t").addClass("badge-important");
                                    $('.select2').html( '<option selected value="'+msg['message']['checklist_Id']+'"> '+msg['message']['checklist']+'</option>' )
                                    editImgVisita(post_id);
                                    reanudaVisita(post_id);
                                });

                        }else
                        {
                            cancelaEnProceso(status, visita_id);
                        }

                    }
                });
            }

        }

        function cancelaEnProceso(status, visita_id)
        {
            url = '{{route('tiendas.tiendas')}}'
            bootbox.confirm({
                title: "Cancelar Visita",
                message: "¿Deseas cancelar visita? no podrás retomarla más adelante.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Salir'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Cancelar'
                    }
                },
                callback: function (result) {
                    console.log(result);

                    if(result === true)
                    {
                        var post_id = visita_id;
                        var time = "";

                        var url_can = '{{route('cancelaVisita')}}';

                        $.ajax({
                            method: 'POST',
                            url: url_can,
                            data: {id:post_id, time: time}
                        })
                            .done(function(msg){
                                console.log(msg['message']);
                                bootbox.alert('Visita en proceso cancelada con éxito, inicia una nueva')
                            });

                    }else
                    {
                        //regresar a eleccion
                        getEnProceso(status, visita_id);
                    }

                }
            });
        }



    </script>
    <script type="text/javascript">


        // $("#fotos").hide();

        function showWidgets()
        {



          // text += "HOLA PERROS"



            var tiendas_Id = '{{$info->Id}}';
            var url = '{{route('getFuncionesTienda')}}';
             $.ajax({
                  method: 'POST',
                  url: url,
                  data: {id:tiendas_Id}
              })
                  .done(function(msg){

                      console.log(msg['modulos']);
                      var text = "";

                      text += '<div id="carousel-widgets" class="carousel slide"   data-ride="carousel">' +
                          '       <ol class="carousel-indicators">';
                      text += ' <li  data-target="#carousel-widgets" data-slide-to="0" class=""></li>';

                      for(var i = 0; i < msg['modulos'].length; i++)
                      {
                          var mod = $("#widget"+msg['modulos'][i]['widgets_tiendas_Id']);
                          var li = $("#li"+msg['modulos'][i]['widgets_tiendas_Id']);
                          switch(msg['modulos'][i]['widgets_tiendas_Id'])
                          {
                              case 1:
                                  text += ' <li id="li1" data-target="#carousel-widgets" data-slide-to="1" class="active"></li>';

                                  break;
                              case 2:
                                  text += ' <li id="li2" data-target="#carousel-widgets" data-slide-to="2" class=""></li>';

                                  break;
                              case 3:
                                  text += ' <li id="li3" data-target="#carousel-widgets" data-slide-to="3" class=""></li>';
                                  break;
                              case 4:
                                  text += ' <li id="li4" data-target="#carousel-widgets" data-slide-to="4" class=""></li>';
                                  break;
                              case 5:
                                  text += ' <li id="li5" data-target="#carousel-widgets" data-slide-to="5" class=""></li>';
                                  break;
                              case 6:
                                  text += ' <li id="li6" data-target="#carousel-widgets" data-slide-to="6" class=""></li>';
                                  break;
                              case 7:
                                  text += ' <li id="li7" data-target="#carousel-widgets" data-slide-to="7" class=""></li>';
                                  break;
                              case 8:
                                  text += ' <li id="li8" data-target="#carousel-widgets" data-slide-to="8" class=""></li>';
                                  break;
                              case 9:
                                  text += ' <li id="li9" data-target="#carousel-widgets" data-slide-to="9" class=""></li>';
                                  break;

                          }


                      }

                      text += '  </ol>';

                      text += '  <div class="carousel-inner"> ';


                      text += ' <div style="height: 500px; width:100%"  class="item active">\n' +
                          '  <div  class="fotowid">\n' +
                          '\n' +
                          '    </div>' +
                          '\n' +
                          '                                <div class="carousel-caption">\n' +
                          '\n' +
                          '                                </div>\n' +
                          '                            </div>';



                      for(var j = 0; j < msg['modulos'].length; j++)
                      {
                          var mod = $("#widget"+msg['modulos'][j]['widgets_tiendas_Id']);
                          var li = $("#li"+msg['modulos'][j]['widgets_tiendas_Id']);



                         switch(msg['modulos'][j]['widgets_tiendas_Id'])
                          {
                              case 1:


                                  text += '<div  style="height: 500px" id="widget1" class="item ">\n' +
                                      '                                <div class="row">\n' +
                                      '                                    <div class="col-md-8">\n' +
                                      '                                        <p class="text-center">\n' +
                                      '                                            <strong>Histórico Anual de To-Do\'s</strong>\n' +
                                      '                                        </p>\n' +
                                      '\n' +
                                      '                                        <div class="chart">\n' +
                                      '                                           \n' +
                                      '\n' +
                                      '                                            <div style="width:100%; height: 100%;">\n' +
                                      '                                                <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">\n' +
                                      '                                                    <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">\n' +
                                      '                                                        <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0">\n' +
                                      '\n' +
                                      '                                                        </div>\n' +
                                      '                                                    </div>\n' +
                                      '                                                    <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">\n' +
                                      '                                                        <div style="position:absolute;width:200%;height:200%;left:0; top:0">\n' +
                                      '\n' +
                                      '                                                        </div>\n' +
                                      '                                                    </div>\n' +
                                      '                                                </div>\n' +
                                      '                                                <canvas id="canvas" style="display: block; width: 378px; height: 200px;" width="378" height="200" class="chartjs-render-monitor"></canvas>\n' +
                                      '                                            </div>\n' +
                                      '                                        </div>\n' +
                                      '                                       \n' +
                                      '                                    </div>';

                                  var concluidos = '{{$concluidos}}';
                                  var totales = '{{$totales}}';
                                  var avance_concluidos = 0;

                                  var nuevos = '{{$nuevos}}';
                                  var avance_nuevos = 0;

                                  var curso = '{{$curso}}';
                                  var avance_curso = 0;

                                  var pendientes = '{{$pendientes}}';
                                  var avance_pend = 0;
                                  if(concluidos != 0)
                                  {
                                      avance_concluidos = concluidos / totales * 100;
                                  }

                                  if(nuevos != 0)
                                  {
                                      avance_nuevos = nuevos / totales * 100;
                                  }

                                  if(curso != 0)
                                  {
                                      avance_curso = curso / totales * 100;
                                  }

                                  if(pendientes != 0)
                                  {
                                      avance_pend = pendientes / totales * 100;
                                  }

                                  text += '<div class="col-md-4">\n' +
                                      '                                        <p class="text-center">\n' +
                                      '                                            <strong>Detalle de To-Do\'s</strong>\n' +
                                      '                                        </p>\n' +
                                      '\n' +
                                      '                                        <div class="progress-group">\n' +
                                      '                                            <span class="progress-text">Concluidos</span>\n' +
                                      '                                            <span class="progress-number"><b>'+concluidos+'</b>/'+totales+'</span>\n' +

                                      '\n' +
                                      '                                            <div class="progress sm">\n' +
                                      '                                                <div class="progress-bar progress-bar-green" style="width: '+avance_concluidos+'%"></div>\n' +
                                      '                                            </div>\n' +
                                      '                                        </div>';


                                  text += ' <div class="progress-group">\n' +
                                      '                                            <span class="progress-text">Nuevos</span>\n' +
                                      '                                            <span class="progress-number"><b>'+nuevos+'</b>/'+totales+'</span>\n' +
                                      '                                           \n' +
                                      '                                            <div class="progress sm">\n' +
                                      '                                                <div class="progress-bar progress-bar-red" style="width: '+avance_nuevos+'%"></div>\n' +
                                      '                                            </div>\n' +
                                      '                                        </div>';
                                  text += ' <div class="progress-group">\n' +
                                      '                                            <span class="progress-text">En Curso</span>\n' +
                                      '                                            <span class="progress-number"><b>'+curso+'</b>/'+totales+'</span>\n' +
                                      '                                           \n' +
                                      '                                            <div class="progress sm">\n' +
                                      '                                                <div class="progress-bar progress-bar-red" style="width: '+avance_curso+'%"></div>\n' +
                                      '                                            </div>\n' +
                                      '                                        </div>';
                                  text += ' <div class="progress-group">\n' +
                                      '                                            <span class="progress-text">Pendientes</span>\n' +
                                      '                                            <span class="progress-number"><b>'+pendientes+'</b>/'+totales+'</span>\n' +
                                      '                                           \n' +
                                      '                                            <div class="progress sm">\n' +
                                      '                                                <div class="progress-bar progress-bar-red" style="width: '+avance_pend+'%"></div>\n' +
                                      '                                            </div>\n' +
                                      '                                        </div>';

                                  text += '</div></div></div>';

                                  genChart();
                                  break;
                              case 2:
                                  text += '  <div  style="height: 500px"  id="widget2"  class="item">\n' +
                                      '                                <div  class="foda">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div> ';

                                 getFoda(tiendas_Id);
                                  break;
                              case 3:
                                  text += ' <div style="height: 500px" id="widget3"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/f39c12/ffffff&amp;text=Empleados+de+sucursal" alt="Empleados de sucursal">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 4:
                                  text += '<div  style="height: 500px" id="widget4"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/E1321D/ffffff&amp;text=Ticket+promedio" alt="Ticket promedio">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 5:
                                  text += '<div style="height: 500px" id="widget5"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/AFE11D/ffffff&amp;text=Transacciones+promedio" alt="Transacciones promedio">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 6:
                                  text += ' <div style="height: 500px" id="widget6"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/1DA0E1/ffffff&amp;text=Articulos+x+ticket+promedio" alt="">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 7:
                                  text += '<div style="height: 500px" id="widget7"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/581DE1/ffffff&amp;text=Matriz+ticket+vs+transacciones" alt="">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 8:
                                  text += '<div  style="height: 500px" id="widget8"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/E11D6A/ffffff&amp;text=Tendencia+to-do\'s" alt="">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;
                              case 9:
                                  text += ' <div  style="height: 500px" id="widget9"  class="item">\n' +
                                      '                                <img width="100%" height="70%" src="http://placehold.it/900x400/1D20E1/ffffff&amp;text=Finanzas+(histórico+anual)" alt="">\n' +
                                      '\n' +
                                      '                                <div class="carousel-caption">\n' +
                                      '\n' +
                                      '                                </div>\n' +
                                      '                            </div>';
                                  break;

                          }
                      }

                      text += ' </div>\n' +
                          '                       \n' +
                          '                    </div> ';

                      $('.carousel-widget').html(text);

                      getFoto();

                  } );


           // genChart();

        }

        function stopCarousel()
        {
            //carousel-widgets
            $("#carousel-widgets").carousel("pause");
        //<button onclick="stopCarousel()" class="btn btn-primary block"> II</button>
            $('.pause-car').html('<button onclick="playCarousel()" class="btn btn-primary block fa fa-play"> </button>');
        }

        function playCarousel()
        {
            //carousel-widgets
            $("#carousel-widgets").carousel("cycle");
            //<button onclick="stopCarousel()" class="btn btn-primary block"> II</button>
            $('.pause-car').html('<button onclick="stopCarousel()" class="btn btn-primary block fa fa-pause"> </button>');
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
        function showPosition(position) {

            var vis = document.getElementById('id_visita').value;

            myMap(position.coords.latitude, position.coords.longitude, vis);


        }
        //To use this code on your website, get a free API key from Google.
        //Read more at: https://www.w3schools.com/graphics/google_maps_basic.asp

        function showError(error) {
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    x.innerHTML = "User denied the request for Geolocation."
                    break;
                case error.POSITION_UNAVAILABLE:
                    x.innerHTML = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    x.innerHTML = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    x.innerHTML = "An unknown error occurred."
                    break;
            }
        }

        function findMe() {


        }

        function getFoto()
        {

            var url = '{{route('regFotoTienda')}}';
            var src = '{{ asset('uploads/Tiendas/')}}';
            src += '/'+'{{$info->imagen}}';
            var text ="";

            text +=
                '          <div class="box box-default">\n' +
                '            <div class="box-header with-border">\n' +
                '              <h3 class="box-title">Foto de la Sucursal</h3>\n' +
                '            </div>\n' +
                '            <div class="box-body">';

            text += '<form action="'+url+'" method="post" style="display: none" id="avatarFormS">\n' +
                '                        <input type="file" id="avatarInputS" name="photo">\n' +
                '                    </form>\n' +
                '                    <div class="rutaFotoS">\n' +
                '                        <img onclick="setFotoS()" width="500" height="500"  style="display: block;\n' +
                '  margin-left: auto;\n' +
                '  margin-right: auto;" class="img-responsive pad" id="avatarImageS" src="'+src+'" alt="User profile picture">\n' +
                '\n' +
                '                    </div>';

            text += ' </div>\n' +
                '          </div>\n' +
                '      ';

            $('.fotowid').html( text );

        }


        function setFotoS()
        {
            $avatarImage = $('#avatarImageS');
            $avatarInput = $('#avatarInputS');
            $avatarForm = $('#avatarFormS');
            $rutaFoto = $('.rutaFotoS');
            $avatarInput.click();
            $avatarInput.on('change', function () {
                var formData = new FormData();
                formData.append('photo', $avatarInput[0].files[0]);

                $('#modal-info').modal('toggle');

                $.ajax({
                    url: $avatarForm.attr('action') + '?' + $avatarForm.serialize(),
                    method: $avatarForm.attr('method'),
                    data: formData,
                    processData: false,
                    contentType: false
                }).done(function (msg) {
                    // alert(cam);
                    /*if (data.success)
                        $avatarImage.attr('src', data.path);*/
                    var URLdomain = window.location.host;
                    var pathImg = 'http://'+URLdomain+'/'+msg['path'];

                    $avatarImage.attr('src', pathImg)
                    console.log(msg['message']);
                    $('#modal-info').modal('toggle');

                    //  $('.foto_tienda').html('<input type="hidden" id="foto_tienda" value="' + msg['message'] + '" >')



                    updateTienda(msg['message']);


                });
            });
        }

        function updateTienda(foto)
        {
            var tiendas_Id = '{{$info->Id}}';
            var url = '{{route('updateFotoTienda')}}'
            $.ajax({
                method: 'POST',
                url: url,
                data: {tiendas_Id:  tiendas_Id,
                    foto: foto }
            })
                .done(function(msg) {

                });
        }


    </script>

    <script>
        function myMap(Lat, Lng, vis) {

            Lat = parseFloat(Lat);
            Lng = parseFloat(Lng);
            var myLatLng = {lat: Lat, lng: Lng};
            var mapProp= {
                center:new google.maps.LatLng(Lat,Lng),
                zoom:17,
            };

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: mapProp,
                title: 'Hello World!'
            });


            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            marker.setMap(map);

          //  var vis = document.getElementById('id_visita').value;
            var lat =Lat;
            var lon =Lng;
            var url = '{{route('setLocalizacion')}}';

            $.ajax({
                method: 'POST',
                url: url,
                data: {id:vis, lat: lat, lon: lon}
            })
                .done(function(msg){
                    console.log(msg['message']);
                });

        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDigdQtAwnyVRn7wCrver_6pilN-7vfnNY&callback=myMap"></script>



@stop