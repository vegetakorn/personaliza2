@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')


@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">


        <!-- Main Header -->
        <header  class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
                <nav  class="navbar navbar-static-top">
                    <div  class="container">
                        <div class="navbar-header">
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                                {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div   class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    @else
                        <!-- Logo -->
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                                <!-- mini logo for sidebar mini 50x50 pixels -->
                                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                                <!-- logo for regular state and mobile devices -->
                                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
                            </a>

                            <!-- Header Navbar -->
                            <nav class="navbar navbar-static-top" role="navigation">
                                <!-- Sidebar toggle button-->
                                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                                </a>
                            @endif
                            <!-- Navbar Right Menu -->
                                <div class="navbar-custom-menu">
                                    <!-- Messages: style can be found in dropdown.less-->

                                    <ul class="nav navbar-nav">

                                        <li class=" notifications-menu">
                                            <a   href="{{route('usuariotienda.tienda.tiendas')}}"  >
                                                <i class="fa fa-shopping-cart"></i>
                                                <span class="label label-success"></span>
                                            </a>

                                        </li>

                                        <!-- User Account: style can be found in dropdown.less -->
                                        <li class="dropdown user user-menu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <img src="{{ asset('/uploads/Tiendas/') }}/{{ Auth::user()->fotoUser }}" class="user-image" alt="User Image">
                                                <span class="hidden-xs">{{ Auth::user()->name }}  </span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <!-- User image -->
                                                <li class="user-header">
                                                    <img src="{{ asset('/uploads/Tiendas/') }}/{{ Auth::user()->fotoUser }}" class="img-circle" alt="User Image">

                                                    <p>
                                                        {{ Auth::user()->name }}
                                                        <small>{{date('d/m/Y' )}}</small>
                                                    </p>
                                                </li>
                                                <!-- Menu Body -->
                                                <li class="user-body">

                                                    <!-- /.row -->
                                                </li>
                                                <!-- Menu Footer-->
                                                <li class="user-footer">
                                                   <!-- <div class="pull-left">
                                                        <a href="" class="btn btn-default btn-flat">Datos de Sucursal</a>
                                                    </div>-->
                                                    <div class="pull-right">
                                                        @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                                            <a class="btn btn-default btn-flat" href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                                                <i class=" fa fa-fw fa-power-off"></i>
                                                            </a>
                                                        @else
                                                            <a class="btn btn-default btn-flat" href="#" style="color: #2e3a78"
                                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                                            >
                                                                <i class=" fa fa-fw fa-power-off"></i>
                                                            </a>
                                                            <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                                                @if(config('adminlte.logout_method'))
                                                                    {{ method_field(config('adminlte.logout_method')) }}
                                                                @endif
                                                                {{ csrf_field() }}
                                                            </form>
                                                        @endif
                                                    </div>
                                                </li>

                                            </ul>
                                        </li>

                                        <li class="">

                                            <img src="{{ asset('/uploads/Empresas/') }}/{{ Auth::user()->fotoEmp }}" alt="Avatar of {{ Auth::user()->name }} " width="150px" height="50px" >
                                        </li>
                                    </ul>
                                </div>
                            @if(config('adminlte.layout') == 'top-nav')
                    </div>
                    @endif
                </nav>
        </header>

    @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">

                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">


                    <ul class="sidebar-menu" data-widget="tree">
                        <!-- Sidebar Menu -->



                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-binoculars"></i>
                                <span>Supervisión</span>
                                <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                            </a>
                            <ul class="treeview-menu">

                                <li >
                                    <a href="{{route('usuariotienda.visitas.lista')}}">
                                        <i class="fa fa-newspaper-o"></i> <span>Listado de Visitas</span>
                                    </a>
                                </li>
                                <li >
                                    <a href="{{route('usuariotienda.todos.lista')}}">
                                        <i class="fa fa-calendar-check-o"></i> <span>Listado de To-do's</span>
                                    </a>
                                </li>

                            </ul>
                        </li>



                        <li class="header">FINANZAS</li>

                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-text"></i>
                                <span>Reportes de Venta</span>
                                <span class="pull-right-container">
                                  <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li >
                                    <a href="{{route('usuariotienda.reportes.ventasmes')}}">
                                        <i class="fa fa-dollar"></i> <span>Venta Mensual</span>
                                    </a>
                                </li>

                                <li >
                                    <a href="{{route('usuariotienda.reportes.ventarango')}}">
                                        <i class="fa fa-dollar"></i> <span>Venta por Rango</span>
                                    </a>
                                </li>

                            </ul>
                        </li>


                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
    @endif

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
                <div class="container">
                @endif

                <!-- Content Header (Page header) -->
                    <section class="content-header">
                        @yield('content_header')
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        @yield('content')

                    </section>
                    <!-- /.content -->
                    @if(config('adminlte.layout') == 'top-nav')
                </div>
                <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    @routes
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>

    <script>

    </script>

    @stack('js')
    @yield('js')
@stop
