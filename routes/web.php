<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
   

    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**PERSONALIZA**/

Route::get('/personaliza/usuarios', 'Personaliza\UsuariosController@index')->name('personaliza.usuarios');
Route::get('/personaliza/clientes', 'Personaliza\ClientesController@index')->name('personaliza.clientes');
Route::get('/personaliza/proveedores', 'Personaliza\ProveedoresController@index')->name('personaliza.proveedores');

/****/

/*

Route::get('/sucursales', 'HomeController@index')->name('sucursales');
Route::get('/empleados/empleados', 'Catalogos\EmpleadosController@index')->name('empleados.empleados');
Route::get('/visitas/visita/{id}', 'Visitas\VisitaController@index')->name('visitas.visita')->where('id', '[0-9]+');
Route::post('/visitas/fotos/', 'Visitas\VisitaController@fotos')->name('visitas.fotos');
Route::get('/visitas/final/{id}', 'Visitas\VisitaController@finalVisita')->name('visitas.final')->where('id', '[0-9]+');
Route::post('/getcat', 'Catalogos\ChecklistController@categorias')->name('getcat');
Route::post('/getcam', 'Catalogos\ChecklistController@campos')->name('getcam');
Route::post('/genGraficaVta', 'HomeController@genGraficaVta')->name('genGraficaVta');
Route::post('/filtroSignos', 'HomeController@filtroSignosVitales')->name('filtroSignos');
//evaluacion del desempeño
Route::post('/getDesempeno', 'HomeController@getDesempeno')->name('getDesempeno');
Route::get('/evalDesempPdf/{fechaini}/{fechafin}', 'HomeController@evalDesempPdf')->name('evalDesempPdf');
Route::get('/evalDesempXls/{fechaini}/{fechafin}', 'HomeController@evalDesempXls')->name('evalDesempXls');
//visita de checklist
Route::post('/genVis', 'Visitas\VisitaController@inicia')->name('genVis');
Route::post('/recuperaVis', 'Visitas\VisitaController@recupera')->name('recuperaVis');
Route::post('/regCampo', 'Visitas\VisitaController@registroCampo')->name('regCampo');
Route::post('/regFotoCampo', 'Visitas\VisitaController@registroFotoCampo')->name('regFotoCampo');
Route::post('/regFotoCampoCom', 'Visitas\VisitaController@registroFotoCampoCom')->name('regFotoCampoCom');
Route::post('/getToDoVis', 'Visitas\VisitaController@getTodosVisita')->name('getToDoVis');
Route::post('/setFotoVis', 'Visitas\VisitaController@setFotoVis')->name('setFotoVis');
Route::post('/getInfoTodo', 'Visitas\VisitaController@getInfoTodo')->name('getInfoTodo');
Route::post('/putFotoCampo', 'Visitas\VisitaController@putFotoCampo')->name('putFotoCampo');
Route::post('/updateTodo', 'Visitas\VisitaController@updateTodo')->name('updateTodo');
Route::post('/setLocalizacion', 'Visitas\VisitaController@setLocalizacion')->name('setLocalizacion');
Route::post('/setTimeFinal', 'Visitas\VisitaController@setTimeFinal')->name('setTimeFinal');
Route::post('/setDescFoto', 'Visitas\VisitaController@setDescFoto')->name('setDescFoto');
Route::post('/putFotoCom', 'Visitas\VisitaController@putFotoCom')->name('putFotoCom');
Route::post('/getFotoComs', 'Visitas\VisitaController@getFotoComs')->name('getFotoComs');
Route::get('/reporteVisita/{id}', 'Visitas\VisitaController@visitaPdf')->name('reporteVisita')->where('id', '[0-9]+');
Route::post('/cancelaVisita', 'Visitas\VisitaController@cancelaVisita')->name('cancelaVisita');
Route::post('/recuperaVisita', 'Visitas\VisitaController@recuperaVisita')->name('recuperaVisita');
Route::post('/valPassTienda', 'Visitas\VisitaController@valPassTienda')->name('valPassTienda');
Route::post('/resetPassTienda', 'Visitas\VisitaController@resetPassTienda')->name('resetPassTienda');
Route::post('/valCodeTienda', 'Visitas\VisitaController@valCodeTienda')->name('valCodeTienda');
Route::post('/getCuestionario', 'Visitas\VisitaController@getCuestionario')->name('getCuestionario');
Route::post('/setCuestionario', 'Visitas\VisitaController@setCuestionario')->name('setCuestionario');
Route::post('/genGraficaTodo', 'Visitas\VisitaController@genGraficaTodo')->name('genGraficaTodo');
//ventas
Route::get('/ventas/ventas', 'VentasController@index')->name('ventas.ventas');
Route::post('/getVentasMes', 'VentasController@getVentasMes')->name('getVentasMes');
Route::post('/getVtaDiaria', 'VentasController@getVtaDiaria')->name('getVtaDiaria');
Route::post('/updateMeses', 'VentasController@updateMeses')->name('updateMeses');
//seguimiento
Route::get('/visitas/seguimiento/{id}', 'Visitas\SeguimientoController@index')->name('visitas.seguimiento')->where('id', '[0-9]+');
Route::post('/genSeg', 'Visitas\SeguimientoController@inicia')->name('genSeg');
Route::post('/getCategorias', 'Visitas\SeguimientoController@categorias')->name('getCategorias');
Route::post('/getCampos', 'Visitas\SeguimientoController@campos')->name('getCampos');
Route::post('/addTodoSeg', 'Visitas\SeguimientoController@addToDo')->name('addTodoSeg');
Route::post('/getToDos', 'Visitas\SeguimientoController@getToDo')->name('getToDos');
Route::post('/putFotoCampoSeg', 'Visitas\SeguimientoController@putFotoCampoSeg')->name('putFotoCampoSeg');
Route::post('/updateTodoSeg', 'Visitas\SeguimientoController@updateTodoSeg')->name('updateTodoSeg');
Route::post('/delFromSeg', 'Visitas\SeguimientoController@delFromSeg')->name('delFromSeg');
Route::post('/cancelaVisitaSeg', 'Visitas\SeguimientoController@cancelaVisitaSeg')->name('cancelaVisitaSeg');
Route::post('/recuperaVisitaSeg', 'Visitas\SeguimientoController@recuperaVisitaSeg')->name('recuperaVisitaSeg');
Route::get('/visitas/finalseg/{id}', 'Visitas\SeguimientoController@finalVisita')->name('visitas.finalseg')->where('id', '[0-9]+');
Route::get('/reporteVisitaSeg/{id}', 'Visitas\SeguimientoController@visitaPdf')->name('reporteVisitaSeg')->where('id', '[0-9]+');
Route::post('/setTimeFinalSeg', 'Visitas\SeguimientoController@setTimeFinalSeg')->name('setTimeFinalSeg');
//cuestionarios
Route::get('/cuestionarios/cuestionarios', 'Catalogos\CuestionariosController@index')->name('cuestionarios.cuestionarios');
Route::post('/getpreg', 'Catalogos\CuestionariosController@preguntas')->name('getpreg');
Route::post('/getresp', 'Catalogos\CuestionariosController@respuestas')->name('getresp');
Route::post('/savePregunta', 'Catalogos\CuestionariosController@savePregunta')->name('savePregunta');
Route::post('/addResp', 'Catalogos\CuestionariosController@addResp')->name('addResp');
//empleados
Route::post('/regFotoEmpleado', 'Catalogos\EmpleadosController@fotoEmpleado')->name('regFotoEmpleado');
Route::post('/cargaInfoEmpleado', 'Catalogos\EmpleadosController@cargaInfoEmpleado')->name('cargaInfoEmpleado');
Route::post('/cargaPlazas', 'Catalogos\EmpleadosController@cargaPlazas')->name('cargaPlazas');
Route::post('/cargaTiendas', 'Catalogos\EmpleadosController@cargaTiendas')->name('cargaTiendas');
Route::post('/validaMail', 'Catalogos\EmpleadosController@validaMail')->name('validaMail');
Route::post('/addEmpleado', 'Catalogos\EmpleadosController@addEmpleado')->name('addEmpleado');
Route::post('/bajaEmpleado', 'Catalogos\EmpleadosController@bajaEmpleado')->name('bajaEmpleado');
Route::post('/cargaInfoEmpleadoEditar', 'Catalogos\EmpleadosController@cargaInfoEmpleadoEditar')->name('cargaInfoEmpleadoEditar');
Route::post('/updateEmpleado', 'Catalogos\EmpleadosController@updateEmpleado')->name('updateEmpleado');
Route::post('/setCuestBaja', 'Catalogos\EmpleadosController@setCuestBaja')->name('setCuestBaja');
//listado de visitas
Route::get('/visitas/lista', 'Visitas\ListadoController@index')->name('visitas.lista');
Route::get('/usuariotienda/visitas/lista', 'Visitas\ListadoController@indextienda')->name('usuariotienda.visitas.lista');
Route::post('/getVisitas', 'Visitas\ListadoController@getVisitas')->name('getVisitas');
Route::get('/listaVisitas/{sup}/{seg}/{fechaini}/{fechafin}/{tienda}/{id_sup}/{razon}/{plaza}', 'Visitas\ListadoController@pdf')->name('listaVisitas');
Route::get('/listaVisitasXls/{sup}/{seg}/{fechaini}/{fechafin}/{tienda}/{id_sup}/{razon}/{plaza}', 'Visitas\ListadoController@excel')->name('listaVisitasXls');
//listado de sucursales
Route::get('/tiendas/tiendas', 'Catalogos\TiendasController@index')->name('tiendas.tiendas');
Route::get('/usuariotienda/tienda/tiendas', 'Catalogos\TiendasController@usertienda')->name('usuariotienda.tienda.tiendas');
Route::post('/regFotoTienda', 'Catalogos\TiendasController@fotoTienda')->name('regFotoTienda');
Route::post('/cargaInfoTiendaEditar', 'Catalogos\TiendasController@cargaInfoTiendaEditar')->name('cargaInfoTiendaEditar');
Route::post('/updateTienda', 'Catalogos\TiendasController@updateTienda')->name('updateTienda');
Route::post('/updateFotoTienda', 'Catalogos\TiendasController@updateFotoTienda')->name('updateFotoTienda');
Route::post('/getUbicacion', 'Catalogos\TiendasController@getUbicacion')->name('getUbicacion');
Route::post('/asignaEmp', 'Catalogos\TiendasController@asignaEmp')->name('asignaEmp');
Route::post('/cargaEmp', 'Catalogos\TiendasController@cargaEmp')->name('cargaEmp');
Route::post('/bajaSucEmp', 'Catalogos\TiendasController@bajaSucEmp')->name('bajaSucEmp');
Route::post('/getFoda', 'Catalogos\TiendasController@getFoda')->name('getFoda');
Route::post('/updateFoda', 'Catalogos\TiendasController@updateFoda')->name('updateFoda');
Route::post('/addWidTienda', 'Catalogos\TiendasController@addWidTienda')->name('addWidTienda');
//listado de puestos
Route::get('/puestos/lista', 'Catalogos\PuestosController@index')->name('puestos.lista');
Route::post('/addPuesto', 'Catalogos\PuestosController@addPuesto')->name('addPuesto');
Route::post('/setTipo', 'Catalogos\PuestosController@setTipo')->name('setTipo');
Route::post('/editPuesto', 'Catalogos\PuestosController@editPuesto')->name('editPuesto');
Route::post('/updatePuesto', 'Catalogos\PuestosController@updatePuesto')->name('updatePuesto');
Route::post('/addChkPuesto', 'Catalogos\PuestosController@addChkPuesto')->name('addChkPuesto');
Route::post('/bajaPuesto', 'Catalogos\PuestosController@bajaPuesto')->name('bajaPuesto');
//listado de razones
Route::get('/razones/razones', 'Catalogos\RazonesController@index')->name('razones.razones');
Route::post('/regFotoRazon', 'Catalogos\RazonesController@fotoRazon')->name('regFotoRazon');
Route::post('/addRazon', 'Catalogos\RazonesController@addRazon')->name('addRazon');
Route::post('/cargaInfoRazonEditar', 'Catalogos\RazonesController@cargaInfoRazonEditar')->name('cargaInfoRazonEditar');
Route::post('/updateRazon', 'Catalogos\RazonesController@updateRazon')->name('updateRazon');
Route::post('/bajaRazon', 'Catalogos\RazonesController@bajaRazon')->name('bajaRazon');
//listado de plazas
Route::get('/plazas/plazas', 'Catalogos\PlazasController@index')->name('plazas.plazas');
Route::post('/regFotoPlaza', 'Catalogos\PlazasController@fotoPlaza')->name('regFotoPlaza');
Route::post('/addPlaza', 'Catalogos\PlazasController@addPlaza')->name('addPlaza');
Route::post('/cargaInfoPlazaEditar', 'Catalogos\PlazasController@cargaInfoPlazaEditar')->name('cargaInfoPlazaEditar');
Route::post('/updatePlaza', 'Catalogos\PlazasController@updatePlaza')->name('updatePlaza');
Route::post('/cargaInfoPlaza', 'Catalogos\PlazasController@cargaInfoPlaza')->name('cargaInfoPlaza');
Route::post('/bajaPlaza', 'Catalogos\PlazasController@bajaPlaza')->name('bajaPlaza');
//listado de zonas
Route::get('/zonas/zonas', 'Catalogos\ZonasController@index')->name('zonas.zonas');
Route::post('/regFotoZona', 'Catalogos\ZonasController@fotoZona')->name('regFotoZona');
Route::post('/addZona', 'Catalogos\ZonasController@addZona')->name('addZona');
Route::post('/cargaInfoZonaEditar', 'Catalogos\ZonasController@cargaInfoZonaEditar')->name('cargaInfoZonaEditar');
Route::post('/updateZona', 'Catalogos\ZonasController@updateZona')->name('updateZona');
Route::post('/bajaZona', 'Catalogos\ZonasController@bajaZona')->name('bajaZona');
//listado de familias
Route::get('/familias/familias', 'Catalogos\FamiliasController@index')->name('familias.familias');
Route::post('/addFamilia', 'Catalogos\FamiliasController@addFamilia')->name('addFamilia');
Route::post('/cargaInfoFamEditar', 'Catalogos\FamiliasController@cargaInfoFamEditar')->name('cargaInfoFamEditar');
Route::post('/updateFamilia', 'Catalogos\FamiliasController@updateFamilia')->name('updateFamilia');
//listadp de todos de visitas
Route::get('/todos/lista', 'Visitas\TodosController@index')->name('todos.lista');
Route::get('/usuariotienda/todos/lista', 'Visitas\TodosController@indextienda')->name('usuariotienda.todos.lista');
Route::post('/getTodos', 'Visitas\TodosController@getTodos')->name('getTodos');
Route::post('/getToDoFiltro', 'Visitas\TodosController@getToDoFiltro')->name('getToDoFiltro');
Route::get('/listaTodo/{tipoChk}/{tipoExp}/{fam}/{nuevo}/{curso}/{pendiente}/{fechaini}/{fechafin}/{tiendas_Id}/{chk_Id}/{cat_Id}/{razon}/{plaza}/{supervisor}', 'Visitas\TodosController@pdf')->name('listaTodo');
//modulo de checklist
Route::get('/checklist/checklist', 'Catalogos\ChecklistController@index')->name('checklist.checklist');
Route::post('addChecklist', 'Catalogos\ChecklistController@addChecklist')->name('addChecklist');
Route::post('updChecklist', 'Catalogos\ChecklistController@updChecklist')->name('updChecklist');
Route::post('delChecklist', 'Catalogos\ChecklistController@delChecklist')->name('delChecklist');
Route::post('addCategoria', 'Catalogos\ChecklistController@addCategoria')->name('addCategoria');
Route::post('updCategoria', 'Catalogos\ChecklistController@updCategoria')->name('updCategoria');
Route::post('delCategoria', 'Catalogos\ChecklistController@delCategoria')->name('delCategoria');
Route::post('/regFotoCampoChk', 'Catalogos\ChecklistController@fotoCampo')->name('regFotoCampoChk');
Route::post('/addCampo', 'Catalogos\ChecklistController@addCampo')->name('addCampo');
Route::post('/editCampo', 'Catalogos\ChecklistController@editCampo')->name('editCampo');
Route::post('/updateCampo', 'Catalogos\ChecklistController@updateCampo')->name('updateCampo');
Route::post('/delCampo', 'Catalogos\ChecklistController@delCampo')->name('delCampo');
//listado de usuarios
Route::get('/usuarios/lista', 'Catalogos\UsuariosController@index')->name('usuarios.lista');
Route::post('getMailEmp',  'Catalogos\UsuariosController@getMailEmp')->name('getMailEmp');
Route::post('saveUsuario',  'Catalogos\UsuariosController@saveUsuario')->name('saveUsuario');
Route::post('getInfoUsuario',  'Catalogos\UsuariosController@getInfoUsuario')->name('getInfoUsuario');
Route::post('saveEditEmp',  'Catalogos\UsuariosController@saveEditEmp')->name('saveEditEmp');
Route::post('/addModEmpleado', 'Catalogos\UsuariosController@addModEmpleado')->name('addModEmpleado');
Route::post('/addWidEmpleado', 'Catalogos\UsuariosController@addWidEmpleado')->name('addWidEmpleado');
Route::post('/addSucEmpleado', 'Catalogos\UsuariosController@addSucEmpleado')->name('addSucEmpleado');
Route::post('/addEmpEmpleado', 'Catalogos\UsuariosController@addEmpEmpleado')->name('addEmpEmpleado');
Route::post('/addColEmpleado', 'Catalogos\UsuariosController@addColEmpleado')->name('addColEmpleado');
Route::post('/showEmps', 'Catalogos\UsuariosController@showEmps')->name('showEmps');
Route::post('/showSuc', 'Catalogos\UsuariosController@showSuc')->name('showSuc');
Route::post('/updateUser', 'Catalogos\UsuariosController@updateUser')->name('updateUser');
Route::post('/todosPerfil', 'Catalogos\UsuariosController@todosPerfil')->name('todosPerfil');
Route::get('/usuarios/perfil', 'Catalogos\UsuariosController@perfil')->name('usuarios.perfil');
Route::get('/usuarios/actividades', 'Catalogos\UsuariosController@actividades')->name('usuarios.actividades');
Route::get('/usuarios/express', 'Catalogos\UsuariosController@express')->name('usuarios.express');
Route::get('/usuarios/tickets', 'Catalogos\UsuariosController@tickets')->name('usuarios.tickets');
Route::post('/setTodoExpress', 'Catalogos\UsuariosController@setTodoExpress')->name('setTodoExpress');
Route::post('/bajaUser', 'Catalogos\UsuariosController@bajaUser')->name('bajaUser');
Route::post('/cambiaPass', 'Catalogos\UsuariosController@cambiaPass')->name('cambiaPass');
Route::post('sendMailRec', 'Auth\ForgotPasswordController@sendMailRec')->name('sendMailRec');
//upload de archivos
Route::post('/uploads/visita', 'FileController@store')->name('uploads.visita');
Route::post('uploadImgVisita',  'ImageController@uploadImgVisita')->name('uploadImgVisita');
Route::post('uploadImgSeguimiento',  'ImageController@uploadImgSeguimiento')->name('uploadImgSeguimiento');
Route::post('uploadImgCampo',  'ImageController@uploadImgCampo')->name('uploadImgCampo');
Route::post('subeArchivoVentas',  'VentasController@subeArchivoVentas')->name('subeArchivoVentas');
Route::post('subeArchivoPres',  'VentasController@subeArchivoPres')->name('subeArchivoPres');
//panel de control
Route::get('/panel/panel', 'PanelController@index')->name('panel.panel');
Route::post('setConfig', 'PanelController@setConfig')->name('setConfig');
Route::post('getFunciones', 'PanelController@getFunciones')->name('getFunciones');
Route::post('getFuncionesTienda', 'PanelController@getFuncionesTienda')->name('getFuncionesTienda');
Route::post('getWidgets', 'PanelController@getWidgets')->name('getWidgets');
//funciones del header
Route::post('getTiendas', 'Catalogos\TiendasController@getTiendas')->name('getTiendas');
//envio de correos
Route::get('/correos/correos', 'Configuracion\CorreosController@index')->name('correos.correos');
Route::post('cargaEmpleados', 'Configuracion\CorreosController@cargaEmpleados')->name('cargaEmpleados');
Route::post('agregaEnvio', 'Configuracion\CorreosController@agregaEnvio')->name('agregaEnvio');
Route::post('cargaConfiguracionesCorreo', 'Configuracion\CorreosController@cargaConfiguracionesCorreo')->name('cargaConfiguracionesCorreo');
Route::post('bajaCorreo', 'Configuracion\CorreosController@bajaCorreo')->name('bajaCorreo');
//views de reportes
Route::get('/reportes/ventasmes', 'ReportesController@index')->name('reportes.ventasmes');
Route::get('/reportes/ventarango', 'ReportesController@rango')->name('reportes.ventarango');
Route::get('/usuariotienda/reportes/ventasmes', 'ReportesController@sucmes')->name('usuariotienda.reportes.ventasmes');
Route::get('/usuariotienda/reportes/ventarango', 'ReportesController@sucrango')->name('usuariotienda.reportes.ventarango');
Route::get('/reportes/evaluaciones', 'ReportesController@evaluaciones')->name('reportes.evaluaciones');
//reportes en excel 3.0
Route::get('/exportUsers', 'Catalogos\UsuariosController@export')->name('exportUsers');
Route::get('/exportSuc', 'Catalogos\TiendasController@export')->name('exportSuc');
Route::get('/exportEmp', 'Catalogos\EmpleadosController@export')->name('exportEmp');
Route::get('exportVentaMes/{anio}/{mes}/{suc}', 'VentasController@export')->name('exportVentaMes');
Route::get('exportVentaSuc/{razon}/{plaza}/{anio}/{mes}/{suc}', 'ReportesController@exportSuc')->name('exportVentaSuc');
Route::get('exportVentaRango/{razon}/{plaza}/{fecha_ini}/{fecha_fin}/{suc}', 'ReportesController@exportRango')->name('exportVentaRango');
Route::get('exportEvaluaciones/{razon}/{plaza}/{anio}/{mes}/{suc}/{chk}', 'ReportesController@exportEv')->name('exportEvaluaciones');
Route::get('/listaTodoExcel/{tipoChk}/{tipoExp}/{fam}/{nuevo}/{curso}/{pendiente}/{fechaini}/{fechafin}/{tiendas_Id}/{chk_Id}/{cat_Id}/{razon}/{plaza}/{supervisor}', 'Visitas\TodosController@excel')->name('listaTodoExcel');
//reportes excel 2.1
//Route::post('/import-excel', 'ExcelController@importUsers');
//manuales de sistema
Route::get('/ayuda/manual', 'ManualController@index')->name('ayuda.manual');
//actividades de supervisor
Route::get('/actividades/lista', 'Supervisores\ActividadesController@index')->name('actividades.lista');
Route::post('/regFotoActividad', 'Supervisores\ActividadesController@foto')->name('regFotoActividad');
Route::post('/agregaActividad', 'Supervisores\ActividadesController@agregaActividad')->name('agregaActividad');
Route::post('/asignaSupervisor', 'Supervisores\ActividadesController@asignaSupervisor')->name('asignaSupervisor');
Route::post('/getActividadFiltro', 'Supervisores\ActividadesController@getActividadFiltro')->name('getActividadFiltro');
Route::post('/editarActividad', 'Supervisores\ActividadesController@editarActividad')->name('editarActividad');
Route::post('/setComentario', 'Supervisores\ActividadesController@setComentario')->name('setComentario');
Route::post('/getComentario', 'Supervisores\ActividadesController@getComentario')->name('getComentario');
Route::post('/updComentario', 'Supervisores\ActividadesController@updComentario')->name('updComentario');
Route::get('/exportAct/{fecha_ini}/{fecha_fin}/{supervisor}/{abierto}/{cerrado}', 'Supervisores\ActividadesController@export')->name('exportAct');
Route::post('/valDirector', 'Supervisores\ActividadesController@valDirector')->name('valDirector');
//plan smart
Route::post('/setSmart', 'Supervisores\SmartController@setSmart')->name('setSmart');
Route::post('/getSmart', 'Supervisores\SmartController@getSmart')->name('getSmart');
Route::post('/validaReg', 'Supervisores\SmartController@validaReg')->name('validaReg');
Route::post('/updSmart', 'Supervisores\SmartController@updSmart')->name('updSmart');
Route::post('/setAccion', 'Supervisores\SmartController@setAccion')->name('setAccion');
Route::post('/getAcciones', 'Supervisores\SmartController@getAcciones')->name('getAcciones');*/